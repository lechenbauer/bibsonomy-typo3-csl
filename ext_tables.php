<?php

if (!defined('TYPO3_MODE')) {
    die('Access denied.');
}

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin('AcademicPuma.' . $_EXTKEY, 'PublicationList',
    'Publication List from PUMA/BibSonomy');

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin('AcademicPuma.' . $_EXTKEY, 'TagCloud',
    'Tag Cloud from PUMA/BibSonomy');

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tt_content.pi_flexform.extbibsonomycsl_publicationlist.list',
    'EXT:ext_bibsonomy_csl/Configuration/FlexForms/locallang_csh_flexform.xml');

if (TYPO3_MODE === 'BE') {
    /**
     * Registers a Backend Module
     */
    \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerModule('AcademicPuma.' . $_EXTKEY, 'System',
        // Make module a submodule of 'web'
        'bibsonomybackend',    // Submodule key
        'top',                        // Position
        array(
            'Backend'   => 'index, list, create, import, delete, upload, json',
            'BasicAuth' => 'list, edit, update, new, create, delete',
            'OAuth'     => 'list, delete, oauth, callback'
        ), array(
            'access' => 'user,group',
            'icon'   => 'EXT:' . $_EXTKEY . '/ext_icon.gif',
            'labels' => 'LLL:EXT:' . $_EXTKEY . '/Resources/Private/Language/locallang_bibsonomybackend.xlf',
        ));
}

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile($_EXTKEY, 'Configuration/TypoScript', 'Setup');

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_extbibsonomycsl_domain_model_citationstylesheet',
    'EXT:ext_bibsonomy_csl/Resources/Private/Language/locallang_csh_tx_extbibsonomycsl_domain_model_citationstylesheet.xml');

$pluginSignature = "extbibsonomycsl_publicationlist"; //publication list

// not neccessary, because full TCA is always loaded in all context
// \TYPO3\CMS\Core\Utility\GeneralUtility::loadTCA('tt_content');

$TCA['tt_content']['types']['list']['subtypes_addlist'][$pluginSignature]     = 'pi_flexform';
$TCA['tt_content']['types']['list']['subtypes_excludelist'][$pluginSignature] = 'layout,select_key';
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPiFlexFormValue($pluginSignature,
    'FILE:EXT:' . $_EXTKEY . '/Configuration/FlexForms/setup.xml');

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig('
    mod.wizards.newContentElement.wizardItems.plugins.elements.' . $pluginSignature . ' {
            icon = LLL:EXT:' . $_EXTKEY . '/Resources/Public/Icons/plugin.png
            title = PUMA/BibSonomy Publication List
            description = Renders a publication list or a bibliography from PUMA or BibSonomy
            tt_content_defValues {
                CType = list
                list_type = ' . $pluginSignature . '
            }
    }
    templavoila.wizards.newContentElement.wizardItems.plugins.elements.' . $pluginSignature . ' < mod.wizards.newContentElement.wizardItems.plugins.elements.' . $pluginSignature . '');

$pluginSignature = "extbibsonomycsl_tagcloud"; //tag cloud

$TCA['tt_content']['types']['list']['subtypes_addlist'][$pluginSignature]     = 'pi_flexform';
$TCA['tt_content']['types']['list']['subtypes_excludelist'][$pluginSignature] = 'layout,select_key';
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPiFlexFormValue($pluginSignature,
    'FILE:EXT:' . $_EXTKEY . '/Configuration/FlexForms/setup_tagcloud.xml');

//::addPageTSConfig('<INCLUDE_TYPOSCRIPT: source="FILE:EXT:ext_bibsonomy_csl/Configuration/TypoScript/pageTsConfig.ts">');

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig('
    mod.wizards.newContentElement.wizardItems.plugins.elements.' . $pluginSignature . ' {
            icon = LLL:EXT:' . $_EXTKEY . '/Resources/Public/Icons/plugin.png
            title = PUMA/BibSonomy Tag Cloud
            description = Renders a Tag Cloud from PUMA or BibSonomy
            tt_content_defValues {
                CType = list
                list_type = ' . $pluginSignature . '
            }
    }
    templavoila.wizards.newContentElement.wizardItems.plugins.elements.' . $pluginSignature . ' < mod.wizards.newContentElement.wizardItems.plugins.elements.' . $pluginSignature . '');

$pluginSignature = "extbibsonomycsl_publicationdetails"; //publication details

// not neccessary, because full TCA is always loaded in all context
// \TYPO3\CMS\Core\Utility\GeneralUtility::loadTCA('tt_content');

$TCA['tt_content']['types']['list']['subtypes_addlist'][$pluginSignature]     = 'pi_flexform';
$TCA['tt_content']['types']['list']['subtypes_excludelist'][$pluginSignature] = 'layout,select_key';
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPiFlexFormValue($pluginSignature,
    'FILE:EXT:' . $_EXTKEY . '/Configuration/FlexForms/setup_details.xml');

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig('
    mod.wizards.newContentElement.wizardItems.plugins.elements.' . $pluginSignature . ' {
            icon = LLL:EXT:' . $_EXTKEY . '/Resources/Public/Icons/plugin.png
            title = PUMA/BibSonomy Details View
            description = Use this plugin to display details of a single publication.
            tt_content_defValues {
                CType = list
                list_type = ' . $pluginSignature . '
            }
    }
    templavoila.wizards.newContentElement.wizardItems.plugins.elements.' . $pluginSignature . ' < mod.wizards.newContentElement.wizardItems.plugins.elements.' . $pluginSignature . '');
