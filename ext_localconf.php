<?php
if (!defined('TYPO3_MODE')) {
    die('Access denied.');
}

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin('AcademicPuma.' . $_EXTKEY, 'Publicationlist', array(
    'Publication' => 'list, details',
    'Document' => 'download, preview, view'
));

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin('AcademicPuma.' . $_EXTKEY, 'Tagcloud', array(
    'Tag' => 'list',

));
