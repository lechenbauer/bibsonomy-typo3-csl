<?php

use \TYPO3\CMS\Core\Utility\GeneralUtility;
use \TYPO3\CMS\Core\Utility\ExtensionManagementUtility;

/* * *************************************************************
 *  Copyright notice
 *
 *  (c) 2013-2014 Stanislas Rolland <typo3(arobas)sjbr.ca>
 *  All rights reserved
 *
 *  This script is part of the Typo3 project. The Typo3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 * ************************************************************* */

/**
 * Class for updating the db
 */
class ext_update
{

    /**
     * @var string Name of the extension this controller belongs to
     */
    protected $extensionName = 'ext_bibsonomy_csl';

    /**
     * @var TYPO3\CMS\Extbase\Object\ObjectManager Extbase Object Manager
     */
    protected $objectManager;

    /**
     * @var TYPO3\CMS\ExtensionManager\Utility\InstallUtility Extbase Install Tool
     */
    protected $installTool;

    /**
     * Main function, returning the HTML content
     *
     * @return string HTML
     */
    function main()
    {

        $content = '';

        $this->objectManager = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('TYPO3\\CMS\\Extbase\\Object\\ObjectManager');
        $this->installTool   = $this->objectManager->get('TYPO3\\CMS\\ExtensionManager\\Utility\\InstallUtility');
        $installToolSqlParser = GeneralUtility::makeInstance(SqlSchemaMigrationService::class);
        $this->installTool->injectInstallToolSqlParser($installToolSqlParser);
        $databaseUpdateUtility = GeneralUtility::makeInstance(DatabaseUpdateUtility::class);

        // Clear the class cache
        //$classCacheManager = $this->objectManager->get('SJBR\\StaticInfoTables\\Cache\\ClassCacheManager');
        //$classCacheManager->reBuild();

        $extensionSitePath = ExtensionManagementUtility::extPath($this->extensionName);
        // Extention only uses one table, so no table iteration?
        $this->processDatabaseUpdates($this->extensionName);
        $content .= '<p>Processing database updates</p>';
        // Now we process the static data updates (ext_tables_static+adt)
        // Note: The Install Tool Utility does not handle sql update statements
        $databaseUpdateUtility->doUpdate($this->extentionName);

        return $content;
    }

    /**
     * Processes the tables SQL File (ext_tables)
     *
     * @param string $extensionKey
     *
     * @return void
     */
    protected function processDatabaseUpdates($extensionKey)
    {

        $extensionSitePath = ExtensionManagementUtility::extPath($extensionKey);

        $extTablesSqlFile    = $extensionSitePath . 'ext_tables_update-2.0.0-beta.sql';
        $extTablesSqlContent = '';
        if (file_exists($extTablesSqlFile)) {
            $extTablesSqlContent .= GeneralUtility::getUrl($extTablesSqlFile);

            /*
            // Load XML with settings
            $xml= new DOMDocument();
            $xml->loadXML("", LIBXML_NOBLANKS); // Or load if filename required
            // Validate against schema XSD
            if (!$xml->schemaValidate($extensionSitePath . "test.xsd" )) // Or schemaValidateSource if string used.
            {
                // You have an error in the XML file
                // Extract basic infomation (API key, username, host system, source)
                // Insert in valid XML format
                // Save
                $extTablesSqlContent = fixExtDatabase($extTablesSqlContent);
            }
            */
        }
        if (!empty($extTablesSqlContent)) {
            $this->installTool->$this->installTool->updateDbWithExtTablesSql($extTablesSqlContent);
        }
    }

    /**
     * Imports a static tables SQL File (ext_tables_static+adt)
     *
     * @param string $extensionSitePath
     *
     * @return void
     */
    protected function importStaticSqlFile($extensionSitePath)
    {

        $extTablesStaticSqlFile    = $extensionSitePath . 'ext_tables_static+adt.sql';
        $extTablesStaticSqlContent = '';
        if (file_exists($extTablesStaticSqlFile)) {
            $extTablesStaticSqlContent .= GeneralUtility::getUrl($extTablesStaticSqlFile);
        }
        if ($extTablesStaticSqlContent !== '') {
            $this->installTool->importStaticSql($extTablesStaticSqlContent);
        }
    }

    function access()
    {
        return false;
    }

    function fixExtDatabase($extTablesSqlContent) {

    }

}
