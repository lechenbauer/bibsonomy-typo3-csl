'FE' => [
    'cacheHash' => [
        'excludedParameters' => [
            'tx_extbibsonomycsl_publicationlist[action]',
            'tx_extbibsonomycsl_publicationlist[preview]',
            'tx_extbibsonomycsl_publicationlist[controller]',
            'tx_extbibsonomycsl_publicationlist[userName]',
            'tx_extbibsonomycsl_publicationlist[fileName]',
            'tx_extbibsonomycsl_publicationlist[intraHash]',
        ],
    ],
]