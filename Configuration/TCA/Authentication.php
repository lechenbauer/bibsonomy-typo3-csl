<?php
if (!defined('TYPO3_MODE')) {
    die ('Access denied.');
}

$GLOBALS['TCA']['tx_extbibsonomycsl_domain_model_authentication'] = array(
    'ctrl'      => $GLOBALS['TCA']['tx_extbibsonomycsl_domain_model_authentication']['ctrl'],
    'interface' => array(
        'showRecordFieldList' => 'sys_language_uid, l10n_parent, l10n_diffsource, hidden, host_address, host_user_name, host_api_key, serialized_access_token, enable_o_auth, create_date',
    ),
    'types'     => array(
        '1' => array('showitem' => 'sys_language_uid;;;;1-1-1, l10n_parent, l10n_diffsource, hidden;;1, host_address, host_user_name, host_api_key, serialized_access_token, enable_o_auth, create_date, --div--;LLL:EXT:cms/locallang_ttc.xlf:tabs.access, starttime, endtime'),
    ),
    'palettes'  => array(
        '1' => array('showitem' => ''),
    ),
    'columns'   => array(

        'sys_language_uid'        => array(
            'exclude' => 1,
            'label'   => 'LLL:EXT:lang/locallang_general.xlf:LGL.language',
            'config'  => array(
                'type'                => 'select',
                'foreign_table'       => 'sys_language',
                'foreign_table_where' => 'ORDER BY sys_language.title',
                'items'               => array(
                    array('LLL:EXT:lang/locallang_general.xlf:LGL.allLanguages', -1),
                    array('LLL:EXT:lang/locallang_general.xlf:LGL.default_value', 0)
                ),
            ),
        ),
        'l10n_parent'             => array(
            'displayCond' => 'FIELD:sys_language_uid:>:0',
            'exclude'     => 1,
            'label'       => 'LLL:EXT:lang/locallang_general.xlf:LGL.l18n_parent',
            'config'      => array(
                'type'                => 'select',
                'items'               => array(
                    array('', 0),
                ),
                'foreign_table'       => 'tx_extbibsonomycsl_domain_model_authentication',
                'foreign_table_where' => 'AND tx_extbibsonomycsl_domain_model_authentication.pid=###CURRENT_PID### AND tx_extbibsonomycsl_domain_model_authentication.sys_language_uid IN (-1,0)',
            ),
        ),
        'l10n_diffsource'         => array(
            'config' => array(
                'type' => 'passthrough',
            ),
        ),
        't3ver_label'             => array(
            'label'  => 'LLL:EXT:lang/locallang_general.xlf:LGL.versionLabel',
            'config' => array(
                'type' => 'input',
                'size' => 30,
                'max'  => 255,
            )
        ),
        'hidden'                  => array(
            'exclude' => 1,
            'label'   => 'LLL:EXT:lang/locallang_general.xlf:LGL.hidden',
            'config'  => array(
                'type' => 'check',
            ),
        ),
        'starttime'               => array(
            'exclude'   => 1,
            'l10n_mode' => 'mergeIfNotBlank',
            'label'     => 'LLL:EXT:lang/locallang_general.xlf:LGL.starttime',
            'config'    => array(
                'type'     => 'input',
                'size'     => 13,
                'max'      => 20,
                'eval'     => 'datetime',
                'checkbox' => 0,
                'default'  => 0,
                'range'    => array(
                    'lower' => mktime(0, 0, 0, date('m'), date('d'), date('Y'))
                ),
            ),
        ),
        'endtime'                 => array(
            'exclude'   => 1,
            'l10n_mode' => 'mergeIfNotBlank',
            'label'     => 'LLL:EXT:lang/locallang_general.xlf:LGL.endtime',
            'config'    => array(
                'type'     => 'input',
                'size'     => 13,
                'max'      => 20,
                'eval'     => 'datetime',
                'checkbox' => 0,
                'default'  => 0,
                'range'    => array(
                    'lower' => mktime(0, 0, 0, date('m'), date('d'), date('Y'))
                ),
            ),
        ),
        'host_address'            => array(
            'exclude' => 1,
            'label'   => 'LLL:EXT:ext_bibsonomy_csl/Resources/Private/Language/locallang_db.xlf:tx_extbibsonomycsl_domain_model_authentication.host_address',
            'config'  => array(
                'type' => 'input',
                'size' => 60,
                'eval' => 'trim'
            ),
        ),
        'host_user_name'          => array(
            'exclude' => 1,
            'label'   => 'LLL:EXT:ext_bibsonomy_csl/Resources/Private/Language/locallang_db.xlf:tx_extbibsonomycsl_domain_model_authentication.host_user_name',
            'config'  => array(
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ),
        ),
        'host_api_key'            => array(
            'exclude' => 1,
            'label'   => 'LLL:EXT:ext_bibsonomy_csl/Resources/Private/Language/locallang_db.xlf:tx_extbibsonomycsl_domain_model_authentication.host_api_key',
            'config'  => array(
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ),
        ),
        'serialized_access_token' => array(
            'exclude' => 1,
            'label'   => 'LLL:EXT:ext_bibsonomy_csl/Resources/Private/Language/locallang_db.xlf:tx_extbibsonomycsl_domain_model_authentication.serialized_access_token',
            'config'  => array(
                'type' => 'text',
                'cols' => 40,
                'rows' => 15,
                'eval' => 'trim'
            )
        ),
        'enable_o_auth'           => array(
            'exclude' => 1,
            'label'   => 'LLL:EXT:ext_bibsonomy_csl/Resources/Private/Language/locallang_db.xlf:tx_extbibsonomycsl_domain_model_authentication.enable_o_auth',
            'config'  => array(
                'type'    => 'check',
                'default' => 0
            )
        ),
        'create_date'             => array(
            'exclude' => 1,
            'label'   => 'LLL:EXT:ext_bibsonomy_csl/Resources/Private/Language/locallang_db.xlf:tx_extbibsonomycsl_domain_model_authentication.create_date',
            'config'  => array(
                'type'     => 'input',
                'size'     => 10,
                'eval'     => 'datetime',
                'checkbox' => 1,
                'default'  => time()
            ),
        ),

    ),
);
