<?php
/*
 * Copyright (C) 2015 Sebastian Böttger
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPlugin(array(
    'LLL:EXT:ext_bibsonomy_csl/Resources/Private/Language/locallang_db.xml:tt_content.list_type_pi1',
    'rx_extkey_pi1',
    'LLL:EXT:ext_bibsonomy_csl/ext_icon.gif'),
    'list_type', 'ext_bibsonomy_csl'
);