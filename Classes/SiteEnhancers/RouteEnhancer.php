<?php


namespace RouteEnhancer;

use TYPO3\CMS\Extbase\Routing\ExtbasePluginEnhancer;

class RouteEnhancer extends  ExtbasePluginEnhancer
{
    public function __construct()
    {
        // TODO load from .yaml
        // $configuration is array from Configuration/Routing/route_config.yaml
        $configuration = array();
        $configuration['plugin'] = 'Publicationlist';
        $configuration['extension'] = 'Extbibsonomycsl';
        $configuration['defaultController'] = 'Document::view';
        $configuration['defaults']['page'] = '0';
        $configuration['requirements']['page'] = '\d+';
        $preview = array();
        $preview['routePath'] = '/preview/{userName}/{intraHash}/{preview}/{fileName}';
        $preview['_controller'] = 'Document::view';
        $download = array();
        $download['routePath'] = '/download/{userName}/{intraHash}/{fileName}';
        $download['_controller'] = 'Document::download';
        $routes = [$preview, $download];
        $configuration['routes'] = $routes;
        parent::__construct($configuration);
    }
}