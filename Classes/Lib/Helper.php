<?php

/*
 *  PUMA/BibSonomy CSL (ext_bibsonomy_csl) is a TYPO3 extension which
 *  enables users to render publication lists from PUMA or BibSonomy in
 *  various styles.
 *
 *  Copyright notice
 *  (c) 2015 Sebastian Böttger <seboettg@gmail.com>
 *
 *  HothoData GmbH (http://www.academic-puma.de)
 *  Knowledge and Data Engineering Group (University of Kassel)
 *
 *  All rights reserved
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace AcademicPuma\ExtBibsonomyCsl\Lib;


use AcademicPuma\ExtBibsonomyCsl\Domain\Exception\InvalidStylesheetException;
use AcademicPuma\ExtBibsonomyCsl\Domain\Model\Authentication;
use AcademicPuma\ExtBibsonomyCsl\Domain\Model\CitationStylesheet;
use AcademicPuma\RestClient\Model\Posts;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * Helper Class
 *
 * @package ext_bibsonomy_csl
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 * @author Sebastian Böttger
 */
class Helper
{

    const URL_PATTERN = '#((https?|ftp):\/\/(\S*?\.\S*?))([\s)\[\]{},;"\':<]|\.\s|$)#i';

    /**
     * creates list of citation stylesheet items for the flexform in frontend
     * plugin.
     *
     * @param array $config
     *
     * @return array
     */
    public function user_getLayouts($config)
    {

        global $GLOBALS;
        $userId = $GLOBALS["BE_USER"]->user["uid"];

// TODO REMOVE 
// $select_fields, $from_table, $where_clause, $groupBy = '', $orderBy = '', $limit = ''
//         $defaultResult = $GLOBALS['TYPO3_DB']->exec_SELECTquery('id,title',
//             'tx_extbibsonomycsl_domain_model_citationstylesheet', '`cruser_id` = 0 AND `deleted` = 0', '',
//             'cruser_id ASC', '');
        
        $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)->getQueryBuilderForTable("tx_extbibsonomycsl_domain_model_citationstylesheet");
        $queryBuilder->select('id', 'title')
            ->from('tx_extbibsonomycsl_domain_model_citationstylesheet')
            ->where(
                $queryBuilder->expr()->eq('cruser_id', 0),
                $queryBuilder->expr()->eq('deleted', 0)
            )
            ->orderBy('cruser_id', 'ASC');
        
        
        $i = 0;

        // TODO REMOVE 
        //while ($row = $defaultResult->fetch_row()) {
        $statement = $queryBuilder->execute();
        while ($row = $statement->fetch()) {
            $config['items'][$i++] = array($row["title"], $row["id"]);
        }

// TODO REMOVE 
//         $result = $GLOBALS['TYPO3_DB']->exec_SELECTquery('id,title',
//             'tx_extbibsonomycsl_domain_model_citationstylesheet', '`cruser_id` = ' . $userId . ' AND `deleted` = 0', '',
//             'cruser_id ASC', '');
        
        $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)->getQueryBuilderForTable("tx_extbibsonomycsl_domain_model_citationstylesheet");
        $queryBuilder->select('id', 'title')
        ->from('tx_extbibsonomycsl_domain_model_citationstylesheet')
        ->where(
            $queryBuilder->expr()->eq('cruser_id', $userId),
            $queryBuilder->expr()->eq('deleted', 0)
            )
            ->orderBy('cruser_id', 'ASC');
        
        $result = $statement = $queryBuilder->execute()->fetchAll();
        if (count($result) > 0) {
            $config['items'][$i++] = array('---------------------', '');
            foreach ($result as $row) {
                $config['items'][$i++] = array($row["title"], $row["id"]);
            }
        }

        // TODO REMOVE 
        //$result->close();

        return $config;
    }

    /**
     *
     * @param array $config
     *
     * @return array
     */
    public function user_getLocales($config)
    {

        $path   = \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('ext_bibsonomy_csl') . 'vendor/academicpuma/locales';
        $handle = opendir($path);
        if ($handle) {
            $i = 0;
            while (false !== ($file = readdir($handle))) {
                if (false !== strpos($file, 'xml')) {
                    $lang                  = substr($file, 8, 5);
                    $config['items'][$i++] = array($lang, $lang);
                }
            }
            closedir($handle);
        }

        asort($config['items']);
        return $config;
    }

    public function user_getHosts($config)
    {

        $confArray = unserialize($GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf']['ext_bibsonomy_csl']);

        $serverListUrl = $confArray['instanceListUrl'];

        $ch       = curl_init();
        $header[] = 'Cache-Control: no-transform,public,max-age=86400,s-maxage=259200';

        curl_setopt($ch, CURLOPT_URL, $serverListUrl);
        curl_setopt($ch, CURLOPT_VERBOSE, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        $responseBody = curl_exec($ch);

        curl_close($ch);

        $serverList = json_decode($responseBody);

        $i = 0;
        $config['items'][$i] = array('BibSonomy', 'https://www.bibsonomy.org/');

        foreach ($serverList->server as $server) {
            $config['items'][++$i] = array($server->instanceName, $server->instanceUrl);
        }

        return $config;
    }

    public function getHosts()
    {

        $res = [];
        $arr = $this->user_getHosts([]);
        foreach ($arr['items'] as $item) {
            $host        = new \stdClass();
            $host->key   = $item[1];
            $host->value = $item[0];
            $res[]       = $host;
        }

        return $res;
    }

    /**
     * @param array $config
     *
     * @return array
     */
    public function user_getAuth($config)
    {

        global $GLOBALS;
        $userId = $GLOBALS["BE_USER"]->user["uid"];

        // TODO REMOVE
        //$select_fields, $from_table, $where_clause, $groupBy = '', $orderBy = '', $limit = ''
//         $result = $GLOBALS['TYPO3_DB']->exec_SELECTquery('host_address, host_user_name,host_api_key,enable_o_auth,uid',
//             'tx_extbibsonomycsl_domain_model_authentication', "`cruser_id` = '" . $userId . "'", '', 'cruser_id', '');
        
        
        $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)->getQueryBuilderForTable("tx_extbibsonomycsl_domain_model_citationstylesheet");
        $queryBuilder->select('host_address', 'host_user_name', 'host_api_key', 'enable_o_auth', 'uid')
        ->from('tx_extbibsonomycsl_domain_model_authentication')
        ->where(
            $queryBuilder->expr()->eq('cruser_id', $userId),
            $queryBuilder->expr()->eq('deleted', 0)
            )
            ->orderBy('cruser_id', 'ASC');
            
        $result = $queryBuilder->execute();
        
        $i = 0;
        while ($row = $result->fetch()) {
            if ($row["enable_o_auth"] == 0) {
                $config['items'][$i++] = array($row["host_address"] . ": " . $row["host_user_name"] . " (API KEY)", $row["uid"]);
            } else {
                $config['items'][$i++] = array($row["host_address"] . ": " . $row["host_user_name"] . " (OAuth)", $row["uid"]);
            }
        }

        // TODO REMOVE
        //$result->close();

        return $config;
    }

    /**
     * Creates an object of type Tx_ExtBibsonomyCsl_Domain_Model_CitationStylesheet
     * from the given xml code and PID. The xml code will be parsed for a
     * "title"-Tag and a "id"-Tag.
     *
     * @param string $xmlSource
     * @param integer $pid
     *
     * @return CitationStylesheet
     * @throws InvalidStylesheetException
     */
    public static function createStylesheetObject($xmlSource, $pid = 0)
    {

        $xml = new \DOMDocument();
        if (!$xml->loadXML($xmlSource)) {
            throw new InvalidStylesheetException('Stylesheet is not valid');
        }
        $title = $xml->getElementsByTagName("title")->item(0)->nodeValue;
        $id    = $xml->getElementsByTagName("id")->item(0)->nodeValue;

        if (empty($title) || empty($id)) {
            throw new InvalidStylesheetException('Stylesheet is not valid: Title and ID missing.');
        }

        $stylesheet = GeneralUtility::makeInstance('AcademicPuma\\ExtBibsonomyCsl\\Domain\\Model\\CitationStylesheet');
        $stylesheet->setTitle($title);
        $stylesheet->setId($id);
        $stylesheet->setXmlSource($xmlSource);
        $stylesheet->setPid($pid);

        return $stylesheet;
    }

    public static function createAuthenticationObject($hostUserName, $hostApiKey)
    {

        /** @var Authentication $authentication */
        $authentication = GeneralUtility::makeInstance('AcademicPuma\\ExtBibsonomyCsl\\Domain\\Model\\Authentication');
        $authentication->setHostUserName($hostUserName);
        $authentication->setHostApiKey($hostApiKey);
        $authentication->setEnableOAuth(false);
        $authentication->setSerializedAccessToken('');
        $authentication->setAccessTokenCreateDate(new \DateTime());
        $authentication->setPid(0);

        return $authentication;
    }

    /**
     * Opens and reads a file into a variable. Then, the charset will be checked
     * and adjusted if necessary. It returns the variable.
     *
     * @param string $filename name with path to the file
     * @param string $charset = 'UTF-8'
     *
     * @return string
     */
    public static function getDataFromCSLFile($filename, $charset = 'UTF-8')
    {

        //debug($filename);
        if (floatval(phpversion()) >= 4.3) {
            $data = file_get_contents($filename);
        } else {
            if (!file_exists($filename)) {
                return -3;
            }

            $filehandle = fopen($filename, 'r');

            if (!$filehandle) {
                return -2;
            }

            $data = '';

            while (!feof($filehandle)) {

                $data .= fread($filehandle, filesize($filename));
            }

            fclose($filehandle);
        }

        if (($encoding = mb_detect_encoding($data, 'auto', true)) != $charset) {

            $data = mb_convert_encoding($data, $charset, $encoding);
        }

        return $data;
    }

    /**
     * replaces all characters that are not a-z, A-Z, 0-9 with
     * underscore character _
     *
     * @param string $string
     *
     * @return string
     */
    public static function replaceSpecialCharacters($string)
    {

        return preg_replace(array("![,./\ ]!", "![^a-zA-Z0-9]!"), "_", $string);
    }

    /**
     *
     * expects author stdClass with attribute family (for lastname) and given for (given name)
     *
     * @param array <stdClass> $authors
     *
     * @return string
     */
    public static function concatAuthors(array $authors)
    {

        $str = "";

        foreach ($authors as $author) {
            $str .= $author->family . ", " . $author->given . '; ';
        }

        return $str;
    }

    /**
     * Debugs a SQL query from a QueryResult
     *
     * @param \TYPO3\CMS\Extbase\Persistence\Generic\QueryResult $queryResult
     * @param boolean $explainOutput
     *
     * @return void
     */
    // TODO REMOVE
    public static function debugQuery(
        \TYPO3\CMS\Extbase\Persistence\Generic\QueryResult $queryResult,
        $explainOutput = false
    ) {

        $GLOBALS['TYPO3_DB']->debugOuput = 2;
        if ($explainOutput) {
            $GLOBALS['TYPO3_DB']->explainOutput = true;
        }
        $GLOBALS['TYPO3_DB']->store_lastBuiltQuery = true;
        $queryResult->toArray();
        //DebuggerUtility::var_dump();
        echo "<pre>";
        print_r($GLOBALS['TYPO3_DB']->debug_lastBuiltQuery);
        echo "</pre>";
        $GLOBALS['TYPO3_DB']->store_lastBuiltQuery = false;
        $GLOBALS['TYPO3_DB']->explainOutput        = false;
        $GLOBALS['TYPO3_DB']->debugOuput           = false;
    }

    /**
     * Cleans up a string containing LaTeX markup
     *
     * @param string $bibtex a bibtex string
     *
     * @return string
     */
    public static function cleanBibTex($bibtex)
    {

        if (empty($bibtex)) {
            return "";
        }

        $pattern = '/\{([^\}]+)/';
        // replace markup
        //$bibtex = preg_replace($pattern, "\1", $bibtex);  // \\markup{marked_up_text}

        //TODO: decode Latex macros into unicode characters
        return trim($bibtex);
    }

    public static function generateListHash(Posts $posts)
    {

        $titles = '';
        foreach ($posts as $post) {
            $titles .= $post->getResource()->getTitle();
        }

        return md5($titles);
    }

    /**
     * @param $string
     *
     * @return bool
     * @throws \Exception
     */
    public static function isUrl($string)
    {

        $match = preg_match(self::URL_PATTERN, $string);

        if ($match === false) {
            throw new \Exception('Invalid regular expression');
        }

        return ($match > 0) ? true : false;
    }
}
