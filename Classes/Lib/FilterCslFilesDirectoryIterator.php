<?php

/*
 *  PUMA/BibSonomy CSL (ext_bibsonomy_csl) is a TYPO3 extension which
 *  enables users to render publication lists from PUMA or BibSonomy in
 *  various styles.
 *
 *  Copyright notice
 *  (c) 2015 Sebastian Böttger <seboettg@gmail.com>
 *
 *  HothoData GmbH (http://www.academic-puma.de)
 *  Knowledge and Data Engineering Group (University of Kassel)
 *
 *  All rights reserved
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace AcademicPuma\ExtBibsonomyCsl\Lib;

use AcademicPuma\RestClient\Exceptions\FileNotFoundException;

/**
 * Short description
 *
 * @since 03.08.15
 * @author Sebastian Böttger / boettger@cs.uni-kassel.de
 */
class FilterCslFilesDirectoryIterator extends \FilterIterator
{

    /**
     * Regex storage
     *
     * @var string
     */
    private $regex;

    /**
     * Creates new AdvancedDirectoryIterator
     *
     * @param string $path , prefix with '-R ' for recursive, postfix with /[wildcards] for matching
     * @param int $flags
     *
     * @throws FileNotFoundException
     */
    public function  __construct($path, $flags = 0)
    {

        $file = '';
        if (preg_match('/^(.+\/)([^\/]+\.csl)$/', $path, $pathParts)) {
            $path = $pathParts[1];
            $file = $pathParts[2];
        }
        if (!file_exists($path)) {
            throw new FileNotFoundException($path);
        }
        if (!empty($file)) {
            if (preg_match('~/?([^/]*\*[^/]*)$~', $file, $matches)) { // matched wildcards in filename
                $this->regex = '~^' . str_replace('*', '.*',
                        str_replace('.', '\.', $matches[1])) . '$~'; // convert wildcards to regex
            }
        } else {
            $this->regex = '~.*\.csl$~';
        }

        parent::__construct(new \DirectoryIterator($path));
    }

    /**
     * Checks for regex in current filename, or matches all if no regex specified
     *
     * @return bool
     */
    public function accept()
    { // FilterIterator method
        return $this->regex === null ? true : preg_match($this->regex, $this->getInnerIterator()->getFilename());
    }
}
