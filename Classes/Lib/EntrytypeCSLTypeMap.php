<?php

/*
 *  PUMA/BibSonomy CSL (ext_bibsonomy_csl) is a TYPO3 extension which
 *  enables users to render publication lists from PUMA or BibSonomy in
 *  various styles.
 *
 *  Copyright notice
 *  (c) 2015 Sebastian Böttger <seboettg@gmail.com>
 *
 *  HothoData GmbH (http://www.academic-puma.de)
 *  Knowledge and Data Engineering Group (University of Kassel)
 *
 *  All rights reserved
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace AcademicPuma\ExtBibsonomyCsl\Lib;

use AcademicPuma\RestClient\Config\CSLType;
use AcademicPuma\RestClient\Config\Entrytype;
use AcademicPuma\RestClient\Util\EntrytypeMapper;

/**
 * Short description
 *
 * @since 10/08/15
 * @author Sebastian Böttger / boettger@cs.uni-kassel.de
 */
class EntrytypeCSLTypeMap extends EntrytypeMapper
{

    public function __construct()
    {

        $this->default = "misc";
    }

    protected $typeMap = [

        //Articles
        CSLType::ARTICLE           => Entrytype::ARTICLE,
        CSLType::ARTICLE_JOURNAL   => Entrytype::ARTICLE,
        CSLType::ARTICLE_MAGAZINE  => Entrytype::ARTICLE,
        CSLType::ARTICLE_NEWSPAPER => Entrytype::ARTICLE,
        //Books
        CSLType::BOOK              => Entrytype::BOOK,
        CSLType::PAMPHLET          => Entrytype::BOOKLET,
        CSLType::CHAPTER           => Entrytype::INBOOK,
        CSLType::PAPER_CONFERENCE  => Entrytype::CONFERENCE,
        CSLType::THESIS            => Entrytype::PHDTHESIS,
        CSLType::REPORT            => Entrytype::TECHREPORT,
        CSLType::PATENT            => Entrytype::PATENT,
        CSLType::WEBPAGE           => Entrytype::ELECTRONIC,
        CSLType::LEGISLATION       => Entrytype::STANDARD,
        CSLType::MANUSCRIPT        => Entrytype::PREPRINT
    ];
}