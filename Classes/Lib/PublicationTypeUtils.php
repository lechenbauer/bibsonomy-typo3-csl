<?php

/*
 *  PUMA/BibSonomy CSL (ext_bibsonomy_csl) is a TYPO3 extension which
 *  enables users to render publication lists from PUMA or BibSonomy in
 *  various styles.
 *
 *  Copyright notice
 *  (c) 2015 Sebastian Böttger <seboettg@gmail.com>
 *
 *  HothoData GmbH (http://www.academic-puma.de)
 *  Knowledge and Data Engineering Group (University of Kassel)
 *
 *  All rights reserved
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace AcademicPuma\ExtBibsonomyCsl\Lib;


use AcademicPuma\RestClient\Config\Entrytype;

/**
 * 08.03.2014
 *
 * Description of PublicationTypeUtils:
 *
 * - Mapping for Entrytype (BibTeX) <=> Type (CSL)
 * - Default order for csl types
 *
 * @package ext_bibsonomy_csl
 * @author Sebastian Böttger <boettger@cs.uni-kassel.de>
 */
class PublicationTypeUtils
{

    /**
     * disable constructor
     */
    private function __construct()
    {
    }

    private static $typeMap = [
        Entrytype::BOOK          => 'Book (Book or Conference Proceedings)',
        Entrytype::ARTICLE       => 'Article (in journal, newspaper, or magazine)',
        Entrytype::INPROCEEDINGS => 'Article in Conference Proceedings',
        Entrytype::PHDTHESIS     => 'Thesis (PhD Thesis and Master Thesis)',
        Entrytype::INCOLLECTION  => 'Chapter (part of a book)',
        Entrytype::TECHREPORT    => 'Report',
        Entrytype::PATENT        => 'Patent',
        Entrytype::STANDARD      => 'Legislation',
        Entrytype::BOOKLET       => 'Pamphlet',
        Entrytype::PRESENTATION  => 'Presentation',
        Entrytype::ELECTRONIC    => 'Web page',
        Entrytype::PREPRINT      => 'Manuscript',
        Entrytype::UNPUBLISHED   => 'Unpublished',
        Entrytype::MISC          => 'Misc'
    ];

    public static $defaultTypeOrder = [
        Entrytype::BOOK,
        Entrytype::ARTICLE,
        Entrytype::INPROCEEDINGS,
        Entrytype::PHDTHESIS,
        Entrytype::INCOLLECTION,
        Entrytype::TECHREPORT,
        Entrytype::PATENT,
        Entrytype::STANDARD,
        Entrytype::BOOKLET,
        Entrytype::PRESENTATION,
        Entrytype::ELECTRONIC,
        Entrytype::PREPRINT,
        Entrytype::UNPUBLISHED,
        Entrytype::MISC
    ];

    public static $typeMapExtend = [
        Entrytype::BOOK          => [Entrytype::BOOK, Entrytype::PROCEEDINGS, Entrytype::PERIODICAL],
        Entrytype::ARTICLE       => [Entrytype::ARTICLE],
        Entrytype::INPROCEEDINGS => [Entrytype::INPROCEEDINGS, Entrytype::CONFERENCE],
        Entrytype::PHDTHESIS     => [Entrytype::PHDTHESIS, Entrytype::MASTERTHESIS],
        Entrytype::INCOLLECTION  => [Entrytype::INCOLLECTION, Entrytype::INBOOK],
        Entrytype::TECHREPORT    => [Entrytype::TECHREPORT],
        Entrytype::PATENT        => [Entrytype::PATENT],
        Entrytype::STANDARD      => [Entrytype::STANDARD],
        Entrytype::BOOKLET       => [Entrytype::BOOKLET],
        Entrytype::PRESENTATION  => [Entrytype::PRESENTATION],
        Entrytype::ELECTRONIC    => [Entrytype::ELECTRONIC],
        Entrytype::PREPRINT      => [Entrytype::PREPRINT],
        Entrytype::UNPUBLISHED   => [Entrytype::UNPUBLISHED],
        Entrytype::MISC          => [Entrytype::MISC]
    ];

    private static $typeMapLikeCSL = [
        Entrytype::BOOK          => Entrytype::BOOK,
        Entrytype::PROCEEDINGS   => Entrytype::BOOK,
        Entrytype::PERIODICAL    => Entrytype::BOOK,
        Entrytype::ARTICLE       => Entrytype::ARTICLE,
        Entrytype::INPROCEEDINGS => Entrytype::INPROCEEDINGS,
        Entrytype::CONFERENCE    => Entrytype::INPROCEEDINGS,
        Entrytype::PHDTHESIS     => Entrytype::PHDTHESIS,
        Entrytype::MASTERTHESIS  => Entrytype::PHDTHESIS,
        Entrytype::INCOLLECTION  => Entrytype::INCOLLECTION,
        Entrytype::INBOOK        => Entrytype::INCOLLECTION,
        Entrytype::TECHREPORT    => Entrytype::TECHREPORT,
        Entrytype::PATENT        => Entrytype::PATENT,
        Entrytype::STANDARD      => Entrytype::STANDARD,
        Entrytype::BOOKLET       => Entrytype::BOOKLET,
        Entrytype::PRESENTATION  => Entrytype::PRESENTATION,
        Entrytype::ELECTRONIC    => Entrytype::ELECTRONIC,
        Entrytype::PREPRINT      => Entrytype::PREPRINT,
        Entrytype::UNPUBLISHED   => Entrytype::UNPUBLISHED,
        Entrytype::MISC          => Entrytype::MISC
    ];

    public static function getTypeOfType($type)
    {

        return self::$typeMapLikeCSL[$type];
    }

    public static function isBibTexType($type)
    {

        return array_key_exists($type, self::$typeMapLikeCSL);
    }

    public static function getTypeTitle($type)
    {

        return self::$typeMap[self::getTypeOfType($type)];
    }
}
