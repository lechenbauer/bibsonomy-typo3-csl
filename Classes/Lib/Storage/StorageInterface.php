<?php

/*
 *  PUMA/BibSonomy CSL (ext_bibsonomy_csl) is a TYPO3 extension which
 *  enables users to render publication lists from PUMA or BibSonomy in
 *  various styles.
 *
 *  Copyright notice
 *  (c) 2015 Sebastian Böttger <seboettg@gmail.com>
 *
 *  HothoData GmbH (http://www.academic-puma.de)
 *  Knowledge and Data Engineering Group (University of Kassel)
 *
 *  All rights reserved
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace AcademicPuma\ExtBibsonomyCsl\Lib\Storage;

use TYPO3\CMS\Core\SingletonInterface;

interface StorageInterface extends SingletonInterface
{

    /**
     * @param mixed $data
     */
    public function isSerializable($data);

    /**
     * @param string $key
     * @param string $type
     */
    public function read($key, $type = '');

    /**
     * @param string $key
     * @param mixed $data
     * @param string $type
     */
    public function write($key, $data, $type = '');

    /**
     * @param string $key
     * @param string $type
     */
    public function remove($key, $type = '');

    /**
     * @param string $key
     * @param string $type
     */
    public function has($key, $type = '');

    /**
     * @param object $object
     * @param string $key
     * @param string $type
     */
    public function storeObject($object, $key = null, $type = '');

    /**
     * @param string
     *
     * @return mixed
     */
    public function getObject($key, $type = '');

    /**
     * @return object
     */
    public function getUser();
}