<?php

/*
 *  PUMA/BibSonomy CSL (ext_bibsonomy_csl) is a TYPO3 extension which
 *  enables users to render publication lists from PUMA or BibSonomy in
 *  various styles.
 *
 *  Copyright notice
 *  (c) 2015 Sebastian Böttger <seboettg@gmail.com>
 *
 *  HothoData GmbH (http://www.academic-puma.de)
 *  Knowledge and Data Engineering Group (University of Kassel)
 *
 *  All rights reserved
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace AcademicPuma\ExtBibsonomyCsl\Lib\Storage;

use TYPO3\CMS\Extbase\Object\ObjectManagerInterface;

/**
 * Short description
 *
 * @since 11/09/15
 * @author Sebastian Böttger / boettger@cs.uni-kassel.de
 */
class SessionStorage implements StorageInterface
{

    /**
     *
     * @var StorageInterface
     */
    protected $concreteSessionManager = null;
    /**
     *
     * @var ObjectManagerInterface
     */
    protected $objectManager = null;

    /**
     * @param ObjectManagerInterface $objectManager
     *
     * @return void
     */
    public function injectObjectManager(ObjectManagerInterface $objectManager)
    {

        $this->objectManager = $objectManager;
        $this->initializeConcreteSessionManager();
    }

    /**
     * @return void
     */
    protected function initializeConcreteSessionManager()
    {

        if (TYPO3_MODE === 'FE') {

            $this->concreteSessionManager = $this->objectManager->get('AcademicPuma\\ExtBibsonomyCsl\\Lib\\Storage\\FrontendSessionStorage');
        } else {
            $this->concreteSessionManager = $this->objectManager->get('AcademicPuma\\ExtBibsonomyCsl\\Lib\\Storage\\BackendSessionStorage');
        }
    }

    /**
     * @param mixed $data
     */
    public function isSerializable($data)
    {

        return $this->concreteSessionManager->isSerializable($data);
    }

    /**
     * @param string $key
     * @param string $type
     */
    public function read($key, $type = '')
    {

        return $this->concreteSessionManager->read($key, $type);
    }

    /**
     * @param string $key
     * @param mixed $data
     * @param string $type
     */
    public function write($key, $data, $type = '')
    {

        $this->concreteSessionManager->write($key, $data, $type);
    }

    /**
     * @param string $key
     * @param string $type
     */
    public function remove($key, $type = '')
    {

        $this->concreteSessionManager->remove($key, $type);
    }

    /**
     * @param string $key
     * @param string $type
     */
    public function has($key, $type = '')
    {

        return $this->concreteSessionManager->has($key, $type);
    }

    /**
     * @param object $object
     * @param string $key
     * @param string $type
     */
    public function storeObject($object, $key = null, $type = '')
    {

        $this->concreteSessionManager->storeObject($object, $key, $type);
    }

    /**
     * @param string
     *
     * @return mixed
     */
    public function getObject($key, $type = '')
    {

        return $this->concreteSessionManager->getObject($key, $type);

    }

    /**
     * @return object
     */
    public function getUser()
    {

        return $this->concreteSessionManager->getUser();
    }
}