<?php

/*
 *  PUMA/BibSonomy CSL (ext_bibsonomy_csl) is a TYPO3 extension which
 *  enables users to render publication lists from PUMA or BibSonomy in
 *  various styles.
 *
 *  Copyright notice
 *  (c) 2015 Sebastian Böttger <seboettg@gmail.com>
 *
 *  HothoData GmbH (http://www.academic-puma.de)
 *  Knowledge and Data Engineering Group (University of Kassel)
 *
 *  All rights reserved
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace AcademicPuma\ExtBibsonomyCsl\Lib\Storage;

/**
 * Short description
 *
 * @since 11/09/15
 * @author Sebastian Böttger / boettger@cs.uni-kassel.de
 */
abstract class AbstractSessionStorage implements StorageInterface
{

    /**
     * @var string
     */
    protected $sessionNamespace = 'ext_bibsonomy_csl';

    /**
     * Check whether the data is serializable or not
     *
     * @param mixed $data
     *
     * @return boolean
     */
    public function isSerializable($data)
    {

        if (is_object($data) || is_array($data)) {
            return true;
        }

        return false;
    }

    /**
     *
     * @param string $key
     *
     * @return string
     */
    protected function getKey($key)
    {

        return $this->sessionNamespace . $key;
    }

    /**
     * Writes a object to the session if the key is empty it used the classname
     *
     * @param object $object
     * @param string $key
     * @param string $type
     *
     * @throws InvalidArgumentException
     */
    public function storeObject($object, $key = null, $type = 'ses')
    {

        if (is_null($key)) {
            $key = get_class($object);
        }
        if ($this->isSerializable($object)) {
            $this->write($key, serialize($object), $type);
        } else {
            throw new InvalidArgumentException(sprintf('The object %s is not serializable.', get_class($object)));
        }
    }

    /**
     * Writes something to storage
     *
     * @param string $key
     *
     * @return  object
     */
    public function getObject($key, $type = 'ses')
    {

        return unserialize($this->read($key, $type));
    }

    /**
     *
     * @return string
     */
    public function getSessionNamespace()
    {

        return $this->sessionNamespace;
    }

    /**
     *
     * @param string $sessionNamespace
     */
    public function setSessionNamespace($sessionNamespace)
    {

        $this->sessionNamespace = $sessionNamespace;
    }
}