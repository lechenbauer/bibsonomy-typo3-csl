<?php

/*
 *  PUMA/BibSonomy CSL (ext_bibsonomy_csl) is a TYPO3 extension which
 *  enables users to render publication lists from PUMA or BibSonomy in
 *  various styles.
 *
 *  Copyright notice
 *  (c) 2015 Sebastian Böttger <seboettg@gmail.com>
 *
 *  HothoData GmbH (http://www.academic-puma.de)
 *  Knowledge and Data Engineering Group (University of Kassel)
 *
 *  All rights reserved
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace AcademicPuma\ExtBibsonomyCsl\Lib\Storage;

use TYPO3\CMS\Core\Authentication\BackendUserAuthentication;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Persistence\Generic\Exception\RepositoryException;

/**
 * Short description
 *
 * @since 11/09/15
 * @author Sebastian Böttger / boettger@cs.uni-kassel.de
 */
class BackendSessionStorage extends AbstractSessionStorage
{

    /**
     * Get session data
     *
     * @param string $key
     * @param string $type
     *
     * @return mixed
     */
    public function read($key, $type = '')
    {

        return $this->getBeUser()->getSessionData($this->getKey($key));
    }

    /**
     * Write data to session
     *
     * @param string $key
     * @param mixed $data
     *
     * @return void
     */
    public function write($key, $data, $type = '')
    {

        $this->getBeUser()->setAndSaveSessionData($this->getKey($key), $data);
    }

    /**
     * Remove data from session
     *
     * @param string $key
     * @param string $type
     */
    public function remove($key, $type = '')
    {

        if ($this->has($key, $type)) {
            $sesDat = $this->read($key, $type);
            //unset($sesDat[$this->getKey($key)]);
            $this->getBeUser()->user['ses_data'] = (!empty($sesDat) ? serialize($sesDat) : '');
            
            
            $db = GeneralUtility::makeInstance(ConnectionPool::class)->getConnectionForTable($this->getBeUser()->session_table);
            $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)->getQueryBuilderForTable($this->getBeUser()->session_table);
            try {
                $db->update(
                    $this->getBeUser()->session_table,
                    array('ses_data' => $this->getBeUser()->user['ses_data']),
                    ['ses_id' => $queryBuilder->createNamedParameter($this->getBeUser()->user['ses_id'], $this->getBeUser()->session_table)]
                    );
            } catch (\Doctrine\DBAL\Connection $e) {
                throw new RepositoryException('Could not update.');
            }
            
            
            
            // TODO REMOVE
//             $GLOBALS['TYPO3_DB']->exec_UPDATEquery($this->getBeUser()->session_table,
//                 'ses_id=' . $GLOBALS['TYPO3_DB']->fullQuoteStr($this->getBeUser()->user['ses_id'], $this->getBeUser()->session_table),   
//                 array('ses_data' => $this->getBeUser()->user['ses_data']));

        }
    }

    /**
     * Has key in session or not
     *
     * @param string $key
     * @param string $type
     *
     * @return bool
     */
    public function has($key, $type = '')
    {

        $sesDat = unserialize($this->getBeUser()->user['ses_data']);

        return isset($sesDat[$this->getKey($key)]) ? true : false;
    }

    /**
     *
     * @return BackendUserAuthentication
     */
    protected function getBeUser()
    {

        return $GLOBALS['BE_USER'];
    }

    /**
     *
     * @return BackendUserAuthentication
     */
    public function getUser()
    {

        return $this->getBeUser();
    }

}
