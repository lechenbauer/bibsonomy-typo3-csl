<?php

/*
 *  PUMA/BibSonomy CSL (ext_bibsonomy_csl) is a TYPO3 extension which
 *  enables users to render publication lists from PUMA or BibSonomy in
 *  various styles.
 *
 *  Copyright notice
 *  (c) 2015 Sebastian Böttger <seboettg@gmail.com>
 *
 *  HothoData GmbH (http://www.academic-puma.de)
 *  Knowledge and Data Engineering Group (University of Kassel)
 *
 *  All rights reserved
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace AcademicPuma\ExtBibsonomyCsl\Lib;

use AcademicPuma\RestClient\Config\Sorting;
use AcademicPuma\RestClient\Model;
use AcademicPuma\RestClient\Util\Collection\PostComparator;

/**
 * Short description
 *
 * @since 17.12.15
 * @author Sebastian Böttger / boettger@cs.uni-kassel.de
 */
class PostYearComparatorAndFilter extends PostComparator implements FilterComparator
{

    /**
     * @type int
     */
    protected $yearFilter;

    /**
     * @param string $sortingKey
     * @param string $sortingOrder
     * @param array $userDefinedTypeOrder
     * @param integer $yearFilter
     */
    public function __construct(
        $sortingKey,
        $sortingOrder = Sorting::ORDER_ASC,
        $userDefinedTypeOrder = [],
        $yearFilter
    ) {

        parent::__construct('year', $sortingOrder, $userDefinedTypeOrder);
        $this->yearFilter = $yearFilter;
    }

    public function filter(Model\Post $post)
    {

        if (!($post->getResource() instanceof Model\Bibtex)) {
            throw new \BadFunctionCallException('Resource not an instance of Bibtex', 20151217);
        }

        return $post->getResource()->getYear() === $this->yearFilter ? true : false;
    }

}