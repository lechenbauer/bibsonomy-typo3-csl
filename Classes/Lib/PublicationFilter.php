<?php

/*
 *  PUMA/BibSonomy CSL (ext_bibsonomy_csl) is a TYPO3 extension which
 *  enables users to render publication lists from PUMA or BibSonomy in
 *  various styles.
 *
 *  Copyright notice
 *  (c) 2015 Sebastian Böttger <seboettg@gmail.com>
 *
 *  HothoData GmbH (http://www.academic-puma.de)
 *  Knowledge and Data Engineering Group (University of Kassel)
 *
 *  All rights reserved
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace AcademicPuma\ExtBibsonomyCsl\Lib;

use AcademicPuma\RestClient\Model\Post;
use AcademicPuma\RestClient\Model\Posts;

/**
 * Short description
 *
 * @since 17.12.15
 * @author Sebastian Böttger / boettger@cs.uni-kassel.de
 */
class PublicationFilter
{

    /**
     * @param Posts $posts
     * @param FilterComparator $filterComparator
     */
    public static function filterPublicationsByYear(Posts &$posts, FilterComparator $filterComparator)
    {

        /** @var Post $post */
        foreach (array_keys($posts->toArray()) as $key) {
            $post = $posts[$key];
            if (!$filterComparator->filter($post)) {
                $posts->remove($key);
            }
        }
    }

    /**
     * @param Posts $posts
     * @param array $settings
     * @param $userDefinedTypeOrder
     */
    public static function filterPublicationsByType(Posts &$posts, array $settings, $userDefinedTypeOrder)
    {

        if (!self::areBibtexTypes($userDefinedTypeOrder)) {
            $userDefinedTypeOrder = self::transFormUserDefinedTypeOrderToBibtex($userDefinedTypeOrder); // necessary for backward compatibility 1.5.x
        }

        $tvalidTypes          = $userDefinedTypeOrder;
        $filteredPublications = [];
        for ($i = 0; $i < count($posts); ++$i) {

            $entrytype = $posts[$i]->getResource()->getEntrytype();

            $type = PublicationTypeUtils::getTypeOfType($entrytype);

            if (in_array($type, $tvalidTypes)) {
                $filteredPublications[$type][] = $posts[$i];
            }
        }

        $posts->replace([]);
        foreach (array_keys($filteredPublications) as $group) {
            $subList = new Posts($filteredPublications[$group]);
            $subList->sort($settings['bib_sorting'], $settings['bib_sorting_order']);
            $posts->add($group, $subList);
        }
    }

    private static function areBibtexTypes($types)
    {

        foreach ($types as $type) {
            if (!PublicationTypeUtils::isBibTexType($type)) {
                return false;
            }
        }

        return true;
    }

    private function transFormUserDefinedTypeOrderToBibtex($userDefinedTypeOrder)
    {

        $newUserDefinedTypeOrder = [];
        $map                     = new EntrytypeCSLTypeMap();
        foreach ($userDefinedTypeOrder as $cslType) {
            $newUserDefinedTypeOrder[] = $map->getEntrytype($cslType);
        }

        return $newUserDefinedTypeOrder;

    }
}