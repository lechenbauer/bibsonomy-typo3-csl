<?php

/*
 *  PUMA/BibSonomy CSL (ext_bibsonomy_csl) is a TYPO3 extension which
 *  enables users to render publication lists from PUMA or BibSonomy in
 *  various styles.
 *
 *  Copyright notice
 *  (c) 2015 Sebastian Böttger <seboettg@gmail.com>
 *
 *  HothoData GmbH (http://www.academic-puma.de)
 *  Knowledge and Data Engineering Group (University of Kassel)
 *
 *  All rights reserved
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace AcademicPuma\ExtBibsonomyCsl\ViewHelpers;


use AcademicPuma\RestClient\Model\Post;
use AcademicPuma\RestClient\Model\Posts;
use TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper;
use TYPO3Fluid\Fluid\Core\Rendering\RenderingContextInterface;

require_once \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('ext_bibsonomy_csl') . 'vendor/autoload.php';

/**
 *
 *
 * @package ext_bibsonomy_csl
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 * @author Kevin Choong <kch@cs.uni-kassel.de>
 */
class PublicationFilterViewHelper extends AbstractViewHelper
{

    public function initializeArguments()
    {
        $this->registerArgument('rawPostsMap', 'mixed', 'Raw post map object', true);
    }

    static public function renderStatic(array $arguments, \Closure $renderChildrenClosure, RenderingContextInterface $renderingContext)
    {
        $posts = $arguments['rawPostsMap'];
        $titles = array();
        foreach ($posts as $post) {
            array_push($titles, $post->getResource()->getTitle());
        }
        $res = '<div class="tx-extbibsonomy-csl-typeahead-data">' . json_encode($titles) . '</div>';
        $res .= '<div class="tx-extbibsonomy-csl-publication-filter">';
        $res .= '<input type="text" class="tx-extbibsonomy-csl-publication-filter-input" placeholder="Filter publications"/>';
        $res .= '<input type="button" class="tx-extbibsonomy-csl-publication-filter-button" value="Filter"/>';
        $res .= '<input type="button" class="tx-extbibsonomy-csl-publication-filter-clear" value="Clear"/>';
        $res .= '</div>';
        return $res;
    }
}
