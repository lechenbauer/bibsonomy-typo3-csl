<?php

/*
 *  PUMA/BibSonomy CSL (ext_bibsonomy_csl) is a TYPO3 extension which
 *  enables users to render publication lists from PUMA or BibSonomy in
 *  various styles.
 *
 *  Copyright notice
 *  (c) 2015 Sebastian Böttger <seboettg@gmail.com>
 *
 *  HothoData GmbH (http://www.academic-puma.de)
 *  Knowledge and Data Engineering Group (University of Kassel)
 *
 *  All rights reserved
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace AcademicPuma\ExtBibsonomyCsl\ViewHelpers;

use AcademicPuma\RestClient\Model\Post;
use TYPO3\CMS\Fluid\Core\ViewHelper\TagBuilder;
use AcademicPuma\RestClient\Model\Document;
use \TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper;
use TYPO3Fluid\Fluid\Core\Rendering\RenderingContextInterface;

/**
 *
 *
 * @package ext_bibsonomy_csl
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 * @author Sebastian Böttger <boettger@cs.uni-kassel.de>
 */
class CslThumbnailRendererViewHelper extends AbstractViewHelper
{

    public function initializeArguments()
    {
        $this->registerArgument('post', 'mixed', 'The post the thumbnail is rendered for', true);
        $this->registerArgument('preview', 'string', 'Size of the preview', false, "small");
        $this->registerArgument('width', 'string', 'The width of the preview image', false, "57");
        $this->registerArgument('linkDocument', 'boolean', '', false, true);
    }
    
    static public function renderStatic(array $arguments, \Closure $renderChildrenClosure, RenderingContextInterface $renderingContext)
    {
        $documents = $arguments['post']->getDocuments();
        $fileName = $documents[0]->getFilename();
        $userName = $arguments['post']->getUser()->getName();
        
        /** @var TagBuilder $img */
        $img = self::getImageTagBuilder($arguments['post'], $documents[0], 
            $arguments['post']->getResource()->getIntraHash(), $userName, $arguments['preview'], $arguments['width'], $renderingContext);
        
        $imageArguments = array(
            "intraHash" => $arguments['post']->getResource()->getIntraHash(),
            "fileName" => $fileName,
            "userName" => $userName,
            "preview" => $arguments['preview']
        );
        
        $uriBuilder = $renderingContext->getControllerContext()->getUriBuilder();
        $arguments['preview'] = "large";
        $large = $uriBuilder->uriFor("preview", $imageArguments, "Document", 'extbibsonomycsl',
            'publicationlist');
        
        $img->addAttribute("onmouseover", "showtrail('" . $large . "');");
        $img->addAttribute("onmouseout", "hidetrail();");
        
        if ($arguments['linkDocument']) {
            unset($arguments['preview']);
            $downloadArguments = array(
                "intraHash" => $arguments['post']->getResource()->getIntraHash(),
                "fileName" => $fileName,
                "userName" => $userName
            );
            $uriBuilder->reset();
            $href = $uriBuilder->uriFor('download', $downloadArguments, 'Document', 'extbibsonomycsl', 'publicationlist');
            $a = new TagBuilder('a');
            $a->addAttribute("href", $href);
            $a->addAttribute("title", $arguments['post']->getResource()->getTitle());
            $a->addAttribute("alt", "Download - " . $arguments['post']->getResource()->getTitle());
            $a->addAttribute("target", "_blank");
            $a->setContent($img->render());
            
            return $a->render();
        } else {
            return '<span>' . $img->render() . '</span>';
        }
    }
    
    
    /**
     * @param Post $post
     * @param Document $document
     * @param string $intraHash
     * @param string $userName
     * @param string $size
     * @param string $width
     *
     * @return TagBuilder
     */
    protected static function getImageTagBuilder(Post $post, Document $document, $intraHash, $userName, $size, $width, $renderingContext)
    {

        $fileName = $document->getFileName();

        $action = "preview";
        $controller = "Document";
        $extensionName = str_replace('_', '', $renderingContext->getControllerContext()->getRequest()->getControllerExtensionKey());
        $pluginName = $renderingContext->getControllerContext()->getRequest()->getPluginName();

        $arguments = ["intraHash" => $intraHash, "fileName" => $fileName, "userName" => $userName, "preview" => $size];

        $uriBuilder = $renderingContext->getControllerContext()->getUriBuilder();
        $uriBuilder->reset();
        // Thumbnail image link chash
        // $uriBuilder->setUseCacheHash(false);

        // caching disabled due to errors in typo3 8
        // $uriBuilder->setNoCache(true);
        $src = $uriBuilder->uriFor($action, $arguments, $controller, $extensionName, $pluginName);

        // no way to guarantee line wrap for img alt-text
        // https://stackoverflow.com/questions/2731484/can-i-wrap-img-alt-text
        $alt = strlen($post->getResource()->getTitle()) > 25 ?
            substr($post->getResource()->getTitle(), 0, 25) . "..." : $post->getResource()->getTitle();

        $img = new TagBuilder('img');
        $img->addAttribute('src', $src);
        $img->addAttribute('title', $post->getResource()->getTitle());
        $img->addAttribute('alt', $alt . " - Download");
        $img->addAttribute('style', 'width: ' . $width . 'px;');
        $img->forceClosingTag(true);

        return $img;
    }

}
