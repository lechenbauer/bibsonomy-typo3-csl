<?php

/*
 *  PUMA/BibSonomy CSL (ext_bibsonomy_csl) is a TYPO3 extension which
 *  enables users to render publication lists from PUMA or BibSonomy in
 *  various styles.
 *
 *  Copyright notice
 *  (c) 2015 Sebastian Böttger <seboettg@gmail.com>
 *
 *  HothoData GmbH (http://www.academic-puma.de)
 *  Knowledge and Data Engineering Group (University of Kassel)
 *
 *  All rights reserved
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace AcademicPuma\ExtBibsonomyCsl\ViewHelpers;

use AcademicPuma\CiteProc\CiteProc;
use AcademicPuma\RestClient\Renderer\CSLModelRenderer;
use TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper;
use TYPO3Fluid\Fluid\Core\Rendering\RenderingContextInterface;

require_once \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('ext_bibsonomy_csl') . 'vendor/autoload.php';

/**
 *
 * @package ext_bibsonomy_csl
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 * @author Sebastian Böttger <boettger@cs.uni-kassel.de>
 *
 */
class CslPublicationRendererViewHelper extends AbstractViewHelper
{

    public function initializeArguments()
    {
        $this->registerArgument('post', 'string', 'The post that should be rendered', true);
        $this->registerArgument('xmlSource', 'string', 'CSL stylesheet', true);
        $this->registerArgument('lang', 'string', 'The language', true);
    }
    
    static public function renderStatic(array $arguments, \Closure $renderChildrenClosure, RenderingContextInterface $renderingContext)
    {
        $lang_short = substr($arguments["lang"], 0, 2);
        $renderer   = new CSLModelRenderer();
        try {
            //$csl = $renderer->render($arguments["post"]);
            $csl = $renderer->render($arguments["post"]->getResource());
            $citeproc = new CiteProc($arguments["xmlSource"], $lang_short);
            $out = $citeproc->render($csl);
        } catch (\Exception $e) {
            return '';
        }
        return $out;
    }
    
    
        
    
    
    
    
}
