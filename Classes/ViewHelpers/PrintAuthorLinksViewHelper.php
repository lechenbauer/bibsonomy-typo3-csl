<?php

/*
 *  PUMA/BibSonomy CSL (ext_bibsonomy_csl) is a TYPO3 extension which
 *  enables users to render publication lists from PUMA or BibSonomy in
 *  various styles.
 *
 *  Copyright notice
 *  (c) 2015 Sebastian Böttger <seboettg@gmail.com>
 *
 *  HothoData GmbH (http://www.academic-puma.de)
 *  Knowledge and Data Engineering Group (University of Kassel)
 *
 *  All rights reserved
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace AcademicPuma\ExtBibsonomyCsl\ViewHelpers;

use TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper;
use TYPO3Fluid\Fluid\Core\Rendering\RenderingContextInterface;

require_once \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('ext_bibsonomy_csl') . 'vendor/autoload.php';

/**
 *
 *
 * @package ext_bibsonomy_csl
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 * @author Kevin Choong <kch@cs.uni-kassel.de>
 */
class PrintAuthorLinksViewHelper extends AbstractViewHelper
{    
    
    public function initializeArguments()
    {
        $this->registerArgument('mode', 'string', '', true);
        $this->registerArgument('authorLinkMap', 'mixed', 'The XML source of the post', true);
        $this->registerArgument('cslname', 'string', 'The language', true);
    }
    
    
    static public function renderStatic(array $arguments, \Closure $renderChildrenClosure, RenderingContextInterface $renderingContext)
    {
        if ($arguments['mode'] == 'publication' || $arguments['mode'] == 'both') {
            
            $content = '{ "authors" : [';
            foreach ($arguments['authorLinkMap'] as $lastname => $value) {
                $content .= '{ "lastname":"' . $lastname . '" , "initial":"' . $value['initial'] . '" , "url":"' . $value['url'] . '" , "mail":"' . $value['mail'] . '" }, ';
            }
            $content = rtrim($content, ", ");
            $content .= ']}';
            
            return '<div class="tx-extbibsonomy-csl-author-links">' .
                '<div id="tx-extbibsonomy-csl-author-links-csl">' . $arguments['cslname'] . '</div>' .
                '<div id="tx-extbibsonomy-csl-author-links-data">' . $content . '</div>' .
                '</div>';
        } 
    }
    
}