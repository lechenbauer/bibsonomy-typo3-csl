<?php

/*
 *  PUMA/BibSonomy CSL (ext_bibsonomy_csl) is a TYPO3 extension which
 *  enables users to render publication lists from PUMA or BibSonomy in
 *  various styles.
 *
 *  Copyright notice
 *  (c) 2015 Sebastian Böttger <seboettg@gmail.com>
 *
 *  HothoData GmbH (http://www.academic-puma.de)
 *  Knowledge and Data Engineering Group (University of Kassel)
 *
 *  All rights reserved
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace AcademicPuma\ExtBibsonomyCsl\ViewHelpers;

use AcademicPuma\ExtBibsonomyCsl\Lib\Helper;
use AcademicPuma\RestClient\Model\Bibtex;
use AcademicPuma\RestClient\Model\Post;
use TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper;
use TYPO3Fluid\Fluid\Core\Rendering\RenderingContextInterface;

/**
 * Short description
 *
 * @since 19/10/15
 * @author Sebastian Böttger / boettger@cs.uni-kassel.de
 */
class GenerateLinksFromMiscFieldsViewHelper extends AbstractViewHelper
{

    const CSS_CLASS = 'tx-extbibsonomycsl-bib-link misc';

    public function initializeArguments()
    {
        $this->registerArgument('post', 'mixed', 'Post object', true);
        $this->registerArgument('settings', 'array', 'Settings...', true);
    }
    
    static public function renderStatic(array $arguments, \Closure $renderChildrenClosure, RenderingContextInterface $renderingContext)
    {
        $list        = explode(',', $arguments['settings']['bib_link_misc']);
        $resultArray = array();
        
        /** @var Bibtex $bibtex */
        $bibtex = $arguments['post']->getResource();
        
        foreach ($list as $miscKey) {
            
            $miscKey   = trim($miscKey);
            $miscValue = trim($bibtex->getMiscField($miscKey));
            
            if (!empty($miscValue)) {
                if (Helper::isUrl($miscValue)) {
                    $resultArray[] = '<span class="' . self::CSS_CLASS . '">[<a target="_blank" href="' . $miscValue . '" title="' . $miscKey . '">' . $miscKey . '</a>]</span>';
                }
            }
        }
        
        return implode(" ", $resultArray);
    }

}