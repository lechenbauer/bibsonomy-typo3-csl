<?php

/*
 *  PUMA/BibSonomy CSL (ext_bibsonomy_csl) is a TYPO3 extension which
 *  enables users to render publication lists from PUMA or BibSonomy in
 *  various styles.
 *
 *  Copyright notice
 *  (c) 2015 Sebastian Böttger <seboettg@gmail.com>
 *
 *  HothoData GmbH (http://www.academic-puma.de)
 *  Knowledge and Data Engineering Group (University of Kassel)
 *
 *  All rights reserved
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace AcademicPuma\ExtBibsonomyCsl\ViewHelpers;

use AcademicPuma\RestClient\Model\Post;
use TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper;
use TYPO3Fluid\Fluid\Core\Rendering\RenderingContextInterface;

require_once \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('ext_bibsonomy_csl') . 'vendor/autoload.php';

/**
 *
 *
 * @package ext_bibsonomy_csl
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 * @author Sebastian Böttger <boettger@cs.uni-kassel.de>
 */
class LinkHostViewHelper extends AbstractViewHelper
{

    public function initializeArguments()
    {
        $this->registerArgument('post', 'mixed', 'Post object', true);
    }
    
    static public function renderStatic(array $arguments, \Closure $renderChildrenClosure, RenderingContextInterface $renderingContext)
    {
        $href = $arguments['post']->getResource()->getHref();
        if (strpos($href, 'bibsonomy.org') >= 0) {
            /*
             * If clause for BibSonomy, because of API-links in each individual link for posts
             * and a seperate label for the post link
             */
            if (strpos($href, 'bibsonomy.org/api/') >= 0 ) {
                // API-Link, example: https://www.bibsonomy.org/api/users/username/posts/intrahash
                $hrefArr = explode('/', $href);
                $bibHref = 'https://www.bibsonomy.org/bibtex/' . $hrefArr[7] . '/' . $hrefArr[5];
            } else {
                $bibHref = $href;
            }
            return '<span class="tx-extbibsonomycsl-bib-link host">' . '[ <a href="' . $bibHref . '" target="_blank">BibSonomy-Post</a> ]</span>';
        } else {
            return '<span class="tx-extbibsonomycsl-bib-link host">' . '[ <a href="' . $href . '" target="_blank">PUMA-Post</a> ]</span>';
        }
    }

}
