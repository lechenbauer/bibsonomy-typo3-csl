<?php

/*
 *  PUMA/BibSonomy CSL (ext_bibsonomy_csl) is a TYPO3 extension which
 *  enables users to render publication lists from PUMA or BibSonomy in
 *  various styles.
 *
 *  Copyright notice
 *  (c) 2015 Sebastian Böttger <seboettg@gmail.com>
 *
 *  HothoData GmbH (http://www.academic-puma.de)
 *  Knowledge and Data Engineering Group (University of Kassel)
 *
 *  All rights reserved
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace AcademicPuma\ExtBibsonomyCsl\ViewHelpers;

use AcademicPuma\RestClient\Model\Post;
use TYPO3\CMS\Extbase\Utility\LocalizationUtility;
use TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper;
use TYPO3\CMS\Fluid\Core\ViewHelper\TagBuilder;
use TYPO3Fluid\Fluid\Core\Rendering\RenderingContextInterface;

require_once \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('ext_bibsonomy_csl') . 'vendor/autoload.php';

/**
 *
 *
 * @package ext_bibsonomy_csl
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 * @author Kevin Choong <kch@cs.uni-kassel.de>
 */
class PrintDetailsModalViewHelper extends AbstractViewHelper
{

    public function initializeArguments()
    {
        $this->registerArgument('post', 'mixed', 'Post object', true);
    }
    
    
    static public function renderStatic(array $arguments, \Closure $renderChildrenClosure, RenderingContextInterface $renderingContext)
    {
        $modalid = $arguments['post']->getResource()->getInterHash();
        
        // Button for opening the modal
        $button = new TagBuilder('span');
        $button->addAttribute('class', 'tx-extbibsonomycsl-bib-link');
        $buttonLink = new TagBuilder('a');
        $buttonLink->addAttribute('class', 'modal-link');
        $buttonLink->addAttribute('id', 'modal-link-details-' . $modalid);
        $buttonLink->addAttribute('href', '#');
        $buttonLink->setContent('Details');
        $button->setContent('[ ' . $buttonLink->render() . ' ]');
        
        // Modal
        $modal = new TagBuilder('div');
        $modal->addAttribute('id', 'modal-details-' . $modalid);
        $modal->addAttribute('class', 'modal');
        $modalContent = new TagBuilder('div');
        $modalContent->addAttribute('class', 'modal-content');
        $modalContent->addAttribute('style', 'width: 75%;');
        $closeButton = new TagBuilder('button');
        $closeButton->addAttribute('class', 'close');
        $closeButton->addAttribute('onClick', 'document.getElementById("modal-details-' . $modalid . '").style.display="none"');
        $closeButton->addAttribute('href', '#');
        $closeButton->setContent('&times;');
        $title = new TagBuilder('h2');
        $title->addAttribute('class', 'modal-title');
        $title->setContent($arguments['post']->getResource()->getTitle());
        $body = new TagBuilder('div');
        $body->addAttribute('class', 'modal-body');
        $body->setContent(self::renderDesc($arguments['post']) . self::renderAbstract($arguments['post']) . self::renderTable($arguments['post']));
        $modalContent->setContent($closeButton->render() . $title->render() . $body->render());
        $modal->setContent($modalContent->render());
        
        return $button->render() . $modal->render();
    }

    /**
     * Generate the details table with editors, links and tags
     *
     * @param   Post    $post
     * @return  string
     */
    private static function renderDesc($post) {
        return '';
    }

    /**
     * Generate the details table with editors, links and tags
     *
     * @param   Post    $post
     * @return  string
     */
    private static function renderAbstract($post) {
        $abstract = new TagBuilder('div');
        $abstract->addAttribute('class', 'tx-extbibsonomycsl-abstract');
        $abstract->setContent($post->getResource()->getBibtexAbstract());

        return $abstract->render();
    }

    /**
     * Generate the details table with editors, links and tags
     *
     * @param   Post    $post
     * @return  string
     */
    private static function renderTable($post) {
        // Table
        $table = new TagBuilder('table');
        $table->addAttribute('id', 'tx-extbibsonomycsl-detailstable');
        $tableContent = '';

        // Caption
        $caption = new TagBuilder('caption');
        $caption->setContent(LocalizationUtility::translate('fe.details.table.caption', 'ext_bibsonomy_csl'));
        $tableContent .= $caption->render();

        // Editor
        if ($post->getResource()->getEditor() !== null) {
            $tr = new TagBuilder('tr');
            $leftData = new TagBuilder('td');
            $leftData->setContent(LocalizationUtility::translate('fe.details.table.editors', 'ext_bibsonomy_csl'));
            $rightData = new TagBuilder('td');
            $rightData->setContent($post->getResource()->getEditor());
            $tr->setContent($leftData->render() . $rightData->render());
            $tableContent .= $tr->render();
        }

        // Links
        if ($post->getResource()->getHref() !== null) {
            $href = $post->getResource()->getHref();
            $tr = new TagBuilder('tr');
            $leftData = new TagBuilder('td');
            $rightData = new TagBuilder('td');
            if (strpos($href, 'bibsonomy.org') >= 0) {
                /*
                 * If clause for BibSonomy, because of API-links in each individual link for posts
                 * and a seperate label for the post link
                 */
                if (strpos($href, 'bibsonomy.org/api/') >= 0 ) {
                    // API-Link, example: https://www.bibsonomy.org/api/users/username/posts/intrahash
                    $hrefArr = explode('/', $href);
                    $bibHref = 'https://www.bibsonomy.org/bibtex/' . $hrefArr[7] . '/' . $hrefArr[5];
                } else {
                    $bibHref = $href;
                }
                $leftData->setContent('BibSonomy-Post');
                $rightData->setContent('<a target="_blank" href="' . $bibHref . '">' . $bibHref . '</a>');
            } else {
                $leftData->setContent('PUMA-Post');
                $rightData->setContent('<a target="_blank" href="' . $href . '">' . $href . '</a>');
            }
            $tr->setContent($leftData->render() . $rightData->render());
            $tableContent .= $tr->render();
        }

        if ($post->getResource()->hasMiscField('doi')) {
            $doi_url = 'http://dx.doi.org/' . $post->getResource()->getMiscField('doi');
            $tr = new TagBuilder('tr');
            $leftData = new TagBuilder('td');
            $leftData->setContent('URN');
            $rightData = new TagBuilder('td');
            $rightData->setContent('<a target="_blank" href="' . $doi_url . '">' . $doi_url . '</a>');
            $tr->setContent($leftData->render() . $rightData->render());
            $tableContent .= $tr->render();
        }
        if ($post->getResource()->hasMiscField('urn')) {
            $urn_url = 'http://nbn-resolving.org/' . urlencode($post->getResource()->getMiscField('urn'));
            $tr = new TagBuilder('tr');
            $leftData = new TagBuilder('td');
            $leftData->setContent('URN');
            $rightData = new TagBuilder('td');
            $rightData->setContent('<a target="_blank" href="' . $urn_url . '">' . $urn_url . '</a>');
            $tr->setContent($leftData->render() . $rightData->render());
            $tableContent .= $tr->render();
        }

        // Tags
        $tags = $post->getTag();
        if (sizeof($tags) > 0) {
            $tr = new TagBuilder('tr');
            $leftData = new TagBuilder('td');
            $leftData->setContent('Tags');
            $rightData = new TagBuilder('td');
            $tagLinks = '';
            foreach ($tags as $tag) {
                $href = $tag->getHref();

                // Temporary fix for REST-API returning wrong tag-links currently
                if (strpos($href, "/api/tags/") > 0) {
                    $href = substr($href, strpos($href, "/api/tags/") + 10);
                    $href = "https://www.bibsonomy.org/tag/" . $href;
                }
                $tagLinks .= '<a href="' . $href . '" target="_blank">' . $tag->getName() . '</a>&nbsp;';
            }
            $rightData->setContent($tagLinks);
            $tr->setContent($leftData->render() . $rightData->render());
            $tableContent .= $tr->render();
        }

        $table->setContent($tableContent);

        return $table->render();
    }

}