<?php

/*
 *  PUMA/BibSonomy CSL (ext_bibsonomy_csl) is a TYPO3 extension which
 *  enables users to render publication lists from PUMA or BibSonomy in
 *  various styles.
 *
 *  Copyright notice
 *  (c) 2015 Sebastian Böttger <seboettg@gmail.com>
 *
 *  HothoData GmbH (http://www.academic-puma.de)
 *  Knowledge and Data Engineering Group (University of Kassel)
 *
 *  All rights reserved
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace AcademicPuma\ExtBibsonomyCsl\ViewHelpers;

use AcademicPuma\RestClient\Model\Post;
use TYPO3\CMS\Extbase\Utility\LocalizationUtility;
use TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper;
use TYPO3\CMS\Fluid\Core\ViewHelper\TagBuilder;
use TYPO3Fluid\Fluid\Core\Rendering\RenderingContextInterface;

require_once \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('ext_bibsonomy_csl') . 'vendor/autoload.php';

/**
 *
 *
 * @package ext_bibsonomy_csl
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 * @author Kevin Choong <kch@cs.uni-kassel.de>
 * @author Mario Holtmüller <m.holtmueller@hothodata.de>
 */
class PublicationDetailsViewHelper extends AbstractViewHelper
{

    public function initializeArguments()
    {
        $this->registerArgument('post', 'mixed', 'Post object', true);
    }
    
    
    static public function renderStatic(array $arguments, \Closure $renderChildrenClosure, RenderingContextInterface $renderingContext)
    {
        return self::renderTable($arguments['post']);
        
    }

    /**
     * Generate the details table with editors, links and tags
     *
     * @param   Post    $post
     * @return  string
     */
    private static function renderTable($post) {
    
        $table = new TagBuilder('div');
        $table->addAttribute('class', 'tx-extbibsonomycsl-detailstable');
        $tableContent = '';

        // Caption
        $caption = new TagBuilder('div');
        $caption->addAttribute('class', 'tx-extbibsonomycsl-detailstable-headline');
        $caption->setContent(LocalizationUtility::translate('fe.details.table.caption', 'ext_bibsonomy_csl'));
        $tableContent .= $caption->render();

        // Editor
        if ($post->getResource()->getEditor() !== null) {
            $tr = new TagBuilder('div');
            $leftData = new TagBuilder('span');
            $leftData->addAttribute('class', 'tx-extbibsonomycsl-detailstable-label');
            $leftData->setContent(LocalizationUtility::translate('fe.details.table.editors', 'ext_bibsonomy_csl'));
            $rightData = new TagBuilder('span');
            $rightData->addAttribute('class', 'tx-extbibsonomycsl-detailstable-data');
            $rightData->setContent($post->getResource()->getEditor());
            $tr->setContent($leftData->render() . $rightData->render());
            $tableContent .= $tr->render();
        }

        // Links
        if ($post->getResource()->getHref() !== null) {
            $href = $post->getResource()->getHref();
            $tr = new TagBuilder('div');
            $leftData = new TagBuilder('span');
            $leftData->addAttribute('class', 'tx-extbibsonomycsl-detailstable-label');
            $rightData = new TagBuilder('span');
            $rightData->addAttribute('class', 'tx-extbibsonomycsl-detailstable-data');
            if (strpos($href, 'bibsonomy.org') !== false) {
                /*
                 * If clause for BibSonomy, because of API-links in each individual link for posts
                 * and a seperate label for the post link
                 */
                if (strpos($href, 'bibsonomy.org/api/') !== false ) {
                    // API-Link, example: https://www.bibsonomy.org/api/users/username/posts/intrahash
                    $hrefArr = explode('/', $href);
                    $bibHref = 'https://www.bibsonomy.org/bibtex/' . $hrefArr[7] . '/' . $hrefArr[5];
                } else {
                    $bibHref = $href;
                }
                $leftData->setContent('BibSonomy-Post');
                $rightData->setContent('<a target="_blank" href="' . $bibHref . '">' . $bibHref . '</a>');
            } else {
                $leftData->setContent('PUMA-Post');
                $rightData->setContent('<a target="_blank" href="' . $href . '">' . $href . '</a>');
            }
            $tr->setContent($leftData->render() . $rightData->render());
            $tableContent .= $tr->render();
        }

        if ($post->getResource()->hasMiscField('doi')) {
            $doi_url = 'http://dx.doi.org/' . $post->getResource()->getMiscField('doi');
            $tr = new TagBuilder('div');
            $leftData = new TagBuilder('span');
            $leftData->addAttribute('class', 'tx-extbibsonomycsl-detailstable-label');
            $leftData->setContent('URN');
            $rightData = new TagBuilder('span');
            $rightData->addAttribute('class', 'tx-extbibsonomycsl-detailstable-data');
            $rightData->setContent('<a target="_blank" href="' . $doi_url . '">' . $doi_url . '</a>');
            $tr->setContent($leftData->render() . $rightData->render());
            $tableContent .= $tr->render();
        }
        if ($post->getResource()->hasMiscField('urn')) {
            $urn_url = 'http://nbn-resolving.org/' . urlencode($post->getResource()->getMiscField('urn'));
            $tr = new TagBuilder('div');
            $leftData = new TagBuilder('span');
            $leftData->addAttribute('class', 'tx-extbibsonomycsl-detailstable-label');
            $leftData->setContent('URN');
            $rightData = new TagBuilder('span');
            $rightData->setContent('<a target="_blank" href="' . $urn_url . '">' . $urn_url . '</a>');
            $tr->setContent($leftData->render() . $rightData->render());
            $tableContent .= $tr->render();
        }

        // Tags
        $tags = $post->getTag();
        if (sizeof($tags) > 0) {
            $tr = new TagBuilder('div');
            $leftData = new TagBuilder('span');
            $leftData->addAttribute('class', 'tx-extbibsonomycsl-detailstable-label');
            $leftData->setContent('Tags');
            $rightData = new TagBuilder('span');
            $rightData->addAttribute('class', 'tx-extbibsonomycsl-detailstable-data');
            $tagLinks = '';
            foreach ($tags as $tag) {
                $href = $tag->getHref();

                // Temporary fix for REST-API returning wrong tag-links currently
                if (strpos($href, "/api/tags/") > 0) {
                    $href = substr($href, strpos($href, "/api/tags/") + 10);
                    $href = "https://www.bibsonomy.org/tag/" . $href;
                }
                $tagLinks .= '<a class="tx-extbibsonomycsl-detailstable-tag-link" href="' . $href . '" target="_blank">' . $tag->getName() . '</a> ';
            }
            $rightData->setContent($tagLinks);
            $tr->setContent($leftData->render() . $rightData->render());
            $tableContent .= $tr->render();
        }

        if ($post->getResource()->getBibtexAbstract() !== null) {
            $tr = new TagBuilder('div');
            $leftData = new TagBuilder('span');
            $leftData->addAttribute('class', 'tx-extbibsonomycsl-detailstable-label');
            $leftData->setContent(LocalizationUtility::translate('fe.list.link.abstract', 'ext_bibsonomy_csl'));
            $rightData = new TagBuilder('span');
            $rightData->addAttribute('class', 'tx-extbibsonomycsl-detailstable-data tx-extbibsonomycsl-detailstable-data-abstract');
            $rightData->setContent($post->getResource()->getBibtexAbstract());
            $tr->setContent($leftData->render() . $rightData->render());
            $clear = new TagBuilder('div');
            $clear->addAttribute('class', 'tx-extbibsonomycsl-detailstable-clear');
            $tableContent .= $tr->render().$clear->render();
        }

        
        $table->setContent($tableContent);

        return $table->render();
    }

}