<?php

/*
 *  PUMA/BibSonomy CSL (ext_bibsonomy_csl) is a TYPO3 extension which
 *  enables users to render publication lists from PUMA or BibSonomy in
 *  various styles.
 *
 *  Copyright notice
 *  (c) 2015 Sebastian Böttger <seboettg@gmail.com>
 *
 *  HothoData GmbH (http://www.academic-puma.de)
 *  Knowledge and Data Engineering Group (University of Kassel)
 *
 *  All rights reserved
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace AcademicPuma\ExtBibsonomyCsl\ViewHelpers;

use AcademicPuma\RestClient\Model\Post;
use AcademicPuma\RestClient\Model\Posts;
use AcademicPuma\RestClient\Renderer\BibtexModelRenderer;
use AcademicPuma\RestClient\Renderer\CSLModelRenderer;
use AcademicPuma\RestClient\Renderer\EndnoteModelRenderer;
use \TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper;
use TYPO3Fluid\Fluid\Core\Rendering\RenderingContextInterface;

require_once \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('ext_bibsonomy_csl') . 'vendor/autoload.php';

/**
 *
 *
 * @package ext_bibsonomy_csl
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 * @author Sebastian Böttger <boettger@cs.uni-kassel.de>
 */
class RendererViewHelper extends AbstractViewHelper
{

    public function initializeArguments()
    {
        $this->registerArgument('post', 'mixed', 'Post object', true);
        $this->registerArgument('rawPostsMap', 'mixed', 'Raw post map object', true);
        $this->registerArgument('type', 'string', 'Rendering type', true);
    }
    
    
    static public function renderStatic(array $arguments, \Closure $renderChildrenClosure, RenderingContextInterface $renderingContext)
    {
        switch ($arguments['type']) {
            case 'csl':
                $renderer = new CSLModelRenderer();
                break;
            case 'endnote':
                $renderer = new EndnoteModelRenderer();
                break;
            case 'bibtex':
                $renderer = new BibtexModelRenderer();
                $intraHash = $arguments['post']->getResource()->getIntraHash();
                $resource = $arguments['rawPostsMap'][$intraHash]->getResource();
                // TODO change place where tags are added to bibtex
                $resource->setKeywords($arguments['post']->getTag());
                return $renderer->render($resource);
                break;
            case 'bibtex-encoded':
                $renderer = new BibtexModelRenderer();
                $intraHash = $arguments['post']->getResource()->getIntraHash();
                $resource = $arguments['rawPostsMap'][$intraHash]->getResource();
                // TODO change place where tags are added to bibtex
                $resource->setKeywords($arguments['post']->getTag());
                $out = $renderer->render($resource);
                $old = array('~', '---', '--');
                $new = array('&nbsp;', '&mdash;', '&ndash;');
                $out = str_replace($old, $new, $out);
                return $out;
                break;
            default:
                $renderer = new BibtexModelRenderer();
                break;
        }
        return $renderer->render($arguments['post']);
    }

}
