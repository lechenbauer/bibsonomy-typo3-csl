<?php

/*
 *  PUMA/BibSonomy CSL (ext_bibsonomy_csl) is a TYPO3 extension which
 *  enables users to render publication lists from PUMA or BibSonomy in
 *  various styles.
 *
 *  Copyright notice
 *  (c) 2015 Sebastian Böttger <seboettg@gmail.com>
 *
 *  HothoData GmbH (http://www.academic-puma.de)
 *  Knowledge and Data Engineering Group (University of Kassel)
 *
 *  All rights reserved
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace AcademicPuma\ExtBibsonomyCsl\ViewHelpers;

use AcademicPuma\RestClient\Model\Post;
use TYPO3\CMS\Extbase\Utility\LocalizationUtility;
use TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper;
use TYPO3Fluid\Fluid\Core\Rendering\RenderingContextInterface;

require_once \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('ext_bibsonomy_csl') . 'vendor/autoload.php';

/**
 *
 *
 * @package ext_bibsonomy_csl
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 * @author Kevin Choong <kch@cs.uni-kassel.de>
 */
class PrintAuthorLinkWithModalViewHelper extends AbstractViewHelper
{
    
    public function initializeArguments()
    {
        $this->registerArgument('post', 'mixed', 'Post object', true);
        $this->registerArgument('mode', 'string', 'Defines the rendering mode', true);
        $this->registerArgument('authorLinkMap', 'array', 'Array that contains author links', true);
    }
    
    static public function renderStatic(array $arguments, \Closure $renderChildrenClosure, RenderingContextInterface $renderingContext)
    {
        if ($arguments['mode'] == 'modal' || $arguments['mode'] == 'both') {
            $authors = explode(' and ', $arguments['post']->getResource()->getAuthor());
            $content = '';
            $amountOfAuthorsWithLinks = 0;
            // Generate the content in the modal
            foreach($authors as $author) {
                $lastName = substr($author, 0, strpos($author, ','));
                $firstName = substr($author, strpos($author, ',') + 2);
                $authorLinkEntry = $arguments['authorLinkMap'][$lastName];
                if ($authorLinkEntry != null && strpos($firstName, $authorLinkEntry["initial"]) == 0) {
                    // Check, if the entry starts with the same lastname and first letter of the firstname as the author
                    $url = ($authorLinkEntry["url"] == 'none') ? '' :
                    '<br/> Website: <a target="_blank" href="' . $authorLinkEntry["url"] . '">' . $authorLinkEntry["url"] . '</a>' ;
                    $email = ($authorLinkEntry["mail"] == 'none') ? '' :
                    '<br/> E-Mail: ' . $authorLinkEntry["mail"];
                    $content .= '<p><label>' . $author . '</label>' . $url . $email . '</p>';
                    $amountOfAuthorsWithLinks++;
                }
            }
            
            if (!empty($content)) {
                $arguments['mode'] = ".single";
                if ($amountOfAuthorsWithLinks > 1) {
                    $arguments['mode'] = ".multiple";
                }
                return self::createModal($arguments['mode'], $content, $arguments['post']->getResource()->getInterHash());
            }
        }
        
        return '';
    }
    
    /**
     * Generates a HTML modal
     * @param $mode     string  Amount of authors with link for label
     * @param $content  string  Websites and e-mails of the authors involved in this post
     * @param $modalid  string  Unique id for the modal using the interhash of bibsonomy
     *
     * @return          string  HTML modal
     */
    private static function createModal($mode, $content, $modalid)
    {
        // Link entry for opening the modal
        $button = '<span class="tx-extbibsonomycsl-bib-link">[<a class="modal-link" id="modal-link-authors-' . $modalid . '" href="#"> '
            . LocalizationUtility::translate("fe.list.link.personal" . $mode, "ext_bibsonomy_csl") . ' </a>]</span>';

        // Modal
        $modal =    '<div id="modal-authors-' . $modalid . '" class="modal">
                        <div class="modal-content style="width: 33%;">
                            <button class="close" onClick="document.getElementById(\'modal-' . $modalid . '\').style.display=\'none\'" href="#">&times;</button>
                            <h4 class="modal-title">' . LocalizationUtility::translate("fe.list.link.personal.title" . $mode, "ext_bibsonomy_csl") . '</h4>
                            <div class="modal-body">' . $content . '</div>
                        </div>
                    </div>';

        return $button . $modal;
    }

}