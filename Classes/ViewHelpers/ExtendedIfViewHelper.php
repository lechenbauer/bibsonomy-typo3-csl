<?php

/*
 *  PUMA/BibSonomy CSL (ext_bibsonomy_csl) is a TYPO3 extension which
 *  enables users to render publication lists from PUMA or BibSonomy in
 *  various styles.
 *
 *  Copyright notice
 *  (c) 2015 Sebastian Böttger <seboettg@gmail.com>
 *
 *  HothoData GmbH (http://www.academic-puma.de)
 *  Knowledge and Data Engineering Group (University of Kassel)
 *
 *  All rights reserved
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace AcademicPuma\ExtBibsonomyCsl\ViewHelpers;

use TYPO3\CMS\Fluid\Core\ViewHelper\AbstractConditionViewHelper;
use TYPO3Fluid\Fluid\Core\Rendering\RenderingContextInterface;

/**
 * Short description
 *
 * @since 14/08/15
 * @author Sebastian Böttger / boettger@cs.uni-kassel.de
 */
class ExtendedIfViewHelper extends AbstractConditionViewHelper
{
    
    public function initializeArguments() {
        $this->registerArgument('condition', 'boolean', 'condition', true);
        $this->registerArgument('or', 'boolean', 'or', false, false);
        $this->registerArgument('and', 'boolean', 'and', false, false);
    }

    /**
     * 1) If 'or' is given: renders <f:then> child if $condition OR $or is true, otherwise renders <f:else> child.
     * 2) If 'and' is given: renders <f:then> child if $condition AND $and is true, otherwise renders <f:else> child.
     *
     * @return string the rendered string
     */
    static public function renderStatic(array $arguments, \Closure $renderChildrenClosure, RenderingContextInterface $renderingContext)
    {
        if ($arguments['or'] !== null) {
            if ($arguments['condition'] || $arguments['or']) {
                return self::renderThenChild();
            } else {
                return self::renderElseChild();
            }
        }
        if ($arguments['and'] !== null) {
            if ($arguments['condition'] && $arguments['and']) {
                return self::renderThenChild();
            } else {
                return self::renderElseChild();
            }
        }
    }
}