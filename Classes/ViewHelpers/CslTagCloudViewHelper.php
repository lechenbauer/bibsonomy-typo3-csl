<?php

/*
 *  PUMA/BibSonomy CSL (ext_bibsonomy_csl) is a TYPO3 extension which
 *  enables users to render publication lists from PUMA or BibSonomy in
 *  various styles.
 *
 *  Copyright notice
 *  (c) 2015 Sebastian Böttger <seboettg@gmail.com>
 *
 *  HothoData GmbH (http://www.academic-puma.de)
 *  Knowledge and Data Engineering Group (University of Kassel)
 *
 *  All rights reserved
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace AcademicPuma\ExtBibsonomyCsl\ViewHelpers;

use AcademicPuma\RestClient\Model\Tag;
use AcademicPuma\RestClient\Model\Tags;
use \TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper;
use TYPO3Fluid\Fluid\Core\Rendering\RenderingContextInterface;

/**
 *
 *
 * @package ext_bibsonomy_csl
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 * @author Sebastian Böttger <boettger@cs.uni-kassel.de>
 */
class CslTagCloudViewHelper extends AbstractViewHelper
{

    const TAG_MAX_SIZE = 2;
    const TAG_MIN_SIZE = 0.7;

    
    public function initializeArguments()
    {
        $this->registerArgument('maxcount', 'mixed', 'Maximum number of tags rendered', true);
        $this->registerArgument('tags', 'mixed', 'List of tags', true);
    }
    
    
    static public function renderStatic(array $arguments, \Closure $renderChildrenClosure, RenderingContextInterface $renderingContext)
    {
        $max = self::TAG_MAX_SIZE;
        $min = self::TAG_MIN_SIZE;
        
        $ret = '';
        /** @var Tag $tag $tag */
        foreach ($arguments['tags'] as $tag) {
            $href = $tag->getHref();
            
            // Temporary fix for REST-API returning wrong tag-links currently
            if (strpos($href, "/api/tags/") > 0) {
                $href = substr($href, strpos($href, "/api/tags/") + 10);
                $href = "https://www.bibsonomy.org/tag/" . $href;
            }
            $count = $tag->getUsercount();
            $size  = ($count / intval($arguments['maxcount'])) * ($max - $min) + $min;
            $ret .= '<span class="tx-extbibsonomycsl-tagcloud-span" style="display: inline-block !important; padding: 0 1px; font-size:' . sprintf("%01.2f",
                $size) . 'em;"><a href="' . $href . '" target="_blank">' . $tag->getName() . '</a></span>' . "\n";
        }
        
        return $ret;
    }

    // TODO: remove?
    private function getTagUrl(Tag $tag, $baseUrl)
    {

        $components = parse_url($tag->getHref());

        //TODO: Specific URL
    }
}
