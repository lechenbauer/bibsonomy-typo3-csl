<?php

/*
 *  PUMA/BibSonomy CSL (ext_bibsonomy_csl) is a TYPO3 extension which
 *  enables users to render publication lists from PUMA or BibSonomy in
 *  various styles.
 *
 *  Copyright notice
 *  (c) 2015 Sebastian Böttger <seboettg@gmail.com>
 *
 *  HothoData GmbH (http://www.academic-puma.de)
 *  Knowledge and Data Engineering Group (University of Kassel)
 *
 *  All rights reserved
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace AcademicPuma\ExtBibsonomyCsl\Domain\Repository;

use AcademicPuma\ExtBibsonomyCsl\Domain\Exception\AuthExistException;
use AcademicPuma\ExtBibsonomyCsl\Domain\Model\Authentication;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Persistence\Generic\Exception\RepositoryException;
use \TYPO3\CMS\Extbase\Persistence\Repository;
use TYPO3\CMS\Core\Database\Connection;

/**
 * Short description
 *
 * @since 07.08.15
 * @author Sebastian Böttger / boettger@cs.uni-kassel.de
 */
class AuthenticationRepository extends Repository
{

    public function findByHostUserName($hostUserName)
    {

        $query = $this->createQuery();
        $query->getQuerySettings()->setRespectStoragePage(false);

        return $query->matching($query->equals("id", $hostUserName))->execute()->getFirst();
    }

    public function findByCruserId($cruserId)
    {

        $query = $this->createQuery();
        $query->getQuerySettings()->setRespectStoragePage(false);

        return $query->matching($query->equals("cruser_id", $cruserId))->execute()->getFirst();
    }

    /**
     * @param Authentication $authentication
     *
     * @throws AuthExistException
     */
    public function add($authentication)
    {

        if ($this->uniqueExist($authentication)) {
            throw new AuthExistException('Operation not allowed! An AccessToken for "' . $authentication->getHostUserName() . '" on "' . $authentication->getHostAddress() . '" has already been saved.',
                20150906, null, $authentication->getHostUserName(), $authentication->getHostAddress(),
                $authentication->getEnableOAuth());
        }

        $record = $this->getTableRecord($authentication);
        
        $db = GeneralUtility::makeInstance(ConnectionPool::class)->getConnectionForTable("tx_extbibsonomycsl_domain_model_authentication");
        
        try {
            $db->insert(
                'tx_extbibsonomycsl_domain_model_authentication', 
                $record,
                [
                    Connection::PARAM_INT,
                    Connection::PARAM_STR,
                    Connection::PARAM_STR,
                    Connection::PARAM_STR,
                    Connection::PARAM_STR,
                    Connection::PARAM_INT,
                    Connection::PARAM_INT,
                    Connection::PARAM_INT,
                    Connection::PARAM_INT,
                    Connection::PARAM_INT,
                ]
            );
        } catch (\Doctrine\DBAL\Connection $e) {
            throw new RepositoryException('Could not create.');
        }

        // TODO REMOVE
//         if (!$GLOBALS['TYPO3_DB']->exec_INSERTquery('tx_extbibsonomycsl_domain_model_authentication', $record)) {
//             throw new RepositoryException('Could not create.');
//         }

    }

    /**
     *
     * @param Authentication $authentication
     */
    public function update($authentication)
    {

        $record = $this->getTableRecord($authentication);

        $db = GeneralUtility::makeInstance(ConnectionPool::class)->getConnectionForTable("tx_extbibsonomycsl_domain_model_authentication");
        
        try {
            $db->update(
                'tx_extbibsonomycsl_domain_model_authentication',
                $record,
                ['uid' => $authentication->getUid()],
                [
                    Connection::PARAM_INT,
                    Connection::PARAM_STR,
                    Connection::PARAM_STR,
                    Connection::PARAM_STR,
                    Connection::PARAM_STR,
                    Connection::PARAM_INT,
                    Connection::PARAM_INT,
                    Connection::PARAM_INT,
                    Connection::PARAM_INT,
                    Connection::PARAM_INT,
                ]
            );
        } catch (\Doctrine\DBAL\Connection $e) {
            throw new RepositoryException('Could not update.');
        }
        
        
        // TODO REMOVE    
//         if (!$GLOBALS['TYPO3_DB']->exec_UPDATEquery('tx_extbibsonomycsl_domain_model_authentication',
//             'uid=' . $authentication->getUid(), $record)
//         ) {
//             throw new RepositoryException('Could not update.');
//         }

    }

    public function findAllBasicAuthOfUser($cruserId)
    {

        $query = $this->createQuery();
        $query->getQuerySettings()->setRespectStoragePage(false);
        $_query = $query->matching($query->logicalAnd($query->equals("cruser_id", $cruserId),
            $query->equals("enable_o_auth", 0)));

        return $_query->execute();

    }

    /**
     * @param Authentication $authentication
     *
     * @return bool
     */
    private function uniqueExist(Authentication $authentication)
    {

        $query      = $this->createQuery();
        $conditions = [
            $query->equals('cruser_id', $GLOBALS["BE_USER"]->user["uid"]),
            $query->equals('enable_o_auth', $authentication->getEnableOAuth()),
            $query->equals('host_address', $authentication->getHostAddress()),
            $query->equals('host_user_name', $authentication->getHostUserName())
        ];

        return ($query->matching($query->logicalAnd($conditions))->execute()->count() > 0);
    }

    public function findAllOAuthOfUser($cruserId)
    {

        $query = $this->createQuery();
        $query->getQuerySettings()->setRespectStoragePage(false);
        $_query = $query->matching($query->logicalAnd($query->equals("cruser_id", $cruserId),
            $query->equals("enable_o_auth", 1)));

        return $_query->execute();
    }

    /**
     * @param $cruserId
     * @param $hostUserName
     *
     * @return \TYPO3\CMS\Extbase\Persistence\QueryResultInterface
     */
    public function findAllOAuthOfThisUser($cruserId, $hostUserName)
    {

        $query = $this->createQuery();
        $query->getQuerySettings()->setRespectStoragePage(false);
        $_query = $query->matching($query->logicalAnd([
            $query->equals("cruser_id", $cruserId),
            $query->equals("host_user_name", $hostUserName),
            $query->equals("enable_o_auth", 1)
        ]));

        return $_query->execute();
    }

    /**
     * @param Authentication $authentication
     *
     * @return array
     */
    private function getTableRecord($authentication)
    {

        $record = array(
            'pid'                     => 0,
            'host_address'            => $authentication->getHostAddress(),
            'host_user_name'          => $authentication->getHostUserName(),
            'host_api_key'            => $authentication->getHostApiKey(),
            'serialized_access_token' => $authentication->getSerializedAccessToken(),
            'enable_o_auth'           => intval($authentication->getEnableOAuth()),
            'create_date'             => $authentication->getCreateDate()->getTimestamp(),
            'tstamp'                  => time(),
            'crdate'                  => time(),
            'cruser_id'               => $GLOBALS["BE_USER"]->user["uid"]
        );

        return $record;
    }

}