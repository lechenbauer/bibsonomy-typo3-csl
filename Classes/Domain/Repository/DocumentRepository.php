<?php

/*
 *  PUMA/BibSonomy CSL (ext_bibsonomy_csl) is a TYPO3 extension which
 *  enables users to render publication lists from PUMA or BibSonomy in
 *  various styles.
 *
 *  Copyright notice
 *  (c) 2015 Sebastian Böttger <seboettg@gmail.com>
 *
 *  HothoData GmbH (http://www.academic-puma.de)
 *  Knowledge and Data Engineering Group (University of Kassel)
 *
 *  All rights reserved
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace AcademicPuma\ExtBibsonomyCsl\Domain\Repository;

require_once \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('ext_bibsonomy_csl') . 'vendor/autoload.php';

use AcademicPuma\ExtBibsonomyCsl\Domain\Exception\AuthenticationException;
use AcademicPuma\ExtBibsonomyCsl\Lib\MimeTypeMapper;
use AcademicPuma\RestClient\Accessor\Accessor;
use AcademicPuma\RestClient\Config\DocumentType;
use AcademicPuma\RestClient\RESTClient;
use \TYPO3\CMS\Extbase\Persistence\Repository;

/**
 *
 *
 * @package ext_bibsonomy_csl
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 * @author Sebastian Böttger
 *
 */
class DocumentRepository extends Repository
{

    /**
     *
     * @param Accessor $accessor
     * @param string $userName
     * @param string $intraHash
     * @param string $fileName
     * @param string $preview
     *
     * @throws AuthenticationException
     * @throws \AcademicPuma\RestClient\Exceptions\UnsupportedOperationException
     */
    public function getImage($accessor, $userName, $intraHash, $fileName, $preview)
    {

        $restClient = new RESTClient($accessor);

        header('Content-Disposition: attachment; filename="' . basename($fileName) . '.jpg');
        print $restClient->getDocumentFile($userName, $intraHash, $fileName, $preview)->file();

        exit();
    }

    /**
     *
     * @param Accessor $accessor
     * @param string $userName
     * @param string $intraHash
     * @param string $fileName
     *
     * @throws AuthenticationException
     * @throws \AcademicPuma\RestClient\Exceptions\UnsupportedOperationException
     */
    public function getDownload($accessor, $userName, $intraHash, $fileName)
    {

        $restClient = new RESTClient($accessor);
        header('Content-Type: ' . MimeTypeMapper::getMimeType($fileName));
        header('Content-Disposition: inline; filename="' . $fileName . '"');
        print $restClient->getDocumentFile($userName, $intraHash, $fileName, DocumentType::FILE)->file();
        exit();
    }

    /**
     *
     * @param Accessor $accessor
     * @param string $userName
     * @param string $intraHash
     * @param string $fileName
     *
     * @throws AuthenticationException
     * @throws \AcademicPuma\RestClient\Exceptions\UnsupportedOperationException
     */
    public function getView($accessor, $userName, $intraHash, $fileName)
    {

        $restClient = new RESTClient($accessor);
        header('Content-Type: ' . MimeTypeMapper::getMimeType($fileName));
        header('Content-Disposition: inline; filename="' . $fileName . '"');
        print $restClient->getDocumentFile($userName, $intraHash, $fileName, DocumentType::FILE)->file();
        exit();
    }
}
