<?php

/*
 *  PUMA/BibSonomy CSL (ext_bibsonomy_csl) is a TYPO3 extension which
 *  enables users to render publication lists from PUMA or BibSonomy in
 *  various styles.
 *
 *  Copyright notice
 *  (c) 2015 Sebastian Böttger <seboettg@gmail.com>
 *
 *  HothoData GmbH (http://www.academic-puma.de)
 *  Knowledge and Data Engineering Group (University of Kassel)
 *
 *  All rights reserved
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace AcademicPuma\ExtBibsonomyCsl\Domain\Repository;

require_once \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('ext_bibsonomy_csl') . 'vendor/autoload.php';

use AcademicPuma\ExtBibsonomyCsl\Domain\Exception\BadResponseException;
use AcademicPuma\ExtBibsonomyCsl\Lib\Helper;
use AcademicPuma\ExtBibsonomyCsl\Log\Logger;
use AcademicPuma\RestClient\Accessor\Accessor;
use AcademicPuma\RestClient\Config\Grouping;
use AcademicPuma\RestClient\Config\Resourcetype;
use AcademicPuma\RestClient\Config\RESTConfig;
use AcademicPuma\RestClient\Model\Post;
use AcademicPuma\RestClient\Model\Posts;
use TYPO3\CMS\Extbase\Persistence\Repository;
use AcademicPuma\RestClient\RESTClient;

/**
 *
 *
 * @package ext_bibsonomy_csl
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 * @author Sebastian Böttger
 *
 */
class PublicationRepository extends Repository
{
    const POST_COUNT_LIMIT = 250;

    /**
     * @var \AcademicPuma\RestClient\RESTClient
     */
    protected $restClient;

    /**
     *
     * @var string hash to identify the list
     */
    protected $listHash;

    /**
     * Fetches publications from Bibsonomy API, depending on the settings of the
     * front-end plugin.
     *
     * @param array $settings an object containing all information from Configuration/FlexForms/setup.xml
     * @param Accessor $accessor an object holding authentication information, e.g. basic auth credentials
     *
     * @param bool $raw
     * @return Posts
     * @throws BadResponseException
     */
    public function fetchPosts($settings, $accessor, $raw = false)
    {
        if ($this->restClient === null) {
            $this->restClient = $this->createRESTClient($settings, $accessor);
        }
        
        $treatCurlyBraces = $settings['curly_braces'];
        $treatBackslashes = $settings['treat_backslashes'];
        if ($raw) {
            $treatCurlyBraces = RESTConfig::CB_KEEP;
            $treatBackslashes = RESTConfig::BS_KEEP;
        }

        $tags = $this->getFilterTags($settings);
        $limit = $this->getLimit($settings);

        $grouping = $groupingName = $search = "";
        if (!empty($settings["bib_content_value"])) {
            $grouping = $settings["bib_content_key"];
            $groupingName = $settings["bib_content_value"];
        }

        if (!empty($settings["bib_search"])) {
            $search = $settings["bib_search"];
        }

        $resourceType = Resourcetype::BIBTEX;
        if ($grouping === Grouping::PERSON) {
            $resourceType = Resourcetype::GOLD_STANDARD_PUBLICATION;
        }
        
        try {
            $resArr = [];

            // Convert string to bool.
            $bibtexCleaning = ($settings['bibtex_cleaning'] === 'true');

            if ($limit > self::POST_COUNT_LIMIT) {
                for ($i = 1; ($i * self::POST_COUNT_LIMIT) <= $limit + self::POST_COUNT_LIMIT; ++$i) {

                    $start = ($i - 1) * self::POST_COUNT_LIMIT;
                    $end = $start + self::POST_COUNT_LIMIT;
                    if ($end > $limit) {
                        $end = $limit;
                    }

                    $this->restClient->getPosts($resourceType, $grouping, $groupingName, $tags, "", $search, "xml",
                        $start, $end);
                    $resArr = array_merge($resArr, $this->restClient->model($treatCurlyBraces, $treatBackslashes, $bibtexCleaning)->toArray());
                }
            } else {
                $this->restClient->getPosts($resourceType, $grouping, $groupingName, $tags, "", $search, "xml", 0,
                    $limit);
                $resArr = $this->restClient->model($treatCurlyBraces, $treatBackslashes, $bibtexCleaning)->toArray();
            }

            $this->DebugLog($settings, $accessor);

            return new Posts($resArr);

        } catch (\GuzzleHttp\Exception\BadResponseException $e) {
            $this->handleBadResponse($e);
            return new Posts();
        } catch (\Exception $e) {
            $this->handleException($e);
            return new Posts();
        }
    }

    /**
     * Creates a RESTClient object in order to make http requests.
     * Sets the verify ssl curl option.
     *
     * @param array    $settings an object containing all information from Configuration/FlexForms/setup.xml
     * @param Accessor $accessor an object holding authentication information, e.g. basic auth credentials
     *
     * @return RESTClient client used for making http requests
     */
    private function createRESTClient($settings, $accessor)
    {
        // Convert string to bool.
        $sslVerify = ($settings['ssl_verify'] === 'true');

        if ($settings['ssl_verify'] != 'path') {
            return new RESTClient($accessor, ['verify' => $sslVerify]);
        } else {
            return new RESTClient($accessor, ['verify' => $settings['ssl_ca_path']]);
        }
    }

    /**
     * Combines tags/concepts to include/exclude content into one array in order to use it as a filter.
     *
     * @param array $settings an object containing all information from Configuration/FlexForms/setup.xml
     *
     * @return array list of tags
     * @throws BadResponseException
     */
    private function getFilterTags($settings)
    {
        $tags = [];

        // Add subtags from concept.
        if (!empty($settings["bib_concepts"])) {
            $concepts = explode(" ", $settings["bib_concepts"]);

            foreach ($concepts as $concept) {

                try {
                    $query = $this->restClient->getConceptDetails($concept, $settings['bib_content_value']);

                    if ($query != null) {
                        foreach ($query->model()->getSubTags() as $subtag) {
                            $tags[] = $subtag->getName();
                        }
                    }
                } catch (\GuzzleHttp\Exception\BadResponseException $e) {
                    $this->handleBadResponse($e);
                } catch (\Exception $e) {
                    $this->handleException($e);
                }
            }
        }

        // Add tags.
        if (!empty($settings["bib_tags"])) {
            $tags = array_merge($tags, explode(" ", $settings["bib_tags"]));
        }

        // Exclude content with tags by prepending sys:not: to the tags.
        if (!empty($settings['bib_unselect_tags'])) {
            $bTags = explode(" ", trim($settings['bib_unselect_tags']));
            foreach ($bTags as $btag) {
                $tags[] = 'sys:not:' . trim(filter_var($btag, FILTER_SANITIZE_URL));
            }
        }

        // Exclude content with concepts by prepending sys:not: to the subtags.
        if (!empty($settings["bib_unselect_concepts"])) {
            $concepts = explode(" ", $settings["bib_unselect_concepts"]);

            foreach ($concepts as $concept) {

                try {
                    $query = $this->restClient->getConceptDetails($concept, $settings['bib_content_value']);

                    if ($query != null) {
                        foreach ($query->model()->getSubTags() as $subtag) {
                            $tags[] = 'sys:not:' . trim(filter_var($subtag, FILTER_SANITIZE_URL));
                        }
                    }
                } catch (\GuzzleHttp\Exception\BadResponseException $e) {
                    $this->handleBadResponse($e);
                } catch (\Exception $e) {
                    $this->handleException($e);
                }
            }
        }

        return $tags;
    }

    /**
     * @param array $settings an object containing all information from Configuration/FlexForms/setup.xml
     * @return int|mixed maximum number of pubs/bookmarks to fetch
     */
    private function getLimit($settings)
    {
        if (!empty($settings["bib_maxcount"])) {
            return filter_var($settings["bib_maxcount"], FILTER_SANITIZE_NUMBER_INT);
        }

        return 0;
    }

    /**
     * @param string   $userName
     * @param string   $intraHash
     * @param Accessor $accessor
     *
     * @return \AcademicPuma\RestClient\Model\Post
     * @throws \TYPO3\CMS\Extbase\Exception
     */
    public function findByUserAndHash($userName, $intraHash, Accessor $accessor)
    {
        // Initialize RESTClient
        //$this->restClient = new RESTClient($accessor);
        $this->restClient = $this->createRESTClient([], $accessor);
        try {
            $this->restClient->getPostDetails($userName, $intraHash);
        } catch (\GuzzleHttp\Exception\BadResponseException $e) {
            throw new BadResponseException('HTTP Client error occurred: ' . $e->getMessage(), $e->getCode(), $e);
        }

        return $this->restClient->model();
    }

    /**
     * Handles exceptions of type BadResponse. This involves logging the error through
     * TYPO3s own logging system and throwing another exception depending on the http
     * error code.
     *
     * @param \GuzzleHttp\Exception\BadResponseException $e
     * @throws BadResponseException
     */
    private function handleBadResponse(\GuzzleHttp\Exception\BadResponseException $e)
    {
        /** @var \TYPO3\CMS\Core\Log\Logger $logger */
        $logger = Logger::getLogger(__CLASS__);

        $logger->error('Exception while fetching posts from ' . $this->restClient->getQuery()->getUrl());
        $logger->error('Error ' . $e->getMessage());
        $logger->error('Error', $e->getTrace());

        $statusCode = $e->getResponse()->getStatusCode();

        if ($statusCode == 401) {
            // throw new BadResponseException('User credentials not valid. ' . $e->getMessage(), $e->getCode(), $e);
            throw new BadResponseException('User credentials are not valid.', $e->getCode(), $e);
        } else if ($statusCode == 404) {
            throw new BadResponseException('Resource not found. ' . $e->getMessage(), $e->getCode(), $e);
        } else {
            throw $e;
        }
    }

    /**
     * Logs thrown exception with TYPO3s own logging system.
     *
     * @param \Exception $e the exception to handle
     * @throws \Exception
     */
    private function handleException(\Exception $e)
    {
        /** @var \TYPO3\CMS\Core\Log\Logger $logger */
        $logger = Logger::getLogger(__CLASS__);

        $logger->error('Exception while fetching posts from ' . $this->restClient->getQuery()->getUrl());
        $logger->error('Error ' . $e->getMessage());
        $logger->error('Error', $e->getTrace());

        throw $e;
    }

    /**
     * Log what's happened when debug log is enabled.
     * Legend says debug log can be turned on, somewhere.
     *
     * @param array    $settings an object containing all information from Configuration/FlexForms/setup.xml
     * @param Accessor $accessor an object holding authentication information, e.g. basic auth credentials
     */
    private function DebugLog($settings, $accessor)
    {
        /** @var \TYPO3\CMS\Core\Log\Logger $logger */
        $logger = Logger::getLogger(__CLASS__);

        $confArray = unserialize($GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf']['ext_bibsonomy_csl']);

        if ($confArray['enableDebugLog']) {
            $logger->debug('Fetching posts from. ' . $this->restClient->getQuery()->getUrl() . ' with settings ', $settings);
            // disabled, as the URL is not accessible any more
            //$logger->debug('Accessor: ', ['baseUrl' => $accessor->getClient()->get('base_uri')]);
        }
    }

    public function getListHash()
    {
        if (empty($this->listHash)) {
            $this->listHash = Helper::generateListHash($this->restClient->model());
        }

        return $this->listHash;
    }
}
