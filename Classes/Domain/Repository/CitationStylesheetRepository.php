<?php

/*
 *  PUMA/BibSonomy CSL (ext_bibsonomy_csl) is a TYPO3 extension which
 *  enables users to render publication lists from PUMA or BibSonomy in
 *  various styles.
 *
 *  Copyright notice
 *  (c) 2015 Sebastian Böttger <seboettg@gmail.com>
 *
 *  HothoData GmbH (http://www.academic-puma.de)
 *  Knowledge and Data Engineering Group (University of Kassel)
 *
 *  All rights reserved
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace AcademicPuma\ExtBibsonomyCsl\Domain\Repository;

require_once \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('ext_bibsonomy_csl') . 'vendor/autoload.php';

use AcademicPuma\ExtBibsonomyCsl\Lib\FilterCslFilesDirectoryIterator;
use AcademicPuma\ExtBibSonomyCsl\Log\Logger;
use TYPO3\CMS\Core\Database\Connection;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Persistence\Generic\Exception\RepositoryException;
use TYPO3\CMS\Extbase\Persistence\QueryInterface;
use \TYPO3\CMS\Extbase\Persistence\Repository;

use \AcademicPuma\ExtBibsonomyCsl\Domain\Exception\InvalidStylesheetException;

/**
 *
 *
 * @package ext_bibsonomy_csl
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 * @author Sebastian Böttger
 */
class CitationStylesheetRepository extends Repository
{

    /**
     * Fetches citation stylesheet from database by using given stylesheet ID, independent
     * from page ID (pid).
     *
     * @param integer $citationStylesheetId
     *
     * @return \AcademicPuma\ExtBibsonomyCsl\Domain\Model\CitationStylesheet
     * @throws InvalidStylesheetException
     */
    public function findById($citationStylesheetId)
    {

        $query = $this->createQuery();
        $query->getQuerySettings()
              ->setIgnoreEnableFields(true)
              ->setRespectStoragePage(false)
              ->setIncludeDeleted(false)
              ->setRespectSysLanguage(false)
              ->setEnableFieldsToBeIgnored(['disabled', 'starttime', 'endtime']);

        $style = $query->matching($query->equals("id", $citationStylesheetId))->execute()->getFirst();

        if (empty($style)) {
            throw new InvalidStylesheetException("CSL style '" . $citationStylesheetId . "' not found. Import the missing style, clear the cache, and reload this page");
        }

        return $style;
    }

    /**
     * @param \AcademicPuma\ExtBibsonomyCsl\Domain\Model\CitationStylesheet $style
     *
     * @throws RepositoryException
     */
    public function add($style)
    {

        $userId = $GLOBALS["BE_USER"]->user["uid"];

        $query = $this->createQuery();

        $constraints = [
            $query->equals("id", $style->getId()),
            $query->equals('cruser_id', $userId)
        ];

        if ($query->matching($query->logicalAnd($constraints))->count() > 0) {
            throw new RepositoryException('Cannot insert style. A style with title "' . $style->getTitle() . '" already exists.');
        }

        $record = array(
            'pid'        => $style->getPid(),
            'id'         => $style->getId(),
            'title'      => $style->getTitle(),
            'xml_source' => $style->getXmlSource(),
            'tstamp'     => time(),
            'crdate'     => time(),
            'cruser_id'  => $userId,
        );

        // TODO REMOVE
        //$res = $GLOBALS['TYPO3_DB']->exec_INSERTquery('tx_extbibsonomycsl_domain_model_citationstylesheet', $record);

        
        $db = GeneralUtility::makeInstance(ConnectionPool::class)->getConnectionForTable("tx_extbibsonomycsl_domain_model_citationstylesheet");
        
        try {
            $db->insert(
                'tx_extbibsonomycsl_domain_model_authentication',
                $record,
                [
                    Connection::PARAM_INT,
                    Connection::PARAM_INT,
                    Connection::PARAM_STR,
                    Connection::PARAM_STR,
                    Connection::PARAM_INT,
                    Connection::PARAM_INT,
                    Connection::PARAM_INT,
                ]
                );
        } catch (\Doctrine\DBAL\Connection $e) {
            throw new RepositoryException('Could not create.');
        }
        
        
    }

    public function findAllOfBEUser()
    {

        $userId = $GLOBALS["BE_USER"]->user["uid"];
        $query  = $this->createQuery();
        $query->getQuerySettings()->setRespectStoragePage(false);

        return $query->matching($query->equals('cruser_id', $userId))->setOrderings([
            'crdate' => QueryInterface::ORDER_DESCENDING,
        ])->execute();

    }

    /**
     * @param $qry
     *
     * @return array
     */
    public function findAllStylesJsonByQuery($qry)
    {

        $stylesPath = ExtensionManagementUtility::extPath('ext_bibsonomy_csl') . 'vendor/academicpuma/styles/';

        $filePattern = $stylesPath;
        if (!empty($qry)) {
            $filePattern .= '*' . str_replace(' ', '*', $qry) . '*.csl';
        }

        $list = [];
        try {
            foreach (new FilterCslFilesDirectoryIterator($filePattern) as $style) {
                $doc = new \DOMDocument();
                $doc->loadXML(file_get_contents($style->getPathname()));
                $item        = new \stdClass();
                $item->id    = $doc->getElementsByTagName('id')->item(0)->nodeValue;
                $item->title = $doc->getElementsByTagName('title')->item(0)->nodeValue;
                $item->path  = $style->getPathname();
                $list[]      = $item;
            }
        } catch (\Exception $e) {
            $logger = Logger::getLogger(__CLASS__);
            $logger->error('Error ' . $e->getMessage(), $e->getTrace());
        }

        return $list;
    }
}
