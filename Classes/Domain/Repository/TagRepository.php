<?php

/*
 *  PUMA/BibSonomy CSL (ext_bibsonomy_csl) is a TYPO3 extension which
 *  enables users to render publication lists from PUMA or BibSonomy in
 *  various styles.
 *
 *  Copyright notice
 *  (c) 2015 Sebastian Böttger <seboettg@gmail.com>
 *
 *  HothoData GmbH (http://www.academic-puma.de)
 *  Knowledge and Data Engineering Group (University of Kassel)
 *
 *  All rights reserved
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace AcademicPuma\ExtBibsonomyCsl\Domain\Repository;

require_once \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('ext_bibsonomy_csl') . 'vendor/autoload.php';

use AcademicPuma\ExtBibsonomyCsl\Domain\Exception\BadResponseException;
use AcademicPuma\RestClient\Accessor\Accessor;
use AcademicPuma\RestClient\Config\TagOrder;
use AcademicPuma\RestClient\Config\TagRelation;
use AcademicPuma\RestClient\Model\Tag;
use AcademicPuma\RestClient\Model\Tags;
use AcademicPuma\RestClient\RESTClient;
use \TYPO3\CMS\Extbase\Persistence\Repository;

use \AcademicPuma\ExtBibsonomyCsl\Domain\Exception\AuthenticationException;

/**
 *
 *
 * @package ext_bibsonomy_csl
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 * @author Sebastian Böttger
 *
 */
class TagRepository extends Repository
{

    /**
     * Fetches publications from Bibsonomy API, depending on the settings of the
     * front-end plugin.
     *
     * @param array $settings
     * @param Accessor $accessor
     *
     * @return Tags
     * @throws AuthenticationException
     */
    public function fetchTags($settings, $accessor)
    {

        $restClient = new RESTClient($accessor);

        list($grouping, $groupingName, $settings, $relatedTags, $limit, $order) = $this->initializeParameters($settings);

        /** @var Tags $tagList */
        $tagList = null;

        if (!empty($relatedTags)) {
            try {
                $restClient->getTagRelation($grouping, $groupingName, TagRelation::RELATED, $relatedTags, $order, 0,
                    $limit);
            } catch (\GuzzleHttp\Exception\BadResponseException $e) {
                throw new BadResponseException('HTTP Client error occurred: ' . $e->getMessage(), $e->getCode(), $e);
            }
            $tagList = $restClient->model();

        } else {

            $restClient->getTags($grouping, $groupingName, '', $order, 0, $limit);
            $tagList = $restClient->model();
        }

        //get array of blacklist tags
        $blacklist = explode(" ", $settings["bib_blacklist_tags"]);

        $this->filterTags($tagList, $blacklist);

        return $tagList;
    }

    /**
     * @param array $settings
     *
     * @return array
     */
    private function initializeParameters(array $settings)
    {

        $grouping     = $settings["bib_tag_content_key"];
        $groupingName = $settings["bib_tag_content_value"];
        $relatedTags  = [];

        //related?
        if (!empty($settings['bib_tags'])) {
            $relatedTags = explode(" ", trim($settings['bib_tags']));
        }

        $limit = $settings["bib_tag_maxcount"];
        $order = TagOrder::FREQUENCY;

        return array($grouping, $groupingName, $settings, $relatedTags, $limit, $order);
    }

    /**
     * @param Tags $tagList
     * @param array $blacklist
     */
    private function filterTags(Tags $tagList, array $blacklist)
    {

    	$count =  $tagList->count();
    	
        for ($i = 0; $i < $count; ++$i) {

            /** @var Tag $tagItem */
            $tagItem = $tagList[$i];

            //filter blacklist tags
            if (in_array($tagItem->getName(), $blacklist)) {
                $tagList->remove($i);
            }
        }
    }
}
