<?php

/*
 *  PUMA/BibSonomy CSL (ext_bibsonomy_csl) is a TYPO3 extension which
 *  enables users to render publication lists from PUMA or BibSonomy in
 *  various styles.
 *
 *  Copyright notice
 *  (c) 2015 Sebastian Böttger <seboettg@gmail.com>
 *
 *  HothoData GmbH (http://www.academic-puma.de)
 *  Knowledge and Data Engineering Group (University of Kassel)
 *
 *  All rights reserved
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace AcademicPuma\ExtBibsonomyCsl\Domain\Model;

/**
 * Authentication
 */
class Authentication extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity
{

    /**
     * hostAddress
     *
     * @var string
     */
    protected $hostAddress = '';

    /**
     * hostUserName
     *
     * @var string
     */
    protected $hostUserName = '';

    /**
     * hostApiKey
     *
     * @var string
     */
    protected $hostApiKey = '';

    /**
     * serializedAccessToken
     *
     * @var string
     */
    protected $serializedAccessToken = '';

    /**
     * enableOAuth
     *
     * @var boolean
     */
    protected $enableOAuth = false;

    /**
     * createDate
     *
     * @var \DateTime
     */
    protected $createDate = null;

    /**
     * @var int
     */
    protected $cruserId = 0;

    /**
     * Returns the hostAddress
     *
     * @return string
     */
    public function getHostAddress()
    {

        return $this->hostAddress;
    }

    /**
     * Sets the hostAddress
     *
     * @param string $hostAddress
     *
     * @return void
     */
    public function setHostAddress($hostAddress)
    {

        $this->hostAddress = $hostAddress;
    }

    /**
     * Returns the hostUserName
     *
     * @return string $hostUserName
     */
    public function getHostUserName()
    {

        return $this->hostUserName;
    }

    /**
     * Sets the hostUserName
     *
     * @param string $hostUserName
     *
     * @return void
     */
    public function setHostUserName($hostUserName)
    {

        $this->hostUserName = $hostUserName;
    }

    /**
     * Returns the hostApiKey
     *
     * @return string $hostApiKey
     */
    public function getHostApiKey()
    {

        return $this->hostApiKey;
    }

    /**
     * Sets the hostApiKey
     *
     * @param string $hostApiKey
     *
     * @return void
     */
    public function setHostApiKey($hostApiKey)
    {

        $this->hostApiKey = $hostApiKey;
    }

    /**
     * Returns the serializedAccessToken
     *
     * @return string $serializedAccessToken
     */
    public function getSerializedAccessToken()
    {

        return $this->serializedAccessToken;
    }

    /**
     * Sets the serializedAccessToken
     *
     * @param string $serializedAccessToken
     *
     * @return void
     */
    public function setSerializedAccessToken($serializedAccessToken)
    {

        $this->serializedAccessToken = $serializedAccessToken;
    }

    /**
     * Returns the enableOAuth
     *
     * @return boolean $enableOAuth
     */
    public function getEnableOAuth()
    {

        return $this->enableOAuth;
    }

    /**
     * Sets the enableOAuth
     *
     * @param boolean $enableOAuth
     *
     * @return void
     */
    public function setEnableOAuth($enableOAuth)
    {

        $this->enableOAuth = $enableOAuth;
    }

    /**
     * Returns the boolean state of enableOAuth
     *
     * @return boolean
     */
    public function isEnableOAuth()
    {

        return $this->enableOAuth;
    }

    /**
     * Returns the createDate
     *
     * @return \DateTime $createDate
     */
    public function getCreateDate()
    {

        return $this->createDate;
    }

    /**
     * Sets the createDate
     *
     * @param \DateTime $createDate
     *
     * @return void
     */
    public function setCreateDate(\DateTime $createDate)
    {

        $this->createDate = $createDate;
    }

    public function getCruserId()
    {

        return $this->cruserId;
    }

    public function setCruserId($cruserId)
    {

        $this->cruserId = $cruserId;
    }
}