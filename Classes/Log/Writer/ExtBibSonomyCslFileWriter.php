<?php

/*
 *  PUMA/BibSonomy CSL (ext_bibsonomy_csl) is a TYPO3 extension which
 *  enables users to render publication lists from PUMA or BibSonomy in
 *  various styles.
 *
 *  Copyright notice
 *  (c) 2015 Sebastian Böttger <seboettg@gmail.com>
 *
 *  HothoData GmbH (http://www.academic-puma.de)
 *  Knowledge and Data Engineering Group (University of Kassel)
 *
 *  All rights reserved
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace AcademicPuma\ExtBibsonomyCsl\Log\Writer;

use TYPO3\CMS\Core\Log\Writer\FileWriter;

/**
 * Short description
 *
 * @since 16.11.15
 * @author Sebastian Böttger / boettger@cs.uni-kassel.de
 */
class ExtBibSonomyCslFileWriter extends FileWriter
{

    /**
     * Default log file path
     *
     * @var string
     */
    protected $defaultLogFile = 'typo3temp/logs/extbibsonomycsl.error.log';

    public function __construct(array $options = array())
    {

        // the parent constructor reads $options and sets them
        parent::__construct($options);
    }

    /**
     * Destructor, closes the log file handle
     */
    public function __destruct()
    {

        parent::__destruct();
    }

}