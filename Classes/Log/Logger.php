<?php

/*
 *  PUMA/BibSonomy CSL (ext_bibsonomy_csl) is a TYPO3 extension which
 *  enables users to render publication lists from PUMA or BibSonomy in
 *  various styles.
 *
 *  Copyright notice
 *  (c) 2015 Sebastian Böttger <seboettg@gmail.com>
 *
 *  HothoData GmbH (http://www.academic-puma.de)
 *  Knowledge and Data Engineering Group (University of Kassel)
 *
 *  All rights reserved
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace AcademicPuma\ExtBibsonomyCsl\Log;

use AcademicPuma\ExtBibsonomyCsl\Log\Writer\ExtBibSonomyCslFileWriter;
use TYPO3\CMS\Core\Log\LogLevel;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * Short description
 *
 * @since 16.11.15
 * @author Sebastian Böttger / boettger@cs.uni-kassel.de
 */
class Logger
{

    /**
     * @var array<\TYPO3\CMS\Core\Log\Logger> $logger
     */
    private static $logger = [];

    /**
     * @param $className
     *
     * @return \TYPO3\CMS\Core\Log\Logger
     */
    public static function getLogger($className)
    {

        if (!isset(self::$logger[$className]) || empty(self::$logger[$className])) {
            self::$logger[$className] = GeneralUtility::makeInstance('TYPO3\CMS\Core\Log\LogManager')
                                                      ->getLogger($className);
            $writer                   = new ExtBibSonomyCslFileWriter();
            self::$logger[$className]->addWriter(LogLevel::DEBUG, $writer);
        }

        return self::$logger[$className];
    }
}