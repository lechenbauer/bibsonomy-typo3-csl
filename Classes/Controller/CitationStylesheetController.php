<?php

/*
 *  PUMA/BibSonomy CSL (ext_bibsonomy_csl) is a TYPO3 extension which
 *  enables users to render publication lists from PUMA or BibSonomy in
 *  various styles.
 *
 *  Copyright notice
 *  (c) 2015 Sebastian Böttger <seboettg@gmail.com>
 *
 *  HothoData GmbH (http://www.academic-puma.de)
 *  Knowledge and Data Engineering Group (University of Kassel)
 *
 *  All rights reserved
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace AcademicPuma\ExtBibsonomyCsl\Controller;

use \AcademicPuma\ExtBibsonomyCsl\Domain\Repository\CitationStylesheetRepository;
use \AcademicPuma\ExtBibsonomyCsl\Domain\Model\CitationStylesheet;
use TYPO3\CMS\Core\Messaging\FlashMessage;

/**
 *
 *
 * @package ext_bibsonomy_csl
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 * @author Sebastian Böttger
 *
 */
class CitationStylesheetController extends DefaultActionController
{

    const EXAMPLE_CITE = '{"DOI":"","ISBN":"","URL":"","abstract":"","author":[{"family":"Einstein","given":"Albert"},{"family":"Grossmann","given":"Marcel"}],"citation-label":"Einstein1913","collection-title":"","container-title":"","editor":[],"event-place":"","id":"76b6a8740eec04a155f846436c8fb2a9marco.giovanell","issued":{"date-parts":[["1913"]],"literal":"1913"},"note":"CPAE 4, 13","page":"225-244, 245-261","publisher":"","title":"Entwurf einer verallgemeinerten Relativitätstheorie und eine Theorie\n\tder Gravitation. I. Physikalischer Teil von A. Einstein II. Mathematischer\n\tTeil von M. Grossmann","volume":"62"}';

    /**
     * citationStylesheetRepository
     *
     * @var \AcademicPuma\ExtBibsonomyCsl\Domain\Repository\CitationStylesheetRepository
     */
    protected $citationStylesheetRepository;

    /**
     * injectCitationStylesheetRepository
     *
     * @param \AcademicPuma\ExtBibsonomyCsl\Domain\Repository\CitationStylesheetRepository $citationStylesheetRepository
     *
     * @return void
     */
    public function injectCitationStylesheetRepository(CitationStylesheetRepository $citationStylesheetRepository)
    {

        $this->citationStylesheetRepository = $citationStylesheetRepository;
    }

    /**
     * @return void
     */
    public function initializeAction()
    {

        $this->citationStylesheetRepository = $this->objectManager->get('AcademicPuma\\ExtBibsonomyCsl\\Domain\\Repository\\CitationStylesheetRepository');
    }

    /**
     *
     * @return void
     */
    public function listAction()
    {

        $result = $this->citationStylesheetRepository->findAll();
        $this->view->assign('styles', $result);
        $this->view->assign('example', self::EXAMPLE_CITE);
    }

    /**
     *
     * @param CitationStylesheet $stylesheet
     */
    public function newAction(CitationStylesheet $stylesheet = null)
    {

        $this->view->assign('stylesheet', $stylesheet);
    }

    /**
     *
     * @param CitationStylesheet $stylesheet
     *
     * @throws \TYPO3\CMS\Extbase\Mvc\Exception\UnsupportedRequestTypeException
     */
    public function createAction(CitationStylesheet $stylesheet)
    {

        $this->citationStylesheetRepository->add($stylesheet);
        $this->createFlashMessage('Success', 'Stylesheet added.', FlashMessage::INFO);
        $this->redirect('list');
    }

    /**
     * A template method for displaying custom error flash messages, or to
     * display no flash message at all on errors. Override this to customize
     * the flash message in your action controller.
     *
     * @return string|boolean The flash message or FALSE if no flash message should be set
     * @api
     */
    protected function getErrorFlashMessage()
    {

        return 'An error occurred while trying to call ' . get_class($this) . '->' . $this->actionMethodName . '()';
    }

}
