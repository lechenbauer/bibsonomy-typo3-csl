<?php

/*
 *  PUMA/BibSonomy CSL (ext_bibsonomy_csl) is a TYPO3 extension which
 *  enables users to render publication lists from PUMA or BibSonomy in
 *  various styles.
 *
 *  Copyright notice
 *  (c) 2015 Sebastian Böttger <seboettg@gmail.com>
 *
 *  HothoData GmbH (http://www.academic-puma.de)
 *  Knowledge and Data Engineering Group (University of Kassel)
 *
 *  All rights reserved
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace AcademicPuma\ExtBibsonomyCsl\Controller;

use AcademicPuma\ExtBibsonomyCsl\Log\Logger;
use AcademicPuma\RestClient\Exceptions\FileNotFoundException;
use AcademicPuma\RestClient\Renderer\XMLModelUnserializer;
use TYPO3\CMS\Core\Messaging\FlashMessage;
use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;
use \TYPO3\CMS\Extbase;
use \TYPO3\CMS\Core\Utility\GeneralUtility;
use \AcademicPuma\ExtBibsonomyCsl\Lib\Helper;
use \AcademicPuma\ExtBibsonomyCsl\Domain\Model\CitationStylesheet;
use \AcademicPuma\ExtBibsonomyCsl\Domain\Repository\CitationStylesheetRepository;
use TYPO3\CMS\Extbase\Persistence\Generic\Exception\RepositoryException;

/**
 * Controller for backend actions
 *
 * @package ext_bibsonomy_csl
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 * @author Sebastian Böttger
 */
class BackendController extends DefaultActionController
{

    protected $pageId;

    /**
     * Repository for Citation Stylesheets
     *
     * @var \AcademicPuma\ExtBibsonomyCsl\Domain\Repository\CitationStylesheetRepository $citationStylesheetRepository ;
     */
    protected $citationStylesheetRepository;

    /**
     * @return void
     */
    protected function initializeAction()
    {

        $this->citationStylesheetRepository =& $this->objectManager->get('AcademicPuma\\ExtBibsonomyCsl\\Domain\\Repository\\CitationStylesheetRepository');

        $this->pageId = intval(GeneralUtility::_GP('id'));
    }

    /**
     * injectCitationStylesheetRepository
     *
     * @param \AcademicPuma\ExtBibsonomyCsl\Domain\Repository\CitationStylesheetRepository $citationStylesheetRepository
     *
     * @return void
     */
    public function injectCitationStylesheetRepository(CitationStylesheetRepository $citationStylesheetRepository)
    {

        $this->citationStylesheetRepository = $citationStylesheetRepository;
    }

    public function indexAction()
    {

        //echo "<pre>";
        //print_r($GLOBALS['TBE_STYLES']['spriteIconApi']['iconsAvailable']);
        //echo "</pre>";
    }

    /**
     * assigns all stylesheets in view
     *
     * @return void
     */
    public function listAction()
    {

        $result = $this->citationStylesheetRepository->findAllOfBEUser();
        $this->view->assign('styles', $result);
        $path = ExtensionManagementUtility::extPath('ext_bibsonomy_csl') . "/Resources/Private/Posts/einstein.xml";
        if (!file_exists($path)) {
            $logger = Logger::getLogger(__CLASS__);
            $logger->error('File does not exist ' . $path);
        }
        $post              = file_get_contents($path);
        $modelUnserializer = new XMLModelUnserializer($post);
        $post              = $modelUnserializer->convertToModel();
        $this->view->assign('post', $post);
    }

    /**
     * adds a new stylesheet to the repository
     *
     * @param string $xmlSource
     *
     * @return void
     */
    public function createAction($xmlSource = '')
    {

        if ($this->request->hasArgument('xmlSource')) {
            $xmlSource = $this->request->getArgument('xmlSource');

            try {
                $stylesheet = Helper::createStylesheetObject($xmlSource, $this->pageId);
                try {
                    $this->citationStylesheetRepository->add($stylesheet);
                } catch (RepositoryException $e) {
                    $this->controllerContext->getFlashMessageQueue()->push($this->createFlashMessage($e->getMessage(),
                        'ERROR', FlashMessage::ERROR));

                    return;
                }
                $this->createFlashMessage('New stylesheet "' . $stylesheet->getTitle() . '" has been saved.', 'Success',
                    FlashMessage::OK);
            } catch (\Exception $e) {
                $exceptionDescription = 'Exception in ' . $e->getFile() . ' on line ' . $e->getLine();
                $this->createFlashMessage($e->getMessage() . "<br />$exceptionDescription", 'Error',
                    FlashMessage::ERROR);
            }
        }
    }

    /**
     * imports a new stylesheet from the given url and adds it to the repository
     *
     * @param string $stylePath
     *
     * @throws FileNotFoundException
     * @throws \AcademicPuma\ExtBibsonomyCsl\Domain\Exception\InvalidStylesheetException
     */
    public function importAction($stylePath = "")
    {

        if ($this->request->hasArgument('stylePath')) {

            $path = $this->request->getArgument('stylePath');

            if (!file_exists($path)) {
                throw new FileNotFoundException($path);
            }

            $xmlSource  = file_get_contents($path);
            $stylesheet = Helper::createStylesheetObject($xmlSource, $this->pageId);
            try {
                $this->citationStylesheetRepository->add($stylesheet);
            } catch (RepositoryException $e) {
                $this->addFlashMessage($e->getMessage(), 'ERROR', FlashMessage::ERROR);

                return;
            }
            $this->addFlashMessage('New stylesheet "' . $stylesheet->getTitle() . '" has been imported.', 'Success',
                FlashMessage::OK);

        } else {
            $this->addFlashMessage('Missing file name.', 'Error', FlashMessage::ERROR);
        }

        $this->redirect('list');

    }

    /**
     * @todo implement when needed
     *
     * @param \AcademicPuma\ExtBibsonomyCsl\Domain\Model\CitationStylesheet $stylesheet
     *
     * @return void
     */
    public function editAction(CitationStylesheet $stylesheet)
    {
        //TODO: implement
    }

    /**
     * @todo implement when needed
     *
     * @param \AcademicPuma\ExtBibsonomyCsl\Domain\Model\CitationStylesheet $stylesheet
     *
     * @return void
     */
    public function updateAction(CitationStylesheet $stylesheet)
    {
        //TODO: implement
    }

    /**
     * deletes the given stylesheet
     *
     * @param \AcademicPuma\ExtBibsonomyCsl\Domain\Model\CitationStylesheet $stylesheet
     *
     * @throws \TYPO3\CMS\Extbase\Mvc\Exception\UnsupportedRequestTypeException
     * @throws \TYPO3\CMS\Extbase\Persistence\Exception\IllegalObjectTypeException
     */
    public function deleteAction(CitationStylesheet $stylesheet)
    {

        $this->citationStylesheetRepository->remove($stylesheet);
        $this->createFlashMessage('The stylesheet "' . $stylesheet->getTitle() . '" has been removed.',
            'Stylesheet deleted', FlashMessage::OK);
        $this->redirect('list');
    }

    public function uploadAction()
    {

        if ($this->request->hasArgument('file')) {

            $file = $this->request->getArgument('file');

            if ($file) {

                /** @var \TYPO3\CMS\Core\Utility\File\BasicFileUtility $fileUtility */
                $fileUtility = $this->objectManager->get('TYPO3\\CMS\\Core\\Utility\\File\\BasicFileUtility');
                $fileName    = $fileUtility->getUniqueName($file['name'],
                    GeneralUtility::getFileAbsFileName('uploads/'), 1);

                GeneralUtility::upload_copy_move($file['tmp_name'], $fileName);
            } else {
                $this->createFlashMessage('Upload has failed.', 'ERROR', FlashMessage::ERROR);

                return;
            }

            $xmlSource = Helper::getDataFromCSLFile($fileName);

            $stylesheet = Helper::createStylesheetObject($xmlSource, $this->pageId);
            try {
                $this->citationStylesheetRepository->add($stylesheet);
            } catch (RepositoryException $e) {

                $this->createFlashMessage($e->getMessage(), 'ERROR', FlashMessage::ERROR);

                return;
            }
            $this->createFlashMessage('New stylesheet "' . $stylesheet->getTitle() . '" has been added.', 'Success',
                FlashMessage::OK);

        }
    }

    public function jsonAction()
    {

        $qry    = filter_input(INPUT_GET, 'query', FILTER_SANITIZE_STRING);
        $styles = $this->citationStylesheetRepository->findAllStylesJsonByQuery($qry);
        header('Content-Type: application/json');
        print json_encode($styles);
        die;
    }

    /**
     * A template method for displaying custom error flash messages, or to
     * display no flash message at all on errors. Override this to customize
     * the flash message in your action controller.
     *
     * @return string|boolean The flash message or FALSE if no flash message should be set
     */
    protected function getErrorFlashMessage()
    {

        return 'An error occurred while trying to call ' . get_class($this) . '->' . $this->actionMethodName . '()';
    }

}

