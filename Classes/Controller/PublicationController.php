<?php

/*
 *  PUMA/BibSonomy CSL (ext_bibsonomy_csl) is a TYPO3 extension which
 *  enables users to render publication lists from PUMA or BibSonomy in
 *  various styles.
 *
 *  Copyright notice
 *  (c) 2015 Sebastian Böttger <seboettg@gmail.com>
 *
 *  HothoData GmbH (http://www.academic-puma.de)
 *  Knowledge and Data Engineering Group (University of Kassel)
 *
 *  All rights reserved
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace AcademicPuma\ExtBibsonomyCsl\Controller;

use AcademicPuma\ExtBibsonomyCsl\Domain\Repository\AuthenticationRepository;
use AcademicPuma\ExtBibsonomyCsl\Domain\Repository\CitationStylesheetRepository;
use AcademicPuma\ExtBibsonomyCsl\Domain\Repository\PublicationRepository;
use AcademicPuma\ExtBibsonomyCsl\Lib\PostYearComparatorAndFilter;
use AcademicPuma\ExtBibsonomyCsl\Lib\PublicationFilter;
use AcademicPuma\RestClient\Config\Sorting;
use AcademicPuma\RestClient\Model\Post;
use AcademicPuma\RestClient\Model\Posts;
use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;

use AcademicPuma\ExtBibsonomyCsl\Domain\Model\CitationStylesheet;
use AcademicPuma\ExtBibsonomyCsl\Domain\Exception\AbstractException;

use AcademicPuma\ExtBibsonomyCsl\Lib\Helper;
use AcademicPuma\ExtBibsonomyCsl\Lib\PublicationTypeUtils;

/**
 * Action Controller for frontend plugin 'Bibsonomy Publication List'.
 * Contains an action to assign a list of publications and a compare method
 * for sorting the list of publications
 *
 * @package ext_bibsonomy_csl
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 * @author Sebastian Böttger
 *
 */
class PublicationController extends DefaultActionController
{
    /**
     *
     * @var array
     */
    private $userDefinedTypeOrder;

    /**
     * publicationRepository
     *
     * @var PublicationRepository
     */
    protected $publicationRepository;

    /**
     * citationStylesheetRepository
     *
     * @var CitationStylesheetRepository
     */
    protected $citationStylesheetRepository;

    /**
     *
     * @var AuthenticationRepository
     */
    protected $authenticationRepository;

    /**
     * injectPublicationRepository
     *
     * @param PublicationRepository $publicationRepository
     *
     * @return void
     */
    public function injectPublicationRepository(PublicationRepository $publicationRepository)
    {
        $this->publicationRepository = $publicationRepository;
    }

    /**
     *
     */
    public function initializeAction()
    {
        if ($this->publicationRepository == null) {
            /**
             * @var PublicationRepository
             */
            $this->publicationRepository = $this->objectManager->get('AcademicPuma\\ExtBibsonomyCsl\\Domain\\Repository\\PublicationRepository');

        }
    }

    private function prepareTypeOrder()
    {
        if (empty($this->userDefinedTypeOrder)) {

            if (!empty($this->settings['bib_type_order'])) {
                $this->userDefinedTypeOrder = explode(',', $this->settings['bib_type_order']);
            } else {
                $this->userDefinedTypeOrder = PublicationTypeUtils::$defaultTypeOrder;
            }

            //$this->userDefinedTypeOrder = array_merge($this->userDefinedTypeOrder, PublicationTypeUtils::$defaultTypeOrder);
        }
    }

    private function getCitationStylesheet()
    {
        if (empty($this->settings["bib_ownstylesheet"])) {

            $this->citationStylesheetRepository = $this->objectManager->get('AcademicPuma\\ExtBibsonomyCsl\\Domain\\Repository\\CitationStylesheetRepository');
            $stylesheetId = $this->settings['bib_stylesheet'];
            $citationStylesheet = $this->citationStylesheetRepository->findById($stylesheetId);

        } else {

            $citationStylesheet = Helper::createStylesheetObject($this->settings['bib_ownstylesheet']);
        }

        return $citationStylesheet;
    }

    /**
     *
     * @param \AcademicPuma\RestClient\Model\Posts $posts
     * @param \AcademicPuma\ExtBibsonomyCsl\Domain\Model\CitationStylesheet|CitationStylesheet $citationStylesheet
     * @param string $listHash
     * @param array $rawPostsMap
     *
     * @internal param array $publications
     */
    private function viewAssignments(Posts $posts, array $rawPostsMap, CitationStylesheet $citationStylesheet, $listHash)
    {
        $this->view->assign('settings', $this->settings);

        $this->view->assign('linkAbstract', $this->settings['bib_link_abstract']);
        $this->view->assign('linkBibtex', $this->settings['bib_link_bibtex']);
        $this->view->assign('linkEndnote', $this->settings['bib_link_endnote']);
        $this->view->assign('linkUrl', $this->settings['bib_link_url']);
        $this->view->assign('linkDoi', $this->settings['bib_link_doi']);
        $this->view->assign('linkHost', $this->settings['bib_link_host']);
        $this->view->assign('linkMisc', !empty($this->settings['bib_link_misc']));

        $this->view->assign('showNotes', $this->settings['bib_show_notes']);

        $this->view->assign('escapeBibtex', $this->settings['bibtex_escape_special']);
        $this->view->assign('htmlencodeBibtex', $this->settings['bibtex_htmlencode_special']);
        $this->view->assign('enableDetailsPage', $this->settings['enable_details_page']);
        $this->view->assign('detailsPageUid', $this->settings['details_page']);

        $this->view->assign('linkDocument', strpos($this->settings['bib_link_doc'], 'link') !== false);
        $this->view->assign('thumbDocument', strpos($this->settings['bib_link_doc'], 'thumb') !== false);

        $sr = $this->settings['bib_sorting'];
        $gr = $this->settings['bib_grouping'];

        if (!is_numeric($gr)) {
            $this->view->assign('grouping', $this->settings['bib_grouping']);
        } else if ($gr == 1 || $sr === 'type') {
            $this->view->assign('oldGrouping', 1);

            if ($sr != 'title' && $sr != 'author') {
                $this->view->assign('oldGroupingType', $sr);
            }
        }

        $this->view->assign('displayJumpLabels',
            ($this->settings['bib_display_anchors'] && $this->settings['bib_grouping']));
        $this->view->assign('posts', $posts);
        $this->view->assign('rawPostsMap', $rawPostsMap);
        $this->view->assign('listHash', $listHash);
        $this->view->assign('citationStylesheet', $citationStylesheet);
        $this->view->assign('host', $this->settings['bib_server']);
        $this->view->assign('extpath',
            ExtensionManagementUtility::siteRelPath($this->request->getControllerExtensionKey()));
        $this->view->assign('renderLanguage', $this->settings['bib_language'] ? $this->settings['bib_language'] : 'en');
        $this->view->assign('url', $this->uriBuilder->getRequest()->getRequestUri());

        /**
         * Generating Map with Author as key and their additional urls/emails as value
         */
        $authorLinkMap = [];
        if (!empty($this->settings['bib_link_personal'])) {
            $linkPersonalSettings = explode("\n", $this->settings['bib_link_personal']);
            if (filter_var($linkPersonalSettings[0], FILTER_VALIDATE_URL) == false) {
                // User entered additional urls and emails of authors directly in the settings
                $authorLinkList = $linkPersonalSettings;
            } else {
                // User listed a url as the first element
                // TODO Check, if text file
                $txt_file = file_get_contents($linkPersonalSettings[0]);
                $authorLinkList = explode("\n", $txt_file);
            }

            foreach ($authorLinkList as $entry) {
                if ($this->validateAuthorLinkEntry($entry)) {
                    $entryArr = explode(';', $entry);
                    $lastName = substr($entryArr[0], 0, strpos($entryArr[0], ','));
                    $firstNameInitial = substr($entryArr[0], strpos($entryArr[0], ',') + 2, 1);
                    $url = (trim($entryArr[1]) == 'none') ? 'none' : trim($entryArr[1]);
                    $mail = (trim($entryArr[2]) == 'none') ? 'none' : trim($entryArr[2]);
                    $authorLinkMap[$lastName] = ["initial" => $firstNameInitial, "url" => $url, "mail" => $mail];
                }
            }
        }
        $this->view->assign('authorLinks', !empty($this->settings['bib_link_personal']));
        $this->view->assign('authorLinksMode', $this->settings['bib_link_personal_mode']);
        $this->view->assign('authorLinkMap', $authorLinkMap);

        $this->view->assign('inlineFilter', $this->settings['bib_inline_filter']);
    }

    private function addAdditionalHeaderData()
    {
        // append js scripts
        $this->response->addAdditionalHeaderData('<script type="text/javascript">loadingImg = "' . ExtensionManagementUtility::siteRelPath($this->request->getControllerExtensionKey()) . 'Resources/Public/Icons/loading.gif";</script>');
        $this->response->addAdditionalHeaderData('<script type="text/javascript" src="' . ExtensionManagementUtility::siteRelPath($this->request->getControllerExtensionKey()) . 'Resources/Public/Scripts/jquery-3.4.1.min.js" /></script>');
        $this->response->addAdditionalHeaderData('<script type="text/javascript" src="' . ExtensionManagementUtility::siteRelPath($this->request->getControllerExtensionKey()) . 'Resources/Public/Scripts/typeahead.bundle.js" /></script>');
        $this->response->addAdditionalHeaderData('<script type="text/javascript" src="' . ExtensionManagementUtility::siteRelPath($this->request->getControllerExtensionKey()) . 'Resources/Public/Scripts/tx_extbibsonomycsl.js" /></script>');
        $this->response->addAdditionalHeaderData('<script type="text/javascript" src="' . ExtensionManagementUtility::siteRelPath($this->request->getControllerExtensionKey()) . 'Resources/Public/Scripts/tooltip.js" /></script>');

        //appending style in html head
        $this->response->addAdditionalHeaderData('<link type="text/css" rel="stylesheet" href="' . ExtensionManagementUtility::siteRelPath($this->request->getControllerExtensionKey()) . 'Resources/Public/Styles/tx_extbibsonomycsl.css" />');

        //appending custom style in html head
        $this->response->addAdditionalHeaderData('<style type="text/css">' . "\n" . $this->settings['bib_css_attribute_for_list_item'] . "\n" . '</style>');
    }

    /**
     * Assigns fetched publications to view.
     *
     * @throws
     *
     * @return void
     */
    public function listAction()
    {
        global $GLOBALS;

        $accessor = $this->makeAccessor();

        /** @var Posts $posts */
        $posts = $this->publicationRepository->fetchPosts($this->settings, $accessor);
        /*
         * Raw posts don't need to be sorted, after they are fetched.
         * Since they will only be used for the raw BibTeX in the publication.
         */
        $rawPosts = $this->publicationRepository->fetchPosts($this->settings, $accessor, true);
        // creating a map for better performance, when looking up the raw posts with the intrahash as key
        $rawPostsMap = [];
        foreach ($rawPosts as $raw) {
            $intraHash = $raw->getResource()->getIntraHash();
            $rawPostsMap[$intraHash] = $raw;
        }

        $listHash = $this->publicationRepository->getListHash();

        // First filter duplicates and non numeric years to avoid further grouping or filtering those posts.
        $this->filterDuplicatesAndYears($posts);

        // YEAR FILTER
        if (!empty($this->settings['bib_year_filter'])) {

            $filter = new PostYearComparatorAndFilter($this->settings['bib_sorting'], Sorting::ORDER_ASC,
                $this->userDefinedTypeOrder, $this->settings['bib_year_filter']);

            PublicationFilter::filterPublicationsByYear($posts, $filter);
        }

        // GROUPING / SORTING
        if ($this->settings['bib_grouping'] !== 'none') {

            $posts = $this->prepareGrouping($posts);

            // Set/overwrite the sorting within the group for DBLP sort
            if ($this->settings['bib_grouping'] == 'dblp') {
                $this->settings['bib_sorting'] = 'dblp';
                $this->settings['bib_sorting_order'] = 'desc';
            }

            // sort within sublists of groups
            $groupKeys = array_keys($posts->toArray()); //get groups
            foreach ($groupKeys as $groupKey) {

                if ($posts[$groupKey] instanceof Posts) {
                    $sublist = new Posts($posts[$groupKey]->toArray());
                } else {
                    $sublist = new Posts($posts[$groupKey]);
                }

                $sublist->sort($this->settings['bib_sorting'],
                    $this->settings['bib_sorting_order'] === 'asc' ? Sorting::ORDER_ASC : Sorting::ORDER_DESC);

                $posts[$groupKey] = $sublist;
            }

        } else {
            $posts->sort($this->settings['bib_sorting'],
                $this->settings['bib_sorting_order'] === 'asc' ? Sorting::ORDER_ASC : Sorting::ORDER_DESC);
            $p = clone $posts;
            $posts->clear();
            $posts->add('', $p);
        }

        $citationStylesheet = $this->getCitationStylesheet();

        $this->viewAssignments($posts, $rawPostsMap, $citationStylesheet, $listHash);

        $this->addAdditionalHeaderData();

        //$this->checkForFiles();
    }


    /**
     * @param string $userName
     * @param string $intraHash
     *
     * @throws
     */
    public function detailsAction($userName, $intraHash)
    {
        $accessor = $this->makeAccessor();

        /** @var Post $post */
        $post = $this->publicationRepository->findByUserAndHash($userName, $intraHash, $accessor);

        $this->view->assign('post', $post);
        $this->view->assign('host', $this->host);

        $this->addAdditionalHeaderData();
    }

    /**
     * for ajax request. renders and prints out BibTeX formatted style in a textarea
     *
     * @param string $userName
     * @param string $intraHash
     */
    public function bibtexAction($userName, $intraHash)
    {
        try {
            $url = $this->buildExportUrl($userName, $intraHash, 'bibtex');
            $bibtex = $this->publicationRepository->getBibsonomyResponseBody($url);

        } catch (AbstractException $e) {

            $e->debug();
        }

        print '<textarea rows="10" style="width:100%;">' . $bibtex . '</textarea>';
        die;
    }

    /**
     * for ajax request. renders and prints out Endnote formatted style in a textarea
     *
     * @param string $userName
     * @param string $intraHash
     */
    public function endnoteAction($userName, $intraHash)
    {
        try {
            $url = $this->buildExportUrl($userName, $intraHash, 'endnote');
            $endnote = $this->publicationRepository->getBibsonomyResponseBody($url);

        } catch (AbstractException $e) {

            $e->debug();
        }

        print '<textarea rows="10" style="width:100%;">' . $endnote . '</textarea>';
        die;
    }

    /**
     * prepares type order and filters publications (grouping by type) or does
     * pre-sorting (grouping by year). This function also transforms <code>$posts</code>
     * to an grouped Posts ArrayList.
     * for instance:
     * <pre>
     * array(
     *   [2010] => array(
     *          [0] => pub1
     *          [1] => pub2)
     *   [2012] => array(
     *          [0] => pub4)
     *   [2014] => array(
     *          [0] => pub3
     *          [1] => pub5)
     * )
     * </pre>
     *
     * @param Posts $posts un-grouped Posts ArrayList
     *
     * @return Posts
     */
    private function prepareGrouping(Posts &$posts)
    {
        //backward compatibility
        if (!is_numeric($this->settings['bib_grouping'])) {

            switch ($this->settings['bib_grouping']) {
                case 'type':
                    $this->prepareTypeOrder();
                    //pre-sorting
                    $posts->sort('entrytype', null, $this->userDefinedTypeOrder);
                    PublicationFilter::filterPublicationsByType($posts, $this->settings, $this->userDefinedTypeOrder);
                    break;
                case 'dblp':
                    $posts->sort('year', Sorting::ORDER_DESC);
                    $this->transformToYearGroupedArray($posts);
                    break;
                case 'year_asc': //pre-sorting (asc) for year grouping

                    $posts->sort('year', Sorting::ORDER_ASC);
                    $this->transformToYearGroupedArray($posts);
                    break;
                case 'year_desc': //pre-sorting (desc) for year grouping
                default:
                    $posts->sort('year', Sorting::ORDER_DESC);
                    $this->transformToYearGroupedArray($posts);

            }
        } else { //transform old setting style to new setting style

            if ($this->settings['bib_sorting'] === 'type') {

                $this->prepareTypeOrder();
                //presorting type
                $posts->sort('entrytype');

                PublicationFilter::filterPublicationsByType($posts, $this->settings, $this->userDefinedTypeOrder);

            } else if ($this->settings['bib_sorting'] === 'year') {

                if ($this->settings['bib_sorting_order'] === 'asc') {
                    $posts->sort('year', Sorting::ORDER_ASC);
                } else {
                    $posts->sort('year', Sorting::ORDER_DESC);
                }

                if ($this->settings['bib_grouping']) {
                    $this->transformToYearGroupedArray($posts);
                } else {
                    $sublist = new Posts();
                    $sublist->add(0, $posts); //create pseudo group
                    $posts = $sublist;
                }
            } else { //author and title
                if ($this->settings['bib_sorting_order'] === 'asc') {
                    $posts->sort($this->settings['bib_sorting'], Sorting::ORDER_ASC);
                } else {
                    $posts->sort($this->settings['bib_sorting'], Sorting::ORDER_DESC);
                }
                $sublist = new Posts();
                $sublist->add(0, $posts); //create pseudo group
                $posts = $sublist;
            }
        }

        return $posts;
    }

    /**
     * transforms publications list to multidimensional Posts Array List:
     * 1st dimension represents the year of the data content
     *
     * @param Posts $posts pre-sorted (by year) publication list
     */
    private function transformToYearGroupedArray(Posts &$posts)
    {
        $groupedPosts = [];
        if (!empty($posts->toArray())) {
            $year = $posts[0]->getResource()->getYear();
            foreach ($posts as $post) {
                if ($post->getResource()->getYear() !== $year) {
                    $year = $post->getResource()->getYear();
                }
                $groupedPosts[$year][] = $post;
            }
            $posts->setArray($groupedPosts);
        }
    }

    /**
     * Filters duplicate publications according to the plugin settings. (by inter- or intrahash)
     * If the removed publications are of any interest, comment in the corresponding lines containing removedPublications.
     * Also filters publications with non-numeric year specifications when the option to allow them is not activated.
     *
     * @param Posts $posts not grouped list of publications.
     */
    private function filterDuplicatesAndYears(Posts &$posts)
    {
        // Do something when filter duplicates is either 1 = filter by intrahash or 2 = filter by interhash
        // or when non numeric values in the year field are not allowed.
        if ($this->settings["bib_filter_duplicates"] > 0 || $this->settings["non_numeric_year"] == false) {

            $hashes = array();
//        $removedPublications = array();

            // Iterate over all publications and either add them to the hashes array or remove them when already added.
            /* @var Tx_ExtBibsonomyCsl_Domain_Model_Publication $pub */
            foreach ($posts as $key => $pub) {

                // YEAR SPECIFICATION FILTERING
                if ($this->settings["non_numeric_year"] == false) {
                    if (!is_numeric($pub->getResource()->getYear())) {
                        unset($posts[$key]);
                        //$removedPublications[] = $posts[$key]->getAuthor() . "<br />" . $posts[$key]->getTitle() . "<br />" . $posts[$key]->getIntraHash();
                    }
                }

                // DUPLICATE FILTERING
                if ($this->settings["bib_filter_duplicates"] > 0 && $pub != null) {

                    // Either use inter- or intrahash depending on the plugin settings.
                    $hash = $this->settings["bib_filter_duplicates"] == 1 ? $pub->getResource()->getIntraHash() : $pub->getResource()->getInterHash();

                    if (in_array($hash, $hashes)) {
                        unset($posts[$key]);
                        //$removedPublications[] = $posts[$key]->getAuthor() . "<br />" . $posts[$key]->getTitle() . "<br />" . $posts[$key]->getIntraHash();
                    } else {
                        $hashes[] = $hash;
                    }
                }
            }

            //var_dump($removedPublications);
        }
    }

    /**
     * Validate the single entry the author list with
     * links and e-mail of individual authors
     *
     * @param   $entry    string String with Lastname, Firstname; URL; E-Mail
     *
     * @throws
     *
     * @return bool
     */
    public function validateAuthorLinkEntry($entry)
    {
        $entryArr = explode(';', $entry);
        if (sizeof($entryArr) != 3) return false;
        $regExNames = '/[A-zÀ-ÿ]+, [A-zÀ-ÿ]+/';
        if (preg_match($regExNames, $entryArr[0]) == false) return false;
        if (Helper::isUrl(trim($entryArr[1])) == false && trim($entryArr[1]) != 'none') return false;
        // Not complex regular expression for email-validation, since we allow users to enter e-mails with any given escape at-character
        $regExMail = '/([\w\(\)\[\].-]+|none)/';
        if (preg_match($regExMail, trim($entryArr[2])) == false) return false;
        return true;
    }

    private function checkForFiles()
    {
        $resourceFactory = \TYPO3\CMS\Core\Resource\ResourceFactory::getInstance();
        $storage = $resourceFactory->getDefaultStorage();
        $separator = PHP_OS == "Windows" || PHP_OS == "WINNT" ? "\\" : "/";
        $newfile = str_replace(array('/', '\\'), $separator, PATH_site . "/fileadmin/ext_bibsonomy_csl/anass.txt");
        fopen($newfile, "w") or die('could not create file' . $newfile);
/*        $newFile = $storage->addFile(
            'D:\Downloads\allied.txt',
            $storage->getRootLevelFolder(),
            'final_file_name.foo'
        );*/
    }
}