<?php

/*
 *  PUMA/BibSonomy CSL (ext_bibsonomy_csl) is a TYPO3 extension which
 *  enables users to render publication lists from PUMA or BibSonomy in
 *  various styles.
 *
 *  Copyright notice
 *  (c) 2015 Sebastian Böttger <seboettg@gmail.com>
 *
 *  HothoData GmbH (http://www.academic-puma.de)
 *  Knowledge and Data Engineering Group (University of Kassel)
 *
 *  All rights reserved
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace AcademicPuma\ExtBibsonomyCsl\Controller;

use AcademicPuma\ExtBibsonomyCsl\Domain\Repository\TagRepository;
use AcademicPuma\RestClient\Config\Sorting;
use AcademicPuma\RestClient\Model\Tag;

/**
 *
 *
 * @package ext_bibsonomy_csl
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 * @author Sebastian Böttger
 *
 */
class TagController extends DefaultActionController
{

    /**
     * tagRepository
     *
     * @var \AcademicPuma\ExtBibsonomyCsl\Domain\Repository\TagRepository
     */
    protected $tagRepository;

    /**
     * injectPublicationRepository
     *
     * @param \AcademicPuma\ExtBibsonomyCsl\Domain\Repository\TagRepository $tagRepository
     *
     * @return void
     */
    public function injectTagRepository(TagRepository $tagRepository)
    {

        $this->tagRepository = $tagRepository;
    }

    /**
     * @return void
     */
    public function initializeAction()
    {

        if ($this->tagRepository == null) {
            $this->tagRepository = $this->objectManager->get('AcademicPuma\\ExtBibsonomyCsl\\Domain\\Repository\\TagRepository');
        }
    }

    /**
     * action list
     *
     * @return void
     */
    public function listAction()
    {

        $accessor = $this->makeAccessor();
        $tags     = $this->tagRepository->fetchTags($this->settings, $accessor);

        $maxcount = 0;

        if (!empty($tags) && $tags->count() > 0) {
        	foreach ($tags as $currTag) {
        		$count = $currTag->getUsercount();
        		$maxcount = ($count > $maxcount) ? $count : $maxcount;
        	}
            $tags->sort('name', Sorting::ORDER_ASC);
        } else {
        	// in case the list is empty, return "no tags" tag
        	$newTag = new Tag();
        	$newTag->setName("no_tags_found");
        	$tags[] = $newTag;
        }

        $this->view->assign('tags', $tags);
        $this->view->assign('maxcount', $maxcount);
    }

}
