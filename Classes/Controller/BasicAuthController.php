<?php

/*
 *  PUMA/BibSonomy CSL (ext_bibsonomy_csl) is a TYPO3 extension which
 *  enables users to render publication lists from PUMA or BibSonomy in
 *  various styles.
 *
 *  Copyright notice
 *  (c) 2015 Sebastian Böttger <seboettg@gmail.com>
 *
 *  HothoData GmbH (http://www.academic-puma.de)
 *  Knowledge and Data Engineering Group (University of Kassel)
 *
 *  All rights reserved
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace AcademicPuma\ExtBibsonomyCsl\Controller;

require_once \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('ext_bibsonomy_csl') . 'vendor/autoload.php';

use AcademicPuma\ExtBibsonomyCsl\Domain\Exception\AuthExistException;
use AcademicPuma\ExtBibsonomyCsl\Domain\Model\Authentication;
use AcademicPuma\ExtBibsonomyCsl\Domain\Repository\AuthenticationRepository;
use AcademicPuma\ExtBibsonomyCsl\Lib\Helper;
use TYPO3\CMS\Core\FormProtection\Exception;
use TYPO3\CMS\Fluid\Core\Rendering\RenderingContext;
use TYPO3\CMS\Fluid\ViewHelpers\TranslateViewHelper;

/**
 * AuthenticationController
 */
class BasicAuthController extends DefaultActionController
{

    /**
     * authenticationRepository
     *
     * @var AuthenticationRepository
     */
    protected $authenticationRepository;

    /**
     * injectPublicationRepository
     *
     * @param \AcademicPuma\ExtBibsonomyCsl\Domain\Repository\AuthenticationRepository $authenticationRepository
     *
     * @return void
     *
     * public function
     *     injectPublicationRepository(\AcademicPuma\ExtBibsonomyCsl\Domain\Repository\AuthenticationRepository
     *     $authenticationRepository) {
     *
     * $this->authenticationRepository = $authenticationRepository;
     * }
     */
    /**
     *
     */
    public function initializeAction()
    {

        if ($this->authenticationRepository == null) {

            $this->authenticationRepository = $this->objectManager->get('AcademicPuma\\ExtBibsonomyCsl\\Domain\\Repository\\AuthenticationRepository');
        }
    }

    public function listAction()
    {

        $cruserId = $GLOBALS["BE_USER"]->user["uid"];
        $result   = $this->authenticationRepository->findAllBasicAuthOfUser($cruserId);

        $this->view->assign('auths', $result);
    }

    /**
     * action new
     *
     * @param Authentication $newAuthentication
     * @ignorevalidation $newAuthentication
     *
     * @return void
     */
    public function newAction(Authentication $newAuthentication = null)
    {

        $helper = new Helper();

        $this->view->assign('newAuthentication', $newAuthentication);
        $this->view->assign('hosts', $helper->getHosts());
    }

    /**
     * action create
     *
     * @param Authentication $newAuthentication
     *
     * @return void
     */
    public function createAction(Authentication $newAuthentication)
    {

        $newAuthentication->setCreateDate(new \DateTime('now'));

        try {
            $this->authenticationRepository->add($newAuthentication);
        } catch (AuthExistException $e) {
            $translation = new TranslateViewHelper();
            /** @var RenderingContext $renderingContext */
            $renderingContext = $this->objectManager->get('TYPO3\\CMS\\Fluid\\Core\\Rendering\\RenderingContext');
            $renderingContext->setControllerContext($this->controllerContext);

            $translation->setRenderingContext($renderingContext);
            $translation->setArguments([
                'key' => 'be.authentication.exist',
                0     => $e->getHostUserName(),
                1     => $e->getHostAddress()
            ]);
            $this->addFlashMessage($translation->render(), \TYPO3\CMS\Core\Messaging\AbstractMessage::ERROR);
            $this->redirect('list');

            return;
        }
        $this->addFlashMessage('Added successfully');
        $this->redirect('list');
    }

    public function editAction(Authentication $authentication)
    {

        $helper = new Helper();
        $hosts  = $helper->getHosts();
        $this->view->assign('authentication', $authentication);
        $this->view->assign('hosts', $hosts);
    }

    public function updateAction(Authentication $authentication)
    {

        $this->authenticationRepository->update($authentication);
        $this->redirect('list');
    }

    /**
     * action delete
     *
     * @param Authentication $authentication
     *
     * @return void
     */
    public function deleteAction(Authentication $authentication)
    {

        $this->authenticationRepository->remove($authentication);
        $this->redirect('list');
    }

}