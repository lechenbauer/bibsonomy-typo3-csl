<?php

/*
 *  PUMA/BibSonomy CSL (ext_bibsonomy_csl) is a TYPO3 extension which
 *  enables users to render publication lists from PUMA or BibSonomy in
 *  various styles.
 *
 *  Copyright notice
 *  (c) 2015 Sebastian Böttger <seboettg@gmail.com>
 *
 *  HothoData GmbH (http://www.academic-puma.de)
 *  Knowledge and Data Engineering Group (University of Kassel)
 *
 *  All rights reserved
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace AcademicPuma\ExtBibsonomyCsl\Controller;

require_once \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('ext_bibsonomy_csl') . 'vendor/autoload.php';

use AcademicPuma\ExtBibsonomyCsl\Domain\Exception\AuthExistException;
use AcademicPuma\ExtBibsonomyCsl\Domain\Model\Authentication;
use AcademicPuma\ExtBibsonomyCsl\Domain\Repository\AuthenticationRepository;
use AcademicPuma\ExtBibsonomyCsl\Lib\Storage\BackendSessionStorage;
use AcademicPuma\OAuth\OAuthAdapter;
use TYPO3\CMS\Extbase\Persistence\Generic\Exception\RepositoryException;
use TYPO3\CMS\Fluid\Core\Rendering\RenderingContext;
use TYPO3\CMS\Fluid\ViewHelpers\TranslateViewHelper;

/**
 * AuthenticationController
 */
class OAuthController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController
{

    /**
     * authenticationRepository
     *
     * @var AuthenticationRepository
     */
    protected $authenticationRepository;

    /**
     * injectPublicationRepository
     *
     * @param \AcademicPuma\ExtBibsonomyCsl\Domain\Repository\AuthenticationRepository $authenticationRepository
     *
     * @return void
     *
     * public function
     *     injectPublicationRepository(\AcademicPuma\ExtBibsonomyCsl\Domain\Repository\AuthenticationRepository
     *     $authenticationRepository) {
     *
     * $this->authenticationRepository = $authenticationRepository;
     * }
     */
    /**
     *
     */
    public function initializeAction()
    {

        if ($this->authenticationRepository == null) {

            $this->authenticationRepository = $this->objectManager->get('AcademicPuma\\ExtBibsonomyCsl\\Domain\\Repository\\AuthenticationRepository');
        }
    }

    public function listAction()
    {

        $cruserId = $GLOBALS["BE_USER"]->user["uid"];
        $results  = $this->authenticationRepository->findAllOAuthOfUser($cruserId);
        $arr      = [];
        $now      = new \DateTime('now');

        foreach ($results as $result) {
            $date = $result->getCreateDate();
            $date->setTimezone(new \DateTimeZone(date_default_timezone_get()));

            $arr[] = 365 - intval($now->diff($result->getCreateDate())->days);
        }

        $confArray = unserialize($GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf']['ext_bibsonomy_csl']);
        $this->view->assign('dateDiffs', $arr);
        $this->view->assign('auths', $results);
        $this->view->assign('hostSystem', $confArray['defaultHostSystemUrl']);
        $this->view->assign('iconsAvailable', $GLOBALS['TBE_STYLES']['spriteIconApi']['iconsAvailable']);
    }

    public function oauthAction()
    {

        $adapter = $this->prepareOAuthAdapter();

        /** @var BackendSessionStorage $sessionStorage */
        $sessionStorage = $this->objectManager->get('AcademicPuma\\ExtBibsonomyCsl\\Lib\\Storage\\BackendSessionStorage');

        try {
            $requestToken = $adapter->getRequestToken(); //get Request Token
            $sessionStorage->storeObject($requestToken, 'REQUEST_TOKEN');

        } catch (\Exception $e) {
            print_r($requestToken);
            print_r($e);

            return;
        }

        $cruserId = $GLOBALS["BE_USER"]->user["uid"];
        if ($this->authenticationRepository->findAllOAuthOfThisUser($cruserId, 'seboettg')->count() > 0) {
            $tokenMode       = new \stdClass();
            $tokenMode->mode = 'update';
            $sessionStorage->storeObject($tokenMode, 'TOKEN_MODE');
        } else {
            $tokenMode       = new \stdClass();
            $tokenMode->mode = 'new';
            $sessionStorage->storeObject($tokenMode, 'TOKEN_MODE');
        }

        $adapter->redirect($requestToken);

    }

    public function callbackAction()
    {

        $defaultHostSystemUrl = unserialize($GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf']['ext_bibsonomy_csl'])['defaultHostSystemUrl'];
        $cruserId             = $GLOBALS["BE_USER"]->user["uid"];

        /** @var BackendSessionStorage $sessionStorage */
        $sessionStorage = $this->objectManager->get('AcademicPuma\\ExtBibsonomyCsl\\Lib\\Storage\\BackendSessionStorage');
        $requestToken   = $sessionStorage->getObject('REQUEST_TOKEN');
        $tokenMode      = $sessionStorage->getObject('TOKEN_MODE');

        $adapter     = $this->prepareOAuthAdapter();
        $accessToken = $adapter->getAccessToken($requestToken); //fetch Access Token

        if ($tokenMode->mode === 'new') {
            $newAuthentication = new \AcademicPuma\ExtBibsonomyCsl\Domain\Model\Authentication();

            $newAuthentication->setSerializedAccessToken(serialize($accessToken));
            $newAuthentication->setEnableOAuth(true);
            $now = new \DateTime('now');
            $now->setTimezone(new \DateTimeZone(date_default_timezone_get()));
            $newAuthentication->setCreateDate($now);

            $newAuthentication->setHostAddress($defaultHostSystemUrl);
            $newAuthentication->setHostUserName(filter_input(INPUT_GET, 'user_id', FILTER_SANITIZE_FULL_SPECIAL_CHARS));
            $newAuthentication->setPid(0);
            try {
                $this->authenticationRepository->add($newAuthentication);
            } catch (AuthExistException $e) {
                $this->addFlashMessage($e->getMessage(), 'Error', \TYPO3\CMS\Core\Messaging\AbstractMessage::ERROR);
                $this->redirect('list');
            }
            $this->addFlashMessage('Added successfully');
        } else {
            /** @var Authentication $authentication */
            $authentication = $this->authenticationRepository->findAllOAuthOfThisUser($cruserId, 'seboettg')
                                                             ->getFirst();
            $now            = new \DateTime('now');
            $now->setTimezone(new \DateTimeZone(date_default_timezone_get()));
            $authentication->setCreateDate($now);
            $authentication->setSerializedAccessToken(serialize($accessToken));
            try {
                $this->authenticationRepository->update($authentication);
            } catch (RepositoryException $e) {
                $this->addFlashMessage($e->getMessage(), 'Error', \TYPO3\CMS\Core\Messaging\AbstractMessage::ERROR);
                $this->redirect('list');
            }
            $this->addFlashMessage('The AccessToken for "' . $authentication->getHostUserName() . '" on "' . $authentication->getHostAddress() . '" has been updated successfully');
        }

        $this->redirect('list');
    }

    /**
     * action new
     *
     * @param \AcademicPuma\ExtBibsonomyCsl\Domain\Model\Authentication $newAuthentication
     * @ignorevalidation $newAuthentication
     *
     * @return void
     */
    public function newAction(\AcademicPuma\ExtBibsonomyCsl\Domain\Model\Authentication $newAuthentication = null)
    {

        $this->view->assign('newAuthentication', $newAuthentication);
    }

    /**
     * action create
     *
     * @param \AcademicPuma\ExtBibsonomyCsl\Domain\Model\Authentication $newAuthentication
     *
     * @return void
     */
    public function createAction(\AcademicPuma\ExtBibsonomyCsl\Domain\Model\Authentication $newAuthentication)
    {

        try {
            $this->authenticationRepository->add($newAuthentication);
        } catch (AuthExistException $e) {
            $translation = new TranslateViewHelper();
            /** @var RenderingContext $renderingContext */
            $renderingContext = $this->objectManager->get('TYPO3\\CMS\\Fluid\\Core\\Rendering\\RenderingContext');
            $renderingContext->setControllerContext($this->controllerContext);

            $translation->setRenderingContext($renderingContext);
            $translation->setArguments([
                'key' => 'be.authentication.exist',
                0     => $e->getHostUserName(),
                1     => $e->getHostAddress()
            ]);
            $this->addFlashMessage($translation->render(), \TYPO3\CMS\Core\Messaging\AbstractMessage::ERROR);
            $this->redirect('list');

            return;
        }
        $this->addFlashMessage('Added successfully');
        $this->redirect('list');
    }

    /**
     * action edit
     *
     * @param \AcademicPuma\ExtBibsonomyCsl\Domain\Model\Authentication $authentication
     * @ignorevalidation $authentication
     *
     * @return void
     */
    public function editAction(\AcademicPuma\ExtBibsonomyCsl\Domain\Model\Authentication $authentication)
    {

        $this->view->assign('authentication', $authentication);
    }

    /**
     * action update
     *
     * @param \AcademicPuma\ExtBibsonomyCsl\Domain\Model\Authentication $authentication
     *
     * @return void
     */
    public function updateAction(\AcademicPuma\ExtBibsonomyCsl\Domain\Model\Authentication $authentication)
    {

        $this->authenticationRepository->update($authentication);
        $this->redirect('list');
    }

    /**
     * action delete
     *
     * @param \AcademicPuma\ExtBibsonomyCsl\Domain\Model\Authentication $authentication
     *
     * @return void
     */
    public function deleteAction(\AcademicPuma\ExtBibsonomyCsl\Domain\Model\Authentication $authentication)
    {

        $this->authenticationRepository->remove($authentication);
        $this->redirect('list');
    }

    private function prepareOAuthAdapter()
    {

        $confArray      = unserialize($GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf']['ext_bibsonomy_csl']);
        $consumerKey    = $confArray['oauthConsumerToken'];
        $consumerSecret = $confArray['oauthConsumerSecret'];
        $baseUrl        = $confArray['defaultHostSystemUrl'];
        $uriBuilder     = $this->controllerContext->getUriBuilder();
        $callbackUrl    = $uriBuilder->setNoCache(true)
                                     ->setCreateAbsoluteUri(true)
                                     ->setUseCacheHash(false)
                                     ->uriFor('callback', [], $this->request->getControllerName(),
                                         $this->request->getControllerExtensionName(), $this->request->getPluginName());
        $adapter        = new OAuthAdapter([
            'consumerKey'    => $consumerKey,
            'consumerSecret' => $consumerSecret,
            'callbackUrl'    => $callbackUrl,
            'baseUrl'        => $baseUrl . "/"
        ]);

        return $adapter;
    }

}