<?php

/*
 *  PUMA/BibSonomy CSL (ext_bibsonomy_csl) is a TYPO3 extension which
 *  enables users to render publication lists from PUMA or BibSonomy in
 *  various styles.
 *
 *  Copyright notice
 *  (c) 2015 Sebastian Böttger <seboettg@gmail.com>
 *
 *  HothoData GmbH (http://www.academic-puma.de)
 *  Knowledge and Data Engineering Group (University of Kassel)
 *
 *  All rights reserved
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace AcademicPuma\ExtBibsonomyCsl\Controller;

use \AcademicPuma\ExtBibsonomyCsl\Domain\Repository\DocumentRepository;

/**
 * Action Controller for frontend plugin 'Bibsonomy Publication List'.
 * Contains an action to assign a list of publications and a compare method
 * for sorting the list of publications
 *
 * @package ext_bibsonomy_csl
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 * @author Sebastian Böttger
 *
 */
class DocumentController extends DefaultActionController
{

    /**
     * publicationRepository
     *
     * @var \AcademicPuma\ExtBibsonomyCsl\Domain\Repository\DocumentRepository
     */
    protected $documentRepository;

    /**
     * injectDocumentRepository
     *
     * @param \AcademicPuma\ExtBibsonomyCsl\Domain\Repository\DocumentRepository $documentRepository
     *
     * @return void
     */
    public function injectDocumentRepository(DocumentRepository $documentRepository)
    {

        $this->documentRepository = $documentRepository;
    }

    /**
     * @return void
     */
    public function initializeAction()
    {

        if ($this->documentRepository == null) {
            $this->documentRepository = $this->objectManager->get('AcademicPuma\\ExtBibsonomyCsl\\Domain\\Repository\\DocumentRepository');
        }
    }

    /**
     *
     * @param string $intraHash
     * @param string $fileName
     * @param string $userName
     * @param string $preview
     */
    public function previewAction($intraHash, $fileName, $userName, $preview)
    {

        $accessor = $this->makeAccessor();
        $this->documentRepository->getImage($accessor, $userName, $intraHash, $fileName, $preview);
    }

    /**
     *
     * @param string $intraHash
     * @param string $fileName
     * @param string $userName
     */
    public function downloadAction($intraHash, $fileName, $userName)
    {

        $accessor = $this->makeAccessor();
        $this->documentRepository->getDownload($accessor, $userName, $intraHash, $fileName);
    }

    /**
     *
     * @param string $intraHash
     * @param string $fileName
     * @param string $userName
     */
    public function viewAction($intraHash, $fileName, $userName)
    {

        $accessor = $this->makeAccessor();
        $this->documentRepository->getView($accessor, $userName, $intraHash, $fileName);
    }
}
