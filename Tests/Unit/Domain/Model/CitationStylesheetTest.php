<?php

namespace AcademicPuma\ExtBibsonomyCsl\Tests\Unit\Domain\Model;

    /***************************************************************
     *  Copyright notice
     *
     *  (c) 2015 Sebastian Böttger <boettger@cs.uni-kassel.de>, HothoData GmbH (http://www.academic-puma.de)
     *
     *  All rights reserved
     *
     *  This script is part of the TYPO3 project. The TYPO3 project is
     *  free software; you can redistribute it and/or modify
     *  it under the terms of the GNU General Public License as published by
     *  the Free Software Foundation; either version 2 of the License, or
     *  (at your option) any later version.
     *
     *  The GNU General Public License can be found at
     *  http://www.gnu.org/copyleft/gpl.html.
     *
     *  This script is distributed in the hope that it will be useful,
     *  but WITHOUT ANY WARRANTY; without even the implied warranty of
     *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     *  GNU General Public License for more details.
     *
     *  This copyright notice MUST APPEAR in all copies of the script!
     ***************************************************************/

/**
 * Test case for class \AcademicPuma\ExtBibsonomyCsl\Domain\Model\CitationStylesheet.
 *
 * @copyright Copyright belongs to the respective authors
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 * @author Sebastian Böttger <boettger@cs.uni-kassel.de>
 */
class CitationStylesheetTest extends \TYPO3\CMS\Core\Tests\UnitTestCase
{

    /**
     * @var \AcademicPuma\ExtBibsonomyCsl\Domain\Model\CitationStylesheet
     */
    protected $subject = null;

    protected function setUp()
    {
        $this->subject = new \AcademicPuma\ExtBibsonomyCsl\Domain\Model\CitationStylesheet();
    }

    protected function tearDown()
    {

        unset($this->subject);
    }

    /**
     * @test
     */
    public function getIdReturnsInitialValueForString()
    {

        $this->assertSame('', $this->subject->getId());
    }

    /**
     * @test
     */
    public function setIdForStringSetsId()
    {

        $this->subject->setId('Conceived at T3CON10');

        $this->assertAttributeEquals('Conceived at T3CON10', 'id', $this->subject);
    }

    /**
     * @test
     */
    public function getTitleReturnsInitialValueForString()
    {

        $this->assertSame('', $this->subject->getTitle());
    }

    /**
     * @test
     */
    public function setTitleForStringSetsTitle()
    {

        $this->subject->setTitle('Conceived at T3CON10');

        $this->assertAttributeEquals('Conceived at T3CON10', 'title', $this->subject);
    }

    /**
     * @test
     */
    public function getXmlSourceReturnsInitialValueForString()
    {

        $this->assertSame('', $this->subject->getXmlSource());
    }

    /**
     * @test
     */
    public function setXmlSourceForStringSetsXmlSource()
    {

        $this->subject->setXmlSource('Conceived at T3CON10');

        $this->assertAttributeEquals('Conceived at T3CON10', 'xmlSource', $this->subject);
    }
}
