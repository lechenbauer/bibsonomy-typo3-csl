<?php

namespace AcademicPuma\ExtBibsonomyCsl\Tests\Unit\Domain\Model;

    /***************************************************************
     *  Copyright notice
     *
     *  (c) 2015 Sebastian Böttger <boettger@cs.uni-kassel.de>, HothoData GmbH (http://www.academic-puma.de)
     *
     *  All rights reserved
     *
     *  This script is part of the TYPO3 project. The TYPO3 project is
     *  free software; you can redistribute it and/or modify
     *  it under the terms of the GNU General Public License as published by
     *  the Free Software Foundation; either version 2 of the License, or
     *  (at your option) any later version.
     *
     *  The GNU General Public License can be found at
     *  http://www.gnu.org/copyleft/gpl.html.
     *
     *  This script is distributed in the hope that it will be useful,
     *  but WITHOUT ANY WARRANTY; without even the implied warranty of
     *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     *  GNU General Public License for more details.
     *
     *  This copyright notice MUST APPEAR in all copies of the script!
     ***************************************************************/

/**
 * Test case for class \AcademicPuma\ExtBibsonomyCsl\Domain\Model\Authentication.
 *
 * @copyright Copyright belongs to the respective authors
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 * @author Sebastian Böttger <boettger@cs.uni-kassel.de>
 */
class AuthenticationTest extends \TYPO3\CMS\Core\Tests\UnitTestCase
{

    /**
     * @var \AcademicPuma\ExtBibsonomyCsl\Domain\Model\Authentication
     */
    protected $subject = null;

    protected function setUp()
    {

        $this->subject = new \AcademicPuma\ExtBibsonomyCsl\Domain\Model\Authentication();
    }

    protected function tearDown()
    {

        unset($this->subject);
    }

    /**
     * @test
     */
    public function getHostUserNameReturnsInitialValueForString()
    {

        $this->assertSame('', $this->subject->getHostUserName());
    }

    /**
     * @test
     */
    public function setHostUserNameForStringSetsHostUserName()
    {

        $this->subject->setHostUserName('Conceived at T3CON10');

        $this->assertAttributeEquals('Conceived at T3CON10', 'hostUserName', $this->subject);
    }

    /**
     * @test
     */
    public function getHostApiKeyReturnsInitialValueForString()
    {

        $this->assertSame('', $this->subject->getHostApiKey());
    }

    /**
     * @test
     */
    public function setHostApiKeyForStringSetsHostApiKey()
    {

        $this->subject->setHostApiKey('Conceived at T3CON10');

        $this->assertAttributeEquals('Conceived at T3CON10', 'hostApiKey', $this->subject);
    }

    /**
     * @test
     */
    public function getSerializedAccessTokenReturnsInitialValueForString()
    {

        $this->assertSame('', $this->subject->getSerializedAccessToken());
    }

    /**
     * @test
     */
    public function setSerializedAccessTokenForStringSetsSerializedAccessToken()
    {

        $this->subject->setSerializedAccessToken('Conceived at T3CON10');

        $this->assertAttributeEquals('Conceived at T3CON10', 'serializedAccessToken', $this->subject);
    }

    /**
     * @test
     */
    public function getEnableOAuthReturnsInitialValueForBoolean()
    {

        $this->assertSame(false, $this->subject->getEnableOAuth());
    }

    /**
     * @test
     */
    public function setEnableOAuthForBooleanSetsEnableOAuth()
    {

        $this->subject->setEnableOAuth(true);

        $this->assertAttributeEquals(true, 'enableOAuth', $this->subject);
    }

    /**
     * @test
     */
    public function getAccessTokenCreateDateReturnsInitialValueForDateTime()
    {

        $this->assertEquals(null, $this->subject->getAccessTokenCreateDate());
    }

    /**
     * @test
     */
    public function setAccessTokenCreateDateForDateTimeSetsAccessTokenCreateDate()
    {

        $dateTimeFixture = new \DateTime();
        $this->subject->setAccessTokenCreateDate($dateTimeFixture);

        $this->assertAttributeEquals($dateTimeFixture, 'accessTokenCreateDate', $this->subject);
    }
}
