<?php
namespace AcademicPuma\ExtBibsonomyCsl\Tests\Unit\Controller;

    /***************************************************************
     *  Copyright notice
     *
     *  (c) 2015 Sebastian Böttger <boettger@cs.uni-kassel.de>, HothoData GmbH (http://www.academic-puma.de)
     *
     *  All rights reserved
     *
     *  This script is part of the TYPO3 project. The TYPO3 project is
     *  free software; you can redistribute it and/or modify
     *  it under the terms of the GNU General Public License as published by
     *  the Free Software Foundation; either version 2 of the License, or
     *  (at your option) any later version.
     *
     *  The GNU General Public License can be found at
     *  http://www.gnu.org/copyleft/gpl.html.
     *
     *  This script is distributed in the hope that it will be useful,
     *  but WITHOUT ANY WARRANTY; without even the implied warranty of
     *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     *  GNU General Public License for more details.
     *
     *  This copyright notice MUST APPEAR in all copies of the script!
     ***************************************************************/

/**
 * Test case for class AcademicPuma\ExtBibsonomyCsl\Controller\AuthenticationController.
 *
 * @author Sebastian Böttger <boettger@cs.uni-kassel.de>
 */
class AuthenticationControllerTest extends \TYPO3\CMS\Core\Tests\UnitTestCase
{

    /**
     * @var \AcademicPuma\ExtBibsonomyCsl\Controller\AuthenticationController
     */
    protected $subject = null;

    protected function setUp()
    {

        $this->subject = $this->getMock('AcademicPuma\\ExtBibsonomyCsl\\Controller\\AuthenticationController',
            array('redirect', 'forward', 'addFlashMessage'), array(), '', false);
    }

    protected function tearDown()
    {

        unset($this->subject);
    }

    /**
     * @test
     */
    public function showActionAssignsTheGivenAuthenticationToView()
    {

        $authentication = new \AcademicPuma\ExtBibsonomyCsl\Domain\Model\Authentication();

        $view = $this->getMock('TYPO3\\CMS\\Extbase\\Mvc\\View\\ViewInterface');
        $this->inject($this->subject, 'view', $view);
        $view->expects($this->once())->method('assign')->with('authentication', $authentication);

        $this->subject->showAction($authentication);
    }

    /**
     * @test
     */
    public function newActionAssignsTheGivenAuthenticationToView()
    {

        $authentication = new \AcademicPuma\ExtBibsonomyCsl\Domain\Model\Authentication();

        $view = $this->getMock('TYPO3\\CMS\\Extbase\\Mvc\\View\\ViewInterface');
        $view->expects($this->once())->method('assign')->with('newAuthentication', $authentication);
        $this->inject($this->subject, 'view', $view);

        $this->subject->newAction($authentication);
    }

    /**
     * @test
     */
    public function createActionAddsTheGivenAuthenticationToAuthenticationRepository()
    {

        $authentication = new \AcademicPuma\ExtBibsonomyCsl\Domain\Model\Authentication();

        $authenticationRepository = $this->getMock('', array('add'), array(), '', false);
        $authenticationRepository->expects($this->once())->method('add')->with($authentication);
        $this->inject($this->subject, 'authenticationRepository', $authenticationRepository);

        $this->subject->createAction($authentication);
    }

    /**
     * @test
     */
    public function editActionAssignsTheGivenAuthenticationToView()
    {

        $authentication = new \AcademicPuma\ExtBibsonomyCsl\Domain\Model\Authentication();

        $view = $this->getMock('TYPO3\\CMS\\Extbase\\Mvc\\View\\ViewInterface');
        $this->inject($this->subject, 'view', $view);
        $view->expects($this->once())->method('assign')->with('authentication', $authentication);

        $this->subject->editAction($authentication);
    }

    /**
     * @test
     */
    public function updateActionUpdatesTheGivenAuthenticationInAuthenticationRepository()
    {

        $authentication = new \AcademicPuma\ExtBibsonomyCsl\Domain\Model\Authentication();

        $authenticationRepository = $this->getMock('', array('update'), array(), '', false);
        $authenticationRepository->expects($this->once())->method('update')->with($authentication);
        $this->inject($this->subject, 'authenticationRepository', $authenticationRepository);

        $this->subject->updateAction($authentication);
    }

    /**
     * @test
     */
    public function deleteActionRemovesTheGivenAuthenticationFromAuthenticationRepository()
    {

        $authentication = new \AcademicPuma\ExtBibsonomyCsl\Domain\Model\Authentication();

        $authenticationRepository = $this->getMock('', array('remove'), array(), '', false);
        $authenticationRepository->expects($this->once())->method('remove')->with($authentication);
        $this->inject($this->subject, 'authenticationRepository', $authenticationRepository);

        $this->subject->deleteAction($authentication);
    }
}
