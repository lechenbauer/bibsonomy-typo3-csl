function toggleTrace() {
    if (document.getElementById('academicpuma_bibsonomyerror').style.display === 'block') {
        document.getElementById('academicpuma_bibsonomyerror').style.display = 'none';
    } else {
        document.getElementById('academicpuma_bibsonomyerror').style.display = 'block';
    }
}

function bindEvent(element, type, handler) {
    if (element.addEventListener) {
        element.addEventListener(type, handler, false);
    } else {
        element.attachEvent('on' + type, handler);
    }
}

function loadurl(dest, objnev) {

    try {
        xmlhttp = window.XMLHttpRequest ? new XMLHttpRequest() : new ActiveXObject("Microsoft.XMLHTTP");
    } catch (e) {
        console.log(e);
    }
    xmlhttp.onreadystatechange = function () {
        triggered(objnev);
    };

    xmlhttp.open("GET", dest);
    xmlhttp.send(null);
}

function triggered(objnev) {

    if ((xmlhttp.readyState == 4) && (xmlhttp.status == 200)) {
        document.getElementById(objnev).innerHTML = xmlhttp.responseText;
    }
}

function readAuthorLinksContainer() {
    if (document.getElementById('tx-extbibsonomy-csl-author-links-data')) {
        let textContent = document.getElementById('tx-extbibsonomy-csl-author-links-data').textContent.trim();
        let authorsJson = JSON.parse(textContent);
        let csl = '';
        if (document.getElementById('tx-extbibsonomy-csl-author-links-csl')) {
                csl = document.getElementById('tx-extbibsonomy-csl-author-links-csl').textContent.trim()
        }
        generateAuthorLinks(csl, authorsJson);
    }
}

function generateAuthorLinks(csl, authorsJson) {

    let publications = document.getElementsByClassName('csl-entry');
    for (let i = 0; i < publications.length; i++) {
        let authorsWithLinks = '';
        // Check, if CSL used a author span
        if (publications[i].getElementsByClassName('citeproc-author').length > 0) {
            let authors = publications[i].getElementsByClassName('citeproc-author')[0].innerHTML.split(';');
            // Iterate through authors in publication
            for (let j = 0; j < authors.length; j++) {
                let lastname = authors[j].substring(0, authors[j].indexOf(',')).trim();
                let obj = getByLastname(authorsJson.authors, lastname);
                if (obj !== undefined && obj.url != 'none') {
                    authorsWithLinks += '<a target="_blank" href="' + obj.url +
                        '">' + authors[j] + '</a>';
                } else {
                    authorsWithLinks += authors[j];
                }
                authorsWithLinks += '; ';
            }
            publications[i].getElementsByClassName('citeproc-author')[0].innerHTML =
                authorsWithLinks.substring(0, authorsWithLinks.length - 2);
        } else if (csl === 'Springer LNCS') {
            let authors = publications[i].innerHTML.substring(0, publications[i].innerHTML.indexOf(':')).split('., ');
            let tmp = publications[i].innerHTML.substring(publications[i].innerHTML.indexOf(':'));

            for (let j = 0; j < authors.length; j++) {
                if (authors.length > 1 && j < authors.length - 1) {
                    authors[j] = authors[j] + '.';
                }
                let lastname = authors[j].substring(0, authors[j].indexOf(',')).trim();
                let obj = getByLastname(authorsJson.authors, lastname);
                if (obj !== undefined && obj.url != 'none') {
                    authorsWithLinks += '<a target="_blank" href="' + obj.url +
                        '">' + authors[j] + '</a>';
                } else {
                    authorsWithLinks += authors[j];
                }
                authorsWithLinks += ', ';
            }
            publications[i].innerHTML = authorsWithLinks.substring(0, authorsWithLinks.length - 2) + tmp;
        } else {
            // Not a supported CSL used
            break;
        }
    }
}

function initModalLinkListener() {
    let links = document.getElementsByClassName('modal-link');
    for (let i = 0; i < links.length; i++) {
        let link = links[i];
        let id = link.id.substring(link.id.lastIndexOf('-') + 1);
        link.addEventListener('click', function (e) {
            e.preventDefault();
            document.getElementById(this.id.replace('link-', '')).style.display = 'inline-block';
        })
    }
}

function getByLastname(arr, lastname) {

    for (let i = 0; i < arr.length; i++) {
        if (arr[i].lastname === lastname) return arr[i];
    }
}

function initPublicationFilter() {
    if (document.getElementsByClassName("tx-extbibsonomy-csl-publication-filter-button").length > 0 && document.getElementsByClassName("tx-extbibsonomy-csl-publication-filter-clear").length > 0) {
        document.getElementsByClassName("tx-extbibsonomy-csl-publication-filter-button")[0].onclick = filterPublictations;
        document.getElementsByClassName("tx-extbibsonomy-csl-publication-filter-clear")[0].onclick = clearPublicationFilter;
    }
}

function filterPublictations() {
    let value = document.getElementsByClassName("tx-extbibsonomy-csl-publication-filter-input")[0].value.toLowerCase();
    let items = document.getElementsByClassName("tx-extbibsonomycsl-publication");
    for (let i = 0; i < items.length; i++) {
        let text = items[i].innerText.toLowerCase();
        if (text.includes(value)) {
            items[i].style.display = "";
        } else {
            items[i].style.display = "None";
        }
    }
    let headers = document.getElementsByClassName("tx-extbibsonomy-csl-group");
    for (let i = 0; i < headers.length; i++) {
        headers[i].style.display = "None";
    }
    let anchors = document.getElementsByClassName("tx-extbibsonomycsl-jump-list");
    if (anchors.length > 0) anchors[0].style.display = "None";
}

function clearPublicationFilter() {
    document.getElementsByClassName("tx-extbibsonomy-csl-publication-filter-input")[0].value = "";
    let items = document.getElementsByClassName("tx-extbibsonomycsl-publication");
    for (let i = 0; i < items.length; i++) {
        items[i].style.display = "";
    }
    let headers = document.getElementsByClassName("tx-extbibsonomy-csl-group");
    for (let i = 0; i < headers.length; i++) {
        headers[i].style.display = "";
    }
    let anchors = document.getElementsByClassName("tx-extbibsonomycsl-jump-list");
    if (anchors.length > 0) anchors[0].style.display = "";
}

function escapeBibtex() {
    let items = document.getElementsByClassName('tx-extbibsonomycsl-pub-bibtex-content');
    for (let i = 0; i < items.length; i++) {
        let item = items[i];
        item.value = item.value.replace(/%/g, '\\%');
        item.value = item.value.replace(/&/g, '\\&');
        item.value = item.value.replace(/#/g, '\\#');
        item.value = item.value.replace(/_/g, '\\_');
        // Remove backslashes of double escaped characters, till a better regex is found
        item.value = item.value.replace(/\\\\/g, '\\');
    }
}

function htmlencodeBibtex() {
    let items = document.getElementsByClassName('tx-extbibsonomycsl-bibtex-htmlencode');
    for (let i = 0; i < items.length; i++) {
        let item = items[i];
        item.value = decodeHtml(item.value);
    }
}

function decodeHtml(html) {
    let txt = document.createElement("textarea");
    txt.innerHTML = html;
    return txt.value;
}

function linkDisplay() {
    let items = document.getElementsByClassName('tx-extbibsonomycsl-bib-link');

    for (let i = 0; i < items.length; i++) {

        let item = items[i];

        item.addEventListener('click', function (event) {

            event = event || window.event;

            if (this.className.indexOf('bibtex') > -1 ||
                this.className.indexOf('endnote') > -1 ||
                this.className.indexOf('abstract') > -1) {

                event.preventDefault();
            }

            // get href
            let href = this.childNodes[1].getAttribute("href");

            //get area (abs|bib|det)
            let area = this.childNodes[1].getAttribute("rel").substr(0, 3);

            //get id
            let id = this.childNodes[1].getAttribute("rel").substr(4);

            // toggle
            let show = area + '-' + id; // element id to show

            let hide = new Array();

            //hide bibtex, abstract, or details?
            if (area === 'bib') {
                hide[0] = 'abs' + '-' + id;
                hide[1] = 'end' + '-' + id;
                hide[2] = 'det' + '-' + id;
            } else if (area === 'abs') {
                hide[0] = 'bib' + '-' + id;
                hide[1] = 'end' + '-' + id;
                hide[2] = 'det' + '-' + id;
            } else if (area === 'end') {
                hide[0] = 'abs' + '-' + id;
                hide[1] = 'bib' + '-' + id;
                hide[2] = 'det' + '-' + id;
            } else if (area === 'det') {
                hide[0] = 'abs' + '-' + id;
                hide[1] = 'bib' + '-' + id;    
                hide[2] = 'end' + '-' + id;
            }

            //element to show
            let showVar = document.getElementById(show);

            if (showVar) {

                // hide the other one
                if (document.getElementById(hide[0])) {
                    document.getElementById(hide[0]).style.display = "none";
                }
                if (document.getElementById(hide[1])) {
                    document.getElementById(hide[1]).style.display = "none";
                }
                if (document.getElementById(hide[2])) {
                    document.getElementById(hide[2]).style.display = "none";
                }

                if (showVar.style.display === 'none') {
                    showVar.style.display = 'block';
                } else {
                    showVar.style.display = 'none';
                }
            }
            // if this container for link to bibtex
//            if (this.className.indexOf('bibtex') > -1 || this.className.indexOf('endnote') > -1) {
//
//                loadurl(href, area + "-" + id);
//            }

            return false;
        });
    }
}

window.addEventListener("DOMContentLoaded", function () {

    readAuthorLinksContainer();
    linkDisplay();
    initModalLinkListener();
    htmlencodeBibtex();
    escapeBibtex();
    initPublicationFilter();

});

$(document).ready(function() {
    let data = [];
    if (document.getElementsByClassName('tx-extbibsonomy-csl-typeahead-data')) {
        let textContent = document.getElementsByClassName('tx-extbibsonomy-csl-typeahead-data')[0].textContent.trim();
        data = JSON.parse(textContent);
    }
    // constructs the suggestion engine
    let titles = new Bloodhound({
        datumTokenizer: Bloodhound.tokenizers.whitespace,
        queryTokenizer: Bloodhound.tokenizers.whitespace,
        // `states` is an array of state names defined in "The Basics"
        local: data
    });

    $('.tx-extbibsonomy-csl-publication-filter .tx-extbibsonomy-csl-publication-filter-input').typeahead({
            hint: true,
            highlight: true,
            minLength: 1
        },
        {
            name: 'titles',
            source: titles
        });
});