BibSonomy/PUMA CSL
=============

[BibSonomy/PUMA CSL](http://typo3.org/extensions/repository/view/ext_bibsonomy_csl) is a TYPO3 extension to include publication lists from the social bookmarking and publication sharing systems [BibSonomy](http://www.bibsonomy.org), (academic) [PUMA](http://www.academic-puma.de) or from other sources.

Features
--------
* Display publication lists from BibSonomy/PUMA by using the [BibSonomy REST API](https://bitbucket.org/bibsonomy/bibsonomy/wiki/documentation/api/REST%20API)
* Display publications from different sources in various styles (CSL Standard)
* Display your BibSonomy/PUMA tag cloud
* Layout bibliographies or publication lists with Citation Style Language (CSL) templates. See [citationstyles.org](http://citationstyles.org/).
* Add and manage different CSL styles within TYPO3.

Usage
-----
Have a look at [docs.typo3.org/typo3cms/extensions/ext_bibsonomy_csl](http://docs.typo3.org/typo3cms/extensions/ext_bibsonomy_csl/1.1.1/)

Development
-----------
This extension uses the Extbase framework and the Fluid template engine.

Before starting development of ext_bibsonomy_csl it is recommended to study the TYPO3 documentation.

Change log
----------

### Version 3.0.0 ###

* Support for TYPO3 v9+
* Support for route enhancers in version 9
* Added instructions on how to use route enhancers for this plugin and removing cHash in URLs

### Version 2.1.23 ###

* RealURL support for clean publication URLs

### Version 2.1.22 ###

* Detail Pages for publication are now in modals within the page
* HTML Encode for certain characters in BibTeX area
* Added inline publication search
* Bug fixes for tag-cloud output format

### Version 2.1.21 ###

* Fixed an error with misc URLs

### Version 2.1.20 ###

* Enabling cHash in links again, until solution for working removal is found
* Showing keywords/tags in BibTeX section of publiction
* Minor bug fixes

### Version 2.1.19 ###

* Support for Typo3 v8.7
* Cleaner alt text in publication thumbnail frame
* Search engine optimization for the documents
* Added option to display both author links directly in publication and as modal
* Added sort-by-month for publication subgroups.

### Version 2.1.18 ###

* Fixed Author-Links for umlauts
* Author-Links data can now be provided via link to text-file instead of list
* LNCS-Support for author links
* DBLP-like-sort now honors CoRR and related things

### Version 2.1.17 ###

* Removed depricated methods for image paths
* Improved author links
* Remove leading backslashes in class paths (Thanks Maximilian Völker)
* Fixed wrong namespace in AuthenticationRepository (Thanks Maximilian Völker)

### Version 2.1.16 ###

* Fixed DBLP-Sort for other parenthesis type
* Fixed Tag-Cloud-Links
* Fixed pdf-files with comma in chrome
* Added Host-Button for resource added
* Now pure BibTex is provided after clicking on BibTeX button

### Version 2.1.15 ###

* Fixed various problems with math operators in math environments
* Added new name-link feature.
* All links are now opened in new tabs
* Fixed DBLP-like sorting

### Version 2.1.13 ###

* Fixed capitalization issues
* Improved 'notes' placement
* PDF in new Tab
* Added DBLP-like sorting of publication

### Version 2.1.12 ###

* Now handling publications with the entrytype 'Unpublished"
* Possible to sort after publication type within year grouping (DBLP-like)
* Option to escape reserved special characters in the BibTeX of th

### Version 2.1.9 ###

* Empty tag-clouds fix
* Problems with excluding tags in tag-cloud fixed

### Version 2.1.6 ###

* New option to display content of the BibTex field "note" under the Layout tab.

### Version 2.1.5 ###

* Minor bugfixes and beatifications.

### Version 2.1.4 ###

* Thumbnails now have a fixed height which should avoid the layout from shifting while loading.

### Version 2.1.2 ###

* Fixed bug where the curly brace options would be ignored.

### Version 2.1.1 ###

* Fixed bug where the curly braces of misc fields would be removed, which resulted in not working custom links.

### Version 2.1.0 ###

* New options to determine how curly braces will be handled (remove/keep/keep in math mode).
* New option to determine whether BibTex cleaning should be applied.
* New option to either keep or remove non-numeric year information. (coming 2017 e.g.)
* New option to remove duplicates either by intra- or by interhash.
* New option to enable and disable SSL ca verification. You can also specify a custom path to the certificates.
* Fixed bug where curly braces of the misc field would be removed resulting in not working misc field links.

### Version 2.0.0 ####

* Nearly completely rewritten (using powerful libraries) / Redesigned BE Module / Even more comfortable way to import CSL Stylesheets in BE Module / Possibility to save API Credentials in BE Module in order to simplify usage of FE Plugins / New "Details View

### Version 1.4.1 ###

* Restored backwards compatibility for using stored plugin settings of extension version 1.3.1.

### Version 1.4.0 ###

1. Improvements in CSL processor "citeproc-php" for rendering CSL. Have a look at the [citeproc-php repo](https://bitbucket.org/seboettg/citeproc-php/issues?&milestone=1.0.0-beta)
1. Enhanced grouping options: No confusing use of the terms "Sorting" and "Ordering" in the plugin settings. Now there are one field for grouping and one field for sorting
1. It is possible to group your publications by year (ascending and descending) and by publication type. In addition you can sort your publications within this group by author, title and year (ascending and descending).
1. HTML tags in abstract will be removed
1. Missing headline "Article journal" will be displayed when you group your publications by type
1. Fixed wrong mime type for preview images. Therefore, thumbnails were not displayed in Safari.
1. Removed typos in plugin setting forms

### Version 1.3.1 ###

1. Fixed an javascript issue that occuring in some browsers: the BibTeX and abstract link opens only after the second click
1. Added ability to provide endnote export
1. BibTeX and Endnote exports will fetched over the REST API (AJAX) and not generated by the extension
1. Improved backward compatibility to Version 1.2.0 for publication list settings (sorting)

### Version 1.3 ###

* a lot of bug fixes

#### Publication List ####
1. Added ability to provide download links in publication list of private documents attached to a publication
1. Added ability to render thumbnails in publication list of private documents attached to a publication
1. Added ability to sort publications by title, author and (entry) types of publications
1. It is possible to exclude publications with an certain tag

#### Tag Cloud ####
1. Filter certain tags with a black list
1. Display only related tags of a certain tag